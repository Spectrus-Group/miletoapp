export enum APP_CONSTANT {
  ID_TOKEN='id_token',
  PIN='pin',
  FINGERPRINT_ENABLED='fingerprint_enabled',
  EVENT_TOKEN_EXPIRED='event_token_expired',
  LOGGED_OUT='logged_out',
  SIGNED_IN='signed_in'
}

// This  file contains list of all the api this angular application uses.
// This file also documenting how to use the api

import {environment} from './environments/environment';

const serverUrl = `${environment.apiURl}:${environment.apiPort}/api/${environment.apiVersion}`;


export const UserApi = {
  signin: {
    url:serverUrl+'/authentication',
    method: 'POST'

  },
    signup: {
    url:serverUrl+'/signup',
    method: 'POST'

  },
  getProfile: {
    url: serverUrl + '/profile',
    method: 'GET'

  },
  signout: {
    url:serverUrl+'',
    method: 'GET'
  },
  updateProfile:{
    url:serverUrl+'/profile/update',
    method:'PUT'
  },
  termAndCondition:{
    url:serverUrl+'/terms',
    method:'GET'
  },
  forgotpassword:{
    url:serverUrl+'/forgotpassword',
    method:'POST'
  }





}

export const LogApi={
  url:serverUrl,
};




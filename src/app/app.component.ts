import {Component, ViewChild} from '@angular/core';
import {Events, ModalController, NavController, Platform} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {Isettings} from "../interface/settings.interface";
import {APP_CONSTANT} from "./constants";
import {SettingsProvider} from "../providers/settings/settings";
import firebase from 'firebase';
import {GoogleAnalytics} from "@ionic-native/google-analytics"
import {App, ViewController} from 'ionic-angular';
import {AppVersion} from '@ionic-native/app-version';
//import {FirebaseAnalytics} from '@ionic-native/firebase-analytics';

import {firebaseConfig} from "./credentials.backup";
import {Unsubscribe} from '@firebase/util';
import {Subject} from "rxjs/Subject";
import {UserService} from "../providers/user.service";
import {FirebaseDataProvider} from "../providers/firebase-data/firebase-data";
import { LocalNotifications } from '@ionic-native/local-notifications';
import { EventServiceProvider } from '../providers/event-service/event-service';
import { empty } from 'rxjs/Observer';
import {AuthProvider} from '../providers/auth/auth';
import {PushNotificationProvider} from "../providers/push-notification/push-notification";


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any;
  activePage = new Subject();

  settings: Isettings = {pinEnabled: false, redirectToPin: true};
  isPinSet = false;
  redirectToPin = true;
  @ViewChild('content') nav: NavController

  constructor(private platform: Platform,
              statusBar: StatusBar,
              private splashScreen: SplashScreen,
              private userService: UserService,
              private settingsService: SettingsProvider,
              private events: Events,
              private ga: GoogleAnalytics,
              private app: App,
              private appVersion: AppVersion,
            //  private firebaseAnalytics: FirebaseAnalytics,
              private firebaseData: FirebaseDataProvider,
              private espr: EventServiceProvider,
              private pushNotification:PushNotificationProvider,
              private localNotifications: LocalNotifications,
              public modalCtrl: ModalController,
              public authService : AuthProvider) {


    //Fire base initialisation
    firebase.initializeApp(firebaseConfig);

    this.firebaseData.getAllCategory();
    this.firebaseData.getProducts();
    this.firebaseData.getFavouriteFlower();

    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.


      statusBar.overlaysWebView(false)
      statusBar.backgroundColorByHexString('#c0c0c0');

      this.splashScreen.hide();

      //initilise google analytics , ga is variable of injected googleAnalytics
      this.ga.startTrackerWithId('UA-115760811-1') //replace with ur app’s property id
        .then(() => {
          console.log('Google analytics is ready now');


          //get version code
          var versionName;
          var versionCode;

          //get version code
          appVersion.getVersionCode().then((vc: any) => {
            versionCode = vc;
            this.ga.setAppVersion("Code " + versionCode + " ,Name " + versionName);
          }).catch(e => console.log('Error getting app version code', e))

          //get version number
          appVersion.getVersionNumber().then((vc: any) => {
            versionName = vc;
            this.ga.setAppVersion("Code " + versionCode + " ,Name " + versionName);
          }).catch(e => console.log('Error getting app version number', e))


          this.ga.setAllowIDFACollection(true); //needed for demography stats

        }).catch(e1 => console.log('Error starting GoogleAnalytics', e1));

      // catch page change event thus send Google analytics
      this.app.viewDidEnter.subscribe((data: ViewController) => {
        console.log(data)
        ga.trackView(data.name)
          .catch(e => console.log('Error starting GoogleAnalytics [Page event]', e));
        ; //sending page name to GA
      })
      /*
      //set crash analytics on
      this.firebaseAnalytics.setEnabled(true).then((data: any) => {
        console.log('Crash analytics is ready now ' + data);
      });

      // catch page change event thus send crash analytics
      this.app.viewDidEnter.subscribe((data: ViewController) => {
        console.log(data);
        this.firebaseAnalytics.setCurrentScreen(data.name) //Current page
          .catch((error: any) => console.error("Crash analytics Page error " + error));
      })   */


      firebase.auth().onAuthStateChanged(user => {
        if (user) {
          this.pushNotification.pushSetUp();
          this.rootPage = 'MenuPage';
          ///if aalready signed in 
          authService.timestampProfileLastActiveOn();
        } else {
          this.rootPage = 'SigninPage';
        }
      });

      this.events.subscribe(APP_CONSTANT.EVENT_TOKEN_EXPIRED, () => {
        this.nav.setRoot('SigninPage');
      });

      this.events.subscribe(APP_CONSTANT.LOGGED_OUT, () => {
        this.nav.setRoot('SigninPage');
      });

       this.localNotifications.on('click').subscribe(data => {
        console.log(data)
        var eventId: string = data.data

        console.log('lengh',this.espr.eventListFromFireBase.length);
        this.openNotification(eventId)
      })
    });



  }


  openPage(p) {
    this.nav.setRoot(p.component);
    this.activePage.next(p);
  }


  onAppResume() {
    this.platform.resume.subscribe(() => {
      // redirectToPin is being used to handle some special case where app goes in background and
      // pause events fired followed by resume
      //for rg. openning barcode
      //so in send page we are making this.settings.redirectToPin=false so that it does not ask for pin
      //in rest of the cases this will always be true
      if (this.settings.isLoggedIn && this.settings.pinEnabled && this.settings.redirectToPin)
        if (this.nav.getActive().id !== 'PasscodeLockPage') {
          this.nav.push('PasscodeLockPage', {source: 'onResume'});
        }

    });
  }

  onAppPause() {
    this.platform.pause.subscribe(() => {

      this.loadSetting();
    });
  }


  loadProfile() {
    this.userService.getProfile().then(() => {

    }).catch((err) => {

    })
  }

  async loadSetting() {
    this.settings = await this.settingsService.loadSettings();
    console.log(this.settings)
  }

  openNotification(eventId){
    
    let i =0;
    console.log(i)
    if(i<60){
      if(this.espr.eventListFromFireBase.length==0){
        setTimeout(() => {
        this.openNotification(eventId)
        }, 1000); 
        i++
        
      }
      else{
        this.viewDetailsPage(eventId); 
        i++
    }
    }
    
  
}

  viewDetailsPage(event_Id: string) {
    
    //if the clicked item is event
      console.log(event_Id);
      if(event_Id!=undefined){
        var index = this.espr.eventListFromFireBase.findIndex(x => x.eventId==event_Id);
        console.log(index);
      //edit mode
      this.espr.eventEditModeFlag = true;
      //set clicked items index
      this.espr.eventItemIndexClicked = index;
      //go to evant  modal page but with edit flag on
  
      let profileModal = this.modalCtrl.create('EventModalPage');
     profileModal.present();
      }
   
  }

  
}

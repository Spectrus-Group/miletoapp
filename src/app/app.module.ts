import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ToastProvider } from '../providers/toast';
import { UserService } from '../providers/user.service';
import { HttpClientModule } from '@angular/common/http';
import { HttpErrorHandlerService } from '../providers/http-error-handler.service';
import { IonicStorageModule } from '@ionic/storage';
import { AppErrorHandlerService } from '../providers/app-error-handler.service';
import { LogService } from '../providers/logger/log.service';
import { LogPublisherService } from '../providers/logger/log-publisher.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { InterceptorService } from '../providers/interceptor.service';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import { SettingsProvider } from '../providers/settings/settings';
import {FingerprintAIO} from "@ionic-native/fingerprint-aio";
import {Toast} from "@ionic-native/toast";
import {Vibration} from "@ionic-native/vibration";
import {LoaderProvider} from "../providers/loader/loader";
import {AuthProvider} from "../providers/auth/auth";
import {GoogleAnalytics} from "@ionic-native/google-analytics";
import { AppVersion } from '@ionic-native/app-version';
import {SpeechRecognition} from "@ionic-native/speech-recognition";
import { FirebaseDataProvider } from '../providers/firebase-data/firebase-data';
import { EventServiceProvider } from '../providers/event-service/event-service';
import {OrderDetailsPage} from '../pages/orderdetails/orderdetails'
import {MoreDetailsPage} from '../pages/moredetails/moredetails'
import {EventDetailsPage} from '../pages/eventdetails/eventdetails'
import { DatePicker } from '@ionic-native/date-picker';
import { Firebase } from '@ionic-native/firebase';
import {ProductsServiceProvider} from '../providers/products-service/products-service';
import { HolidayProvider } from '../providers/holiday/holiday-service';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { EmailComposer } from '@ionic-native/email-composer';
import { CallNumber } from '@ionic-native/call-number';
import { ReminderServiceProvider } from '../providers/reminder-service/reminder-service';
import {PushNotificationProvider} from "../providers/push-notification/push-notification";
import {Push} from "@ionic-native/push";
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { BlogserviceProvider } from '../providers/blogservice/blogservice';
import { CalendarModule } from "ion2-calendar";



@NgModule({
  declarations: [
    MyApp,
    OrderDetailsPage,
    MoreDetailsPage,
    EventDetailsPage,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    IonicStorageModule.forRoot({
      name: '__mydb',
      driverOrder: ['sqlite','indexeddb', 'websql']
    }),
    IonicModule.forRoot(MyApp),
    CalendarModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    OrderDetailsPage,
    MoreDetailsPage,
    EventDetailsPage,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptorService,
      multi: true
    },
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: AppErrorHandlerService},
    ToastProvider,
    LogService,
    LogPublisherService,
    UserService,
    HttpErrorHandlerService,
    FingerprintAIO,
    SettingsProvider,
    Toast,
    Vibration,
    LoaderProvider,
    AuthProvider,
    EventServiceProvider,
    GoogleAnalytics,
    AppVersion,
    SpeechRecognition,
    FirebaseDataProvider,
    DatePicker,
    Firebase,
    HolidayProvider,
    ProductsServiceProvider,
    LocalNotifications,
    EmailComposer,
    CallNumber,
    ReminderServiceProvider,
    PushNotificationProvider,
    Push,
    InAppBrowser,
    BlogserviceProvider
  ]
})
export class AppModule {}

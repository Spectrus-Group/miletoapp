import { Injectable } from '@angular/core';
import firebase from 'firebase';
import { User } from '@firebase/auth-types';
import {HttpErrorHandlerService} from "../http-error-handler.service";
import {Events} from "ionic-angular";
import {APP_CONSTANT} from "../../app/constants";
import {Icredential} from "../../interface/credential.interface";
import {Iprofile} from "../../interface/profile.interface";


@Injectable()
export class AuthProvider {
  constructor(private httpErrorHandler:HttpErrorHandlerService,private events:Events) {}

  loginUser(email: string, password: string): Promise<User> {
    return firebase.auth().signInWithEmailAndPassword(email, password);
  }

  async signupUser(credential:Icredential): Promise<User> {
    try {
      const newUser: User = await firebase
        .auth()
        .createUserWithEmailAndPassword(credential.email, credential.password);

      await firebase
        .database()
        .ref(`/profile/${newUser.uid}`)
        .set({email:credential.email,name:credential.name,createdOn:Date.now()});
      return newUser;
    } catch (error) {
      throw error; 
    }
  }

  async resetPassword(email: string): Promise<void> {
    try{
      return await firebase.auth().sendPasswordResetEmail(email);
    }
    catch (err){
      return Promise.reject(this.httpErrorHandler.handle(err));

    }
  }

  async logoutUser(): Promise<void> {
    try {
      return await firebase.auth().signOut();
    }
    catch (err)
      {
        return Promise.reject('could not be logged out!');

      }

    }

  /**
   * this method will return the current user
   */
  getUser() : User {
    return firebase.auth().currentUser;
  }



  async changePassword(currentPassword:string, newPassword:string) {
    let user:firebase.User = firebase.auth().currentUser;
    let credentials = firebase.auth.EmailAuthProvider.credential(user.email,currentPassword);

    try{
     await user.reauthenticateWithCredential(credentials);
    }
    catch (err){
      return Promise.reject(this.httpErrorHandler.handle(err));
    }

    try{
     return await user.updatePassword(newPassword);
    }
    catch (err){
      return Promise.reject(this.httpErrorHandler.handle(err));
    }
}

   timestampProfileLastActiveOn(){
     //get the user 
     let user = this.getUser();
     let userId = user.uid;
  
     this.newMethod(user);
     var ref = firebase.database().ref('/profile/'+userId+'/').once('value')
     .then((Snapshot)=>{
       var profile : Iprofile = {};
       //name
       if(Snapshot.val().name!=undefined){
       profile.name=Snapshot.val().name;
       }else{
        profile.name="NA";
       }
       //mobile
       if(Snapshot.val().mobile!=undefined){
        profile.mobile=Snapshot.val().mobile;
        }else{
         profile.mobile="NA";
        }
        //email
       if(Snapshot.val().email!=undefined){
        profile.email=Snapshot.val().email;
        }else{
         profile.email="NA";
        }
        //adress
        if(Snapshot.val().address!=undefined){
          profile.address=Snapshot.val().address;
          }else{
           profile.address="";
          }
        //created on
       if(Snapshot.val().createdOn!=undefined){
        profile.createdOn=Snapshot.val().createdOn;
        }else{
         profile.createdOn=Date.now();
        } 
         //last logged on
        profile.lastLoggedIn=Date.now();
   
        firebase.database().ref('/profile/'+userId+'/').set(profile);
     })
  }



  private newMethod(user: User) {
    console.log(user);
  }
}

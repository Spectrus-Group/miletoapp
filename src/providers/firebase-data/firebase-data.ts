import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import firebase from 'firebase';
import {Icategory, ICategorySchema} from "../../interface/category.interface";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {Observable} from "rxjs/Observable";
import {category, Iproduct, data} from "../../interface/product.interface";
import {StoreDetails} from '../../interface/storedetails_interface';

/*
  Generated class for the FirebaseDataProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class FirebaseDataProvider {

  private _category: BehaviorSubject<Icategory>;
  categoryData: Icategory = {color: [], source: [], type: [], favourite:[], featured:[]};
  category: Observable<Icategory>;

  private _product: BehaviorSubject<Iproduct[]>;
  productData: Iproduct[] = [];
  product: Observable<Iproduct[]>;

  private _favourite: BehaviorSubject<string[]>;
  favouriteData: string[] = [];
  favourite: Observable<string[]>;

  public storeDetails : StoreDetails[];


  constructor(public http: HttpClient) {

    this._category = <BehaviorSubject<Icategory>> new BehaviorSubject(this.categoryData);
    this.category = this._category.asObservable();

    this._product = <BehaviorSubject<Iproduct[]>>new BehaviorSubject<Iproduct[]>(this.productData);
    this.product = this._product.asObservable();

    this._favourite = <BehaviorSubject<string[]>>new BehaviorSubject<string[]>(this.favouriteData);
    this.favourite = this._favourite.asObservable();

  }

  getProducts() {
    return new Promise((resolve, reject) => {
      var databaseRef = firebase.database().ref('products/');
      var array: Iproduct[] = [];
      var todayDate : Date = new Date();

      databaseRef
        .on('value', (snapshot) => {
          let snapShotVal = snapshot.val();
          if (snapShotVal) {
          //  console.log("Start product retrieve ......................"+new Date().getTime())
            array = [];

            for (let productKey in snapShotVal) {

              let prod = snapShotVal[productKey];
              prod.groupid = productKey;
              //check prod for offseason
              let prod_temp : data = prod ; 
              prod_temp.isOffSeason = this.checkOffSeason(prod_temp.season,todayDate);
              array.push({
                id: productKey,
                isFavorite: false,
                show: false,
                product: prod_temp
              })
              ;
            }
          }
        //  console.log("End product retrieve ......................"+new Date().getTime())
        //sort array accoridng to alphabetical order
        array.sort(function(p:Iproduct , n:Iproduct){
          return p.product.name.localeCompare(n.product.name);
        })
        resolve(array)
          this._product.next(array);
        },
          (error)=>{
          reject (error);
          })
    })
  }

  getAllCategory() {
   // console.log('i am in the getall category start')
    return new Promise((resolve, reject) => {
      var databaseRef = firebase.database().ref('category/');
      let category: Icategory = {color: [], source: [], type: [], favourite:[], featured:[], seasonal:[], roundAYear:[]};

      databaseRef
        .once('value', (snapshot) => {
          let snapShotVal = snapshot.val();
          if (snapShotVal) {
          //  console.log(snapshot.val())
            for (let colorKey in snapShotVal.color) {
              category.color.push({
                id: colorKey,
                name: snapShotVal.color[colorKey].name,
                hex: snapShotVal.color[colorKey].hex,
                url: snapShotVal.color[colorKey].url
              });
            }
            //sort color category
            //sort array accoridng to alphabetical order
              category.color.sort(function(p:ICategorySchema , n:ICategorySchema){
                return p.name.localeCompare(n.name);
              })

              
            for (let sourceKey in snapShotVal.source) {
              category.source.push({
                id: sourceKey,
                source: snapShotVal.source[sourceKey]
              });
            }
            for (let typeKey in snapShotVal.type) {
              category.type.push({
                id: typeKey,
                name: snapShotVal.type[typeKey].name,
                hex: snapShotVal.type[typeKey].hex,
                url: snapShotVal.type[typeKey].url
              });
            }
          }
          this._category.next(category);

         // console.log(category);
          resolve(category)
        },
          (error)=>{
          reject (error);
          })
    })
  }

  addAsFavorite(data: Iproduct) {
    return new Promise((resolve, reject) => {
      var databaseRef = firebase.database().ref('favorite/').child(firebase.auth().currentUser.uid);
      databaseRef.push({product: data.id});
    });
  }

  getFavouriteFlower() {
    return new Promise((resolve, reject) => {

      firebase.auth().onAuthStateChanged(user => {
        if (user) {
          let userId = user.uid

          var databaseRef = firebase.database().ref('favorite/').child(userId);
          let array: string[] = [];
          databaseRef.on('value', (snapshot) => {
            let snapShotVal = snapshot.val();
            if (snapShotVal) {
              array = [];
              for (let key in snapShotVal) {
                try{
                  array.push(snapShotVal[key].product.toString());
                }catch(E){}

              }
            }
             //sort array accoridng to alphabetical order
              array.sort(function(p , n){
                return p.localeCompare(n);
              })
            this._favourite.next(array);
            resolve(array);
          },
            (error)=>{
            reject(error);
            });

        }
      });

    });
  }

  removeFromFavorite(data: Iproduct) {
    return new Promise((resolve, reject) => {

      firebase.auth().onAuthStateChanged(user => {
        if (user) {
          let userId = user.uid

          var databaseRef = firebase.database().ref('favorite/').child(userId);
          let array: string[] = [];

          databaseRef.once('value' ,(snapshot)=>{
            array = [];
            let snapshotVal = snapshot.val();
            if (snapshot){
              for (let key in snapshotVal){
                if (data.id === snapshotVal[key].product){
                  databaseRef.child(key).remove();
                  resolve ({success: true});
                }else{
                  array.push(snapshotVal[key].product);
                }
              }
            }
            this._favourite.next(array);
            resolve (array);
          },
            (error)=>{
            reject (error);
            }).catch((error)=>{
            reject (error);
          })
        }
      });
    });
  }


  public getStoreDetails(){
    var databaseRef = firebase.database().ref('stores/');
    var keysorted = databaseRef.orderByKey();
    keysorted.on('value',  (snapshot)=> {
      this.storeDetails=[];
      let snapshotVal = snapshot.val();
      if (snapshotVal) {
        for (let productKey in snapshotVal){
          var snap = snapshotVal[productKey];
          var temp : StoreDetails={};

          temp.storekey = productKey;
          temp.storeHeader = snap.name;
          temp.storeAddrline1 = snap.addressLine1;
          temp.storeAddrline2 = snap.addressLine2;
          temp.storeAddrline3 = snap.addressLine3;
          temp.storeAddrline4 = "";
          temp.storephone = snap.phone;
          temp.storeemail = snap.email;

          this.storeDetails.push(temp);
         // console.log("STore details "+temp);
        }}});
    }


     /**
   * This method will check wether the flower is offseason during the event
   */
  public checkOffSeason ( season  : string[] ,  date: Date)  : boolean{
    let monthTable : string[] = ["jan","feb","mar","apr","may","jun","jul","aug","sep","oct","nov","dec"];

    //if event not in the db that means it is a new event so get date form temp obj
    var evetMonth = date.getMonth();
    if(evetMonth == undefined){
      return false;
    }
    if(season == undefined){
      return false;
    }
    var offSeasonMonths : string[] =season;

    //check for offseason match
    for(var j=0; j<offSeasonMonths.length; j++){
        if((offSeasonMonths[j].toLocaleLowerCase().includes(monthTable[evetMonth]))||
        (offSeasonMonths[j].toLocaleLowerCase()==(monthTable[evetMonth]))){
           return true;
      }
    }
    return false;
  }



}

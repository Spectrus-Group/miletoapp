import { Injectable } from '@angular/core';
import { Toast } from '@ionic-native/toast';


@Injectable()
export class ToastProvider {

  constructor(public toast: Toast) {
  }
  public presentToast(text:string,position:string='bottom',duration:number=3000,showCloseButton:boolean=false) {
    text=text && text.length ? text :'Something went wrong';
    try{
    this.toast.show(text, ''+duration, position).subscribe();
    }catch(E){}
  }
}

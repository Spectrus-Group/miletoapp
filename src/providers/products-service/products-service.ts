import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import {HttpClient} from "@angular/common/http";
import {FlowerDetails,EventDetails} from "../../interface/event_interfaces";
import {ProductDetails,ProductCategory} from "../../interface/product_interface";
import {AuthProvider} from "../../providers/auth/auth";
import firebase from 'firebase';
import {EventServiceProvider} from '../../providers/event-service/event-service'
import {FirebaseDataProvider} from '../../providers/firebase-data/firebase-data'
import {category, Iproduct} from "../../interface/product.interface";


@Injectable()
export class ProductsServiceProvider {
public productsFromDB : ProductDetails[] =[];
public lastClickedProductId : string ="";
public espr : EventServiceProvider ;
public firebaseData : FirebaseDataProvider;
public monthTable : string[] = ["jan","feb","mar","apr","may","jun","jul","aug","sep","oct","nov","dec"];

  constructor(private http: HttpClient,private auth:AuthProvider,
  private esp :EventServiceProvider , private fbdata : FirebaseDataProvider) {
   this.espr = esp;
   this.firebaseData = fbdata;
  }

  public getAllProductsFromDb(){
    var context = this;  
    var userId = firebase.auth().currentUser.uid;
    var productsRef = firebase.database().ref("products/");
    //load all products
    var keySorted = productsRef.orderByKey();
    keySorted.on('value',  (snapshot)=> {
      let snapShotVal = snapshot.val();
      if (snapShotVal) {
        for (let productKey in snapShotVal){
          var snap = snapShotVal[productKey];
          var temp : ProductDetails={};
          temp.id = productKey;
          temp.description = snap.description;
          temp.name = snap.name;
       //   temp.groupid = snap.groupid;  
           temp.groupid = productKey; 
          temp.price_large = snap.price_large;
          temp.price_medium = snap.price_medium;
          temp.price_small = snap.price_small;
          temp.url_medium = snap.url_medium;
          temp.url_small = snap.url_small;
          temp.url_large = snap.url_large;
          temp.season = snap.season;
          temp.size = snap.size;
          temp.type = snap.type;
          temp.color = snap.color;
          temp. est_stems = snap. est_stems;
          
          this.productsFromDB.push(temp);
        }
      }
    });
  }
  /**
   * this method is used to get a product by key in master array 
   * @param key product key
   */
  public getProductByKeyInMaster (key : string)  : ProductDetails {
  //  console.log("total "+this.productsFromDB.length);
      for(var i =0 ; i<this.productsFromDB.length;i++){
      //  console.log("---------------------- __loop"+ this.productsFromDB[i].id +"    "+key);
        if(this.productsFromDB[i].id == key){
       //   console.log("--------------------- ____Return"+ this.productsFromDB[i].name);
          return this.productsFromDB[i];
        }
      }
  }

   /**
   * This method will check wether the flower is offseason during the event
   */
  public checkOffSeason ( productId  : string ,  eventId: string)  : boolean{
    var product : ProductDetails;
    var eventDetails : EventDetails ;
    product= this.getProductByKeyInMaster(productId);
    eventDetails = this.espr.getEventObjectFromMasterArrayByKey(eventId);
    //if event not in the db that means it is a new event so get date form temp obj
    if(eventDetails==null || eventDetails==undefined){
      if(this.espr.getEventDetailsObject().date.toString()=="" ||this.espr.getEventDetailsObject().date== undefined ){
        return false;
      }else{
        var evetMonth = new Date(this.espr.getEventDetailsObject().date).getMonth();
      }
    
    }else{
      var evetMonth = new Date(eventDetails.date).getMonth();
    }
    if(product.season == undefined){
      return false;
    }
    var offSeasonMonths : string[] = product.season.toString().split(",");
    //get date of event (get month only)
    //var month : number = parseInt(eventDetails.date.toString().split(" ")[1].toString().split["/"][0]);

  //  console.log("Offseason    "+offSeasonMonths);
   // console.log("Event month    "+evetMonth+1);

    //check for offseason match
    for(var j=0; j<offSeasonMonths.length; j++){
        if((offSeasonMonths[j].toLocaleLowerCase().includes(this.monthTable[evetMonth]))||
        (offSeasonMonths[j].toLocaleLowerCase()==(this.monthTable[evetMonth]))){
           return true;
      }
    }
    return false;
  }

 
}
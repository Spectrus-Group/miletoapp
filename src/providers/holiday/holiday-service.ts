import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import firebase from 'firebase';
import { HolidayDetails } from "../../interface/event_interfaces"

@Injectable()
export class HolidayProvider {

  holiday: HolidayDetails[] = [];
  constructor(public http: HttpClient) {
    console.log(this.holiday);
  }

  fetchHolidays() {
  return new Promise((resolve,reject)=>{
    var playersRef = firebase.database().ref('holidays/');
    var array = [];

    playersRef.on('value', function (snapshot) {
      if (snapshot.val()){
        snapshot.val().forEach(data=>{
          array.push(data);
        })
      }
      resolve(array)
    })
    this.holiday = array;
    console.log(this.holiday);
  })
  
}

}
  






import {ErrorHandler, Injectable, Injector} from '@angular/core';
import {LogService} from "./logger/log.service";
//import { Firebase } from '@ionic-native/firebase';

@Injectable()
export class AppErrorHandlerService implements ErrorHandler{


  constructor(private injector:Injector ,/* private FA : Firebase */) { }
  handleError(error:any){
      const logService = this.injector.get(LogService);
    logService.fatal((<Error>error).message,(<Error>error).stack);
   /*  //send error event to firebase
    this.FA.logEvent("Crash occured!"," Error Message: "+(<Error>error).message 
    +" Stack : "+ (<Error>error).stack);
    } */
  }
}

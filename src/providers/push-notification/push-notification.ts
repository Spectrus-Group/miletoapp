import { Injectable } from '@angular/core';
import {Push, PushObject, PushOptions} from '@ionic-native/push';
import * as firebase from 'firebase';
import {AlertController} from 'ionic-angular';

import {Events} from "ionic-angular/index";
import {environment} from "../../app/environments/environment";
import {firebaseConfig} from "../../app/credentials.backup"
@Injectable()
export class PushNotificationProvider {

  topics=[];
  pushObject:PushObject;
  constructor(private push:Push,private events:Events, public alrtCtrl : AlertController) {
  }

  pushSetUp() {
    console.log('initialising push notification..........')

    const options:PushOptions = {
      android: {
        senderID: firebaseConfig.messagingSenderId,
        sound:true,
        vibrate:true,
        forceShow:true  
      },
      ios: {
        alert: true,
        badge: true,
        sound: true
      },
      windows: {}
    };


    this.push.hasPermission()
      .then((res: any) => {

        if (res.isEnabled) {
          console.log('We have permission to send push notifications');
          this.pushObject= this.push.init(options);

          this.pushObject.on('notification').subscribe((notification:any) =>
          {

            console.log('got notofication');
            console.log(notification)
            if(!notification.additionalData.foreground){
              if(notification.additionalData.eventName==='approval'){
                this.events.publish('approval', notification.additionalData.jobId);
              }
            }else{
              let alert = this.alrtCtrl.create({
                message: 'New notification received.',
                buttons: [
                  {
                    text: 'Ok',
                    role: 'cancel',
                    handler: () => {
                      //do nothing
                    }
                  }
                ]
              });
              alert.present();
            }


          });

          this.pushObject.on('registration').subscribe((registration:any) =>{

            this.storeDeviceId(registration.registrationId);
            console.log('Device registered', registration)

            this.subscribe('blogs');

          });

          this.pushObject.on('error').subscribe(error => console.error('Error with Push plugin', error));
        } else {
          console.log('We do not have permission to send push notifications');
        }

      });
  }

  subscribe(topic:string){
    if(this.topics.indexOf(topic)===-1){
      this.topics.push(topic);
      this.pushObject.subscribe(topic).then(()=>{
        console.log('**************')
        console.log('%c subscribed to '+topic,'font-size:20px;color:green');
        console.log('**************')

      })
    }
  }

  storeDeviceId(id:string){
    let userId:string='';
    console.log('started saving reg id');
    let interval=setInterval(()=>{
      console.log('checking interval after 5 sec')
      if(firebase.auth().currentUser){
        userId = firebase.auth().currentUser.uid;
        console.log('uid is',userId);
        let ref=firebase.database().ref('/profile').child(userId).child('registrationToken');
        ref.set(id).then(()=>{
          console.log('device is saved');
          console.log('clear interval')
          clearInterval(interval);

        }).catch((err)=>{
          console.log('device id could not saved')
          console.log(err)
        })
      }


    },5000)



  }
}

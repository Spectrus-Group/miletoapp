import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IBlog } from '../../interface/blogs.interface';
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {Observable} from "rxjs/Observable";
import firebase from 'firebase';

/*
  Generated class for the BlogserviceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class BlogserviceProvider {

  private bs_blogs: BehaviorSubject<IBlog[]>;
  private blogData: IBlog[] = [];
  public o_blogs: Observable<IBlog[]>;

  private lastSeeNNots : number = 0;
  private bs_lastSeeNNots: BehaviorSubject<number>;
  public o_lastSeeNNots: Observable<number> ;

  constructor(public http: HttpClient) {
    console.log('Hello BlogserviceProvider Provider');
    this.getBlogs();
    this.getLastSeenNNotification();
  }

  private getBlogs(){
    this.bs_blogs = <BehaviorSubject<IBlog[]>> new BehaviorSubject(this.blogData);
    this.o_blogs = this.bs_blogs.asObservable();

    return new Promise((resolve, reject) => {
      var databaseRef = firebase.database().ref('blogs/');
      var array: IBlog[] = [];

      databaseRef
        .on('value', (snapshot) => {
          let snapShotVal = snapshot.val();
          if (snapShotVal) {
            array = [];

            for (let productKey in snapShotVal) {
              let blog :IBlog = snapShotVal[productKey];
              blog.createdAt = new Date(blog.createdAt).getTime().toString();
              blog.updatedAt = new Date(blog.updatedAt).getTime().toString();
              array.push(blog);
            }
          }
          //sort array according to date
          array.sort(function(a:IBlog,b:IBlog){
            if(a.updatedAt=="NaN" || b.updatedAt=="NaN"){
              if(a.createdAt>b.createdAt){
                return -1;
              }else{
                return 1;
              }
            }else{
              if(a.updatedAt>b.updatedAt){
                return -1;
              }else{
                return 1;
              }
            }
          })

          this.bs_blogs.next(array);
          resolve(array)
        },
          (error)=>{
          reject (error);
          })
    })
  }

  public setLastSeenNotifications(howMany : number){
    interface lastseennot {
      userid?: string;
      lastseennotifications? : string;
    }
    let temp : lastseennot={};
    temp.userid = firebase.auth().currentUser.uid;
    temp.lastseennotifications =howMany+"";

    if(howMany != undefined || howMany != NaN){
      this.lastSeeNNots = howMany;
      var databaseRef = firebase.database().ref('flags/lastseen/'+temp.userid+'/');
      databaseRef.set(temp);
    }
  }

  public getLastSeenNNotification() : Promise<number>{
    this.bs_lastSeeNNots = <BehaviorSubject<number>> new BehaviorSubject(this.lastSeeNNots);
    this.o_lastSeeNNots = this.bs_lastSeeNNots.asObservable();

    let isFound : boolean= false;
    let userid = firebase.auth().currentUser.uid;
    return new Promise((resolve,reject)=>{
      firebase.database().ref('flags/lastseen/') .on('value', (snapshot) => {
        let snapShotVal = snapshot.val();
        let isFound = false;
        if (snapShotVal) {
          for (let productKey in snapShotVal) {
            let data = snapShotVal[productKey];
            if(data.userid.toString()==userid.toString()){
              this.lastSeeNNots =parseInt(data.lastseennotifications.toString());
              this.bs_lastSeeNNots.next(this.lastSeeNNots);
              isFound = true;
              resolve (this.lastSeeNNots);
            }
          }
          if(!isFound){
            this.lastSeeNNots=0;
            this.bs_lastSeeNNots.next(this.lastSeeNNots);
            resolve(0);
          }
        }
      });
    })
  }

}

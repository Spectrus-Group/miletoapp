import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Loading, LoadingController} from "ionic-angular";

@Injectable()
export class LoaderProvider {

  loading:Loading;
  constructor(private loadingController: LoadingController) {
    console.log('Hello LoaderProvider Provider');
  }

  present(message:string) {
    this.loading = this.loadingController.create({
      content: message
    });

    this.loading.present();
  }


  dismiss(){
    if(this.loading){
      this.loading.dismiss();
      this.loading=null;
    }
  }

}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {APP_CONSTANT} from "../../app/constants";
import {Isettings} from "../../interface/settings.interface";
import {FingerprintAIO, FingerprintOptions} from "@ionic-native/fingerprint-aio";
import {Storage} from '@ionic/storage'

@Injectable()
export class SettingsProvider {
  fingerprintOption:FingerprintOptions = {
    clientId: 'appname',
    disableBackup: true,
    clientSecret: 'appsecret', //Only necessary for Android
    localizedFallbackTitle: 'Use Pin', //Only for iOS
    localizedReason: 'Please authenticate' //Only for iOS
  };

  settings:Isettings={pin:null,pinEnabled:false,fingerprintEnabled:false,redirectToPin:true}
  constructor(public http: HttpClient,
              private faio:FingerprintAIO,
              private storage:Storage) {

  }



  getSettings(){
    return Object.assign({},this.settings);
  }

  async  loadSettings(){
    this.settings.pin=await this.getPinFromStorage();
    this.settings.pinEnabled=!!this.settings.pin;

    this.settings.fingerprintAvailable=await this.isFingerPrintAvailable();

    this.settings.fingerprintEnabled=await this.isFingerPrintEnabled();

    this.settings.isLoggedIn= !! await  this.getTokenFromStorage();

    console.log(this.settings)
    // this will be false when user will open the barcode scanner
    // because when user open barcode scanner,app will ge in pause mode,and come to resume mode
    // and since in app.component we have subscribed to resume event,on resume we are pushing the pincode page
    // so  redirectToPin=false will avoid pushing the pin page
    return Object.assign({},this.settings);

  }



  public resetPin(){
    this.storage.remove(APP_CONSTANT.PIN);
    this.settings.pin=null;
    this.settings.pinEnabled=false;
  }


  private async getPinFromStorage():Promise<number|null>{
    let pin;
    try{
      pin= await this.storage.get(APP_CONSTANT.PIN)
    }
    catch (err){
    }
    return pin;

  }


  public setPinInStorage(pin:number):Promise<void>{
    return this.storage.set(APP_CONSTANT.PIN,pin);
  }


  private async getTokenFromStorage():Promise<string|null>{
    let token;
    try{
      token= await this.storage.get(APP_CONSTANT.ID_TOKEN)
    }
    catch (err){
    }
    return token;
  }

  // fingerprint methods
  // no need to handle error while calling these methods as these methods already handling errors
  public async isFingerPrintAvailable() {
    try {
      let isAvailable=await this.faio.isAvailable();
      console.log(isAvailable)
      return true;
    }
    catch (err) {
      console.log(err)
      return false;
    }
  }
  public async isFingerPrintEnabled(){

    let isAvailable;

    try{
      await this.isFingerPrintAvailable();
      isAvailable=true
    }

    catch (err){
      isAvailable=false
    }

    if(!isAvailable){
      try{
        await this.setFingerPrintDisabled();
        return false;
      }

      catch (err){
        return false;

      }
    }
    else {
      try {
        let val=await this.storage.get('fingerPrintEnabled');
        if(val==1){
          return true;
        }
        else {
          return false;
        }

      }
      catch (err) {
        return false;
      }
    }

  }

  public async setFingerPrintEnabled(){
    try {
      await this.storage.set('fingerPrintEnabled',1);
      return true;
    }
    catch (err) {
      return false;
    }

  }

  public async setFingerPrintDisabled(){
    try {
      await this.storage.set('fingerPrintEnabled',0);
      return true;
    }
    catch (err) {
      return false;
    }
  }

  public async fingerprintVerification() {
    try {
      let result=await this.faio.show(this.fingerprintOption);
      console.log('finger print result')
      console.log(result)
      return true;
    }
    catch (err) {
      console.log('xxx')
      console.log(err)
      return false;
    }

  }

}

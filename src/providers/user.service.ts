import {Injectable} from '@angular/core';
import {Iuser} from "../interface/user.interface";
import {UserApi} from '../app/api.config';

import {HttpClient} from "@angular/common/http";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {Storage} from '@ionic/storage'
import {HttpErrorHandlerService} from "./http-error-handler.service";

import {Events} from "ionic-angular";
import firebase from 'firebase';
import {Iprofile} from "../interface/profile.interface";
import {APP_CONSTANT} from "../app/constants";

@Injectable()
export class UserService {

  profile:Iprofile;

  constructor(private http: HttpClient,
              private events: Events,
              private errorHandler: HttpErrorHandlerService, private storage: Storage) {

    this.events.subscribe(APP_CONSTANT.LOGGED_OUT,()=>{
      this.profile=null;
    })

  }




  async getProfile(){
    let user= firebase.auth().currentUser;

    if(this.profile){
      return Promise.resolve(this.profile)
    }
    try{
      let snapshot=await firebase.database().ref(`/profile/${user.uid}`).once('value');
      if(snapshot)
        this.profile=snapshot.val();
      return Promise.resolve(this.profile)
    }
    catch (err){
      return Promise.reject(this.errorHandler.handle(err));
    }


  }


  async updateProfile(profile:Iprofile){
    let user= firebase.auth().currentUser;
    let mergedProfile= Object.assign({},this.profile,profile);
    try{
      await firebase.database().ref(`profile/${user.uid}/`).set(mergedProfile);
      this.profile= Object.assign({},mergedProfile);
      return Promise.resolve(this.profile)
    }
    catch (err){
      return Promise.reject(this.errorHandler.handle(err));
    }
  }



}

import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import {HttpClient} from "@angular/common/http";
import {FlowerDetails,EventDetails, NoteDetails, ReminderDetails,Customer} from "../../interface/event_interfaces";
import {AuthProvider} from "../../providers/auth/auth";
import firebase from 'firebase';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { Modal, Platform,NavController,Loading, } from 'ionic-angular';
import {ToastProvider} from '../../providers/toast'
import * as moment from 'moment';
import {IQoute} from '../../interface/qoute.interface'
import {Iprofile} from "../../interface/profile.interface";
import {UserService} from "../../providers/user.service";
import { IBlog } from '../../interface/blogs.interface';
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {Observable} from "rxjs/Observable";




@Injectable()
export class EventServiceProvider {
  private evantModalNavController = null;
  /**
   * This contains the last working event
   *
*/
  public lastWorkingEventId : string = "";
  /**
   * this variable will be true if evet edit mode is on
   */
  public eventEditModeFlag : boolean = false;
  /**
   * this variable is used to hold the last cliecked
   * event item's index id in {{eventListFromFireBaseBasicDet}}
   */
  public eventItemIndexClicked : number = -1;
  /**
   * this variable is used to hold the last cliecked
   * event item's id in {{eventListFromFireBaseBasicDet}}
   */
  public eventIdClicked : string;
  /**
   * this variable is used to keep track of whether any of the
   * details in event has been changed.
   */
  public isDetailsChanged : boolean = false;

  /**
   * this variable must be made true when search page is triggered.
   */
  public isSearchPageMode : boolean = false;

  /**
   * this object contains basic details of the event
   * will be updated thru the static injected singeton class
   * and will v=be available at the time of adding the venet to the
   * firebase.
   */


  public  ES_EventDetails =  {
    date: "",
    datetimestamp : "",
    phone: "",
    address:"",
    eventName:""  ,
     isOrderPlaced : "" ,
    orderType : "" ,
    deliveryAddress  : "" ,
    eventid: "",
    ordernumber:"",
    status:"",
    eventtype:""
    };
    public  RM_EventDetails =  {
      reminderId: "",
      reminderText: "",
      time:""
      };
  /**
   * this flower details are to be filled thru the fucntions
   * provided in this service like add , edit delete etc.
   * These flower detaiols are nested with the event basic details
   * aand sent to the fire base at the ime of save,
   */
  public array_flowerDetails : FlowerDetails[]=[];
  public array_reminderDetails : ReminderDetails[]=[];
  public array_noteDetails : NoteDetails[]=[];
  public eventObj : EventDetails={};
  public reminderObj : ReminderDetails={};
  public arrayCustomerDetails : Customer[] = [];
  private authObj : AuthProvider;
  public eventListFromFireBase : EventDetails[] = [];
  public o_eventistFromFirebase : Observable<EventDetails[]>
  public bs_eventlistFromFiirebase : BehaviorSubject<EventDetails[]>
  public totalFlowerCostInCart : number;
  public viaSearchPage : boolean = false;
  private userProfile : Iprofile = {};

  /**
   * to hold the date passed by any page
   * to event details page
   */
  private datePassed : Date = null;
  private searchPageModal : Modal;
  private flowerSizeModal : Modal;
  public isPopUpAciveODPage : boolean = false;
  public disableEvantModalBackButtonOverrride : Function ;
  public jumpToCartScreen : boolean ;

  public isSearchPageOpenModal : boolean = false;
  public mtst : ToastProvider;
  public isPlacedOrUpdated : boolean = false;

  public bs_isDetailsChanged: BehaviorSubject<boolean>;
  public o_isDetailsChanged: Observable<boolean>;



  constructor(private http: HttpClient,private auth:AuthProvider,
    private localNotifications :LocalNotifications, private plt: Platform,
tst : ToastProvider , public userservice : UserService) {

    this.authObj = auth;
    this.datePassed = null;
    this.eventEditModeFlag = false;
    this.isDetailsChanged = false;
    this.eventItemIndexClicked =-1;
    this.lastWorkingEventId = "";
    this.searchPageModal = null;
    this.isPopUpAciveODPage = false;
    this.jumpToCartScreen = false;
    this.isPlacedOrUpdated = false;
    this.mtst = tst;

    this.init();
  }

  private init(){
    this.bs_isDetailsChanged = <BehaviorSubject<boolean>> new BehaviorSubject(this.isDetailsChanged);
    this.o_isDetailsChanged = this.bs_isDetailsChanged.asObservable();

    //event details
    this.bs_eventlistFromFiirebase = <BehaviorSubject<EventDetails[]>> new BehaviorSubject(this.eventListFromFireBase);
    this.o_eventistFromFirebase = this.bs_eventlistFromFiirebase.asObservable();
  }

  /**
   * This method is used to add an event to the firebase db.
   * @param data it is a object whiich has key value pairs.
   */
  public addEvent(){
    let context = this;
    this.getUserProfile().then((res)=>{
       if(res==true){
        context.getOrderNumber().then((orderNo : number)=>{
          console.log("Number recived "+orderNo);
          context.addEventAfterGettingOno(orderNo);
        })
       }else{
        context.mtst.presentToast("An Error occured , Try again!")
       }
    })
  }

  private addEventAfterGettingOno(orderNo : number){
      // create the object which is to be pushed to the firebase
      this.eventObj.address= this.ES_EventDetails.address ;
      this.eventObj.phone =  this.ES_EventDetails.phone ;
      this.eventObj.date = this.ES_EventDetails.date;
      this.eventObj.orders = this.array_flowerDetails;
      this.eventObj.reminders = this.array_reminderDetails;
      this.eventObj.notes = this.array_noteDetails;
      this.eventObj.userID = this.authObj.getUser().uid ;
      this.eventObj.username = this.userProfile.name;
      this.eventObj.userphone = this.userProfile.mobile;
      this.eventObj.name =  this.ES_EventDetails.eventName ;
      this.eventObj.datetimestamp =this.ES_EventDetails.datetimestamp.toString();;
      this.eventObj.ordernumber = orderNo+"";

      if(this.eventObj.name=="" || this.eventObj.name==undefined || this.eventObj.name==null){
     //   this.eventObj.name="Unnamed";  // to save as draft
     //else dont save
       //clear the singleton object
       this.clearEventDetailsObject() ;
       //clear last working event
       this.isDetailsChanged = false;
       this.bs_isDetailsChanged.next(this.isDetailsChanged);
       this.lastWorkingEventId = "";
       this.eventEditModeFlag = false;
        return;
      }
      if(this.ES_EventDetails.isOrderPlaced=="true"){
        this.eventObj.isorderplaced = true;
      }else{
        this.eventObj.isorderplaced = false;
      }
      this.eventObj.ordertype = this.ES_EventDetails.orderType;
      this.eventObj.deliveryaddress = this.ES_EventDetails.deliveryAddress;
      this.eventObj.status = this.ES_EventDetails.status;
      this.eventObj.eventtype = this.ES_EventDetails.eventtype;

      //add the event details to the fire base
      this.pushEventDataToFirebase(this.eventObj);
      //clear the singleton object
      this.clearEventDetailsObject() ;
      //clear last working event
      this.lastWorkingEventId = "";
      this.eventEditModeFlag = false;
      console.log(this.authObj.getUser())
      console.log(this.eventObj)
  }


  public editEventAfterGettingProfile(){
      // create the object which is to be pushed to the firebase
   this.eventObj.address= this.ES_EventDetails.address;
   this.eventObj.phone =  this.ES_EventDetails.phone;
   this.eventObj.date = this.ES_EventDetails.date;
   this.eventObj.ordernumber = this.ES_EventDetails.ordernumber;
   this.eventObj.orders = this.array_flowerDetails;
   this.eventObj.reminders = this.array_reminderDetails;
  this.eventObj.notes = this.array_noteDetails;
   this.eventObj.userID = this.authObj.getUser().uid ;
   this.eventObj.username = this.userProfile.name;
   this.eventObj.userphone = this.userProfile.mobile;
   this.eventObj.name =  this.ES_EventDetails.eventName ;
   this.eventObj.datetimestamp =this.ES_EventDetails.datetimestamp.toString();
   console.log(this.eventObj);
   if(this.eventObj.name=="" || this.eventObj.name==undefined || this.eventObj.name==null){
     //   this.eventObj.name="Unnamed";  // to save as draft
     //else dont save
       //clear the singleton object
       this.clearEventDetailsObject() ;
       //clear last working event
       this.isDetailsChanged = false;
       this.bs_isDetailsChanged.next(this.isDetailsChanged);
       this.lastWorkingEventId = "";
       this.eventEditModeFlag = false;
     return;
  }

   if(this.ES_EventDetails.isOrderPlaced=="true"){
    this.eventObj.isorderplaced = true;
  }else{
    this.eventObj.isorderplaced = false;
  }
  this.eventObj.ordertype = this.ES_EventDetails.orderType;
  this.eventObj.deliveryaddress = this.ES_EventDetails.deliveryAddress;
  this.eventObj.status = this.ES_EventDetails.status;
  this.eventObj.eventtype = this.ES_EventDetails.eventtype;
   //edit event in firebase
   this.editEventDataToFirebase(this.eventObj);
    //clear the singleton object
    this.clearEventDetailsObject() ;
    //clear last working event
    this.lastWorkingEventId = "";

    //Edit / save
  this.clearEventDetailsObject();
  this.eventEditModeFlag = false;
  this.isDetailsChanged=true;
  this.bs_isDetailsChanged.next(this.isDetailsChanged);
  }
  /**
   * this method is used to edit the event
   */
  public editEvent(){
    let context = this;
    this.eventObj.eventId = this.eventListFromFireBase[this.eventItemIndexClicked].eventId;
    this.getUserProfile().then((res)=>{
       if(res==true){
        context.editEventAfterGettingProfile();
       }else{
        context.mtst.presentToast("An Error occured , Try again!")
       }
    })
  }

  /**
   * this method is used to get the event details object
   */
  public getEventDetailsObject() : any{
    this.isDetailsChanged = true;
    this.bs_isDetailsChanged.next(this.isDetailsChanged);
    return this.ES_EventDetails;
  }
  /**
   * this method is used to clear the
   * evet details object
   */
  public clearEventDetailsObject() {
    this.isDetailsChanged = false;
    this.bs_isDetailsChanged.next(this.isDetailsChanged);
    this.ES_EventDetails.address="";
    this.ES_EventDetails.date="";
    this.ES_EventDetails.eventName="";
    this.ES_EventDetails.phone="";
    this.ES_EventDetails.isOrderPlaced="false";
    this.ES_EventDetails.orderType="";
    this.ES_EventDetails.deliveryAddress = "";
    this.ES_EventDetails.eventid="";
    this.ES_EventDetails.status="";
    this.ES_EventDetails.eventtype="";
    this.ES_EventDetails.datetimestamp="";
    this.array_flowerDetails=[];
    this.array_reminderDetails=[];
    this.array_noteDetails=[];
    this.isPlacedOrUpdated = false;
  }

  /**
   * to get all the flowers in the order list
   */
  public getFlowerOrderList() : FlowerDetails[] {
    this.isDetailsChanged = true;
    this.bs_isDetailsChanged.next(this.isDetailsChanged);
    return this.array_flowerDetails;
  }
  /**
   *
   * @param flowerdet FlowerDetails
   * this method is used to add the flower in the
   * flower list
   */
  public addFlowerToOrderList(flowerdet : FlowerDetails){
    this.isDetailsChanged = true;
    this.bs_isDetailsChanged.next(this.isDetailsChanged);
    this.array_flowerDetails.push(flowerdet);
    this.calc_totalFlowerCostInCart();
  }
  /**
   *
   * @param remdet ReminderDetails
   * this method is used to add the Reminder in the
   * more tab
   */
  public addReminderToEvent(remdet : ReminderDetails){
    this.isDetailsChanged = true;
    this.bs_isDetailsChanged.next(this.isDetailsChanged);
    this.array_reminderDetails.push(remdet);
  }
  /**
   * This method is used to edit the reminder in the more tab
   * @param reminderDetails  reminder details obj
   */
  editReminderEvent(reminderDetails: ReminderDetails){
      this.isDetailsChanged=true;
      this.bs_isDetailsChanged.next(this.isDetailsChanged);
      for(var i=0; i<this.array_reminderDetails.length; i++){
        if(this.array_reminderDetails[i].reminderId == reminderDetails.reminderId ){
          this.array_reminderDetails[i].time = reminderDetails.time;
          this.array_reminderDetails[i].reminderText = reminderDetails.reminderText;
          break;

        }
      }

  }
  /**
   * This method will delete the reminder with its reminderid
   * in the more tab.
   * @param reminder   ReminderDetails
   */
  public deleteReminderEvent(reminder : ReminderDetails){
    //remove from arra list
    const index = this.array_reminderDetails.indexOf(reminder);
    if (index !== -1) {
      this.array_reminderDetails.splice(index, 1);
     this.isDetailsChanged = true;
     this.bs_isDetailsChanged.next(this.isDetailsChanged);
     }
  }
   /**
   *
   * @param notedet noteDetails
   * this method is used to add the note in the
   * more tab
   */
  public addNoteToEvent(notedet : NoteDetails){
    this.isDetailsChanged = true;
    this.bs_isDetailsChanged.next(this.isDetailsChanged);
    this.array_noteDetails.push(notedet);
  }
 /**
   * This method is used to edit the Note in the more tab
   * @param noteDetails  Note details obj
   */
  editNoteEvent(noteDetails: NoteDetails){
    this.isDetailsChanged=true;
    for(var i=0; i<this.array_noteDetails.length; i++){
      if(this.array_noteDetails[i].noteId == noteDetails.noteId ){
        this.array_noteDetails[i].time = noteDetails.time;
        this.array_noteDetails[i].noteText = noteDetails.noteText;
        break;
      }
    }
}
 /**
   * This method will delete the reminder with its noteid
   * in the more tab.
   * @param note   NoteDetails
   */
  public deleteNoteEvent(note : NoteDetails){
    //remove from arra list
    const index = this.array_noteDetails.indexOf(note);
    if (index !== -1) {
      this.array_noteDetails.splice(index, 1);
     this.isDetailsChanged = true;
     this.bs_isDetailsChanged.next(this.isDetailsChanged);
     }
  }

  /**
   * This method will delete the flower with its flower id
   * in the order list.
   * @param flower   FlowerDetails
   */
  public deleteFlowerInOrderList(flower : FlowerDetails){
    //remove from arra list
    const index = this.array_flowerDetails.indexOf(flower);
    if (index !== -1) {
      this.array_flowerDetails.splice(index, 1);
     this.isDetailsChanged = true;
     this.bs_isDetailsChanged.next(this.isDetailsChanged);
     this.calc_totalFlowerCostInCart();
     }
  }
  /**
   *
   * @param id this methd will add comment to flower
   */
  public addCommentToFlower(prod : FlowerDetails){
    this.isDetailsChanged = true;
    this.bs_isDetailsChanged.next(this.isDetailsChanged);
   for(var i=0; i<this.array_flowerDetails.length; i++){
     if(prod.flowerId == this.array_flowerDetails[i].flowerId &&
      prod.size ==this.array_flowerDetails[i].size ){
      this.array_flowerDetails[i].comment = prod.comment;
      console.log("found "+prod.comment);
      break;
     }
     console.log("not found "+prod.comment);
     console.log("not found ID1 "+prod.flowerId+"Id 2 "+ this.array_flowerDetails[i].flowerId );
   }
  }

  public changeSpecialOrderStatus(prod: FlowerDetails){
    this.isDetailsChanged = true;
    this.bs_isDetailsChanged.next(this.isDetailsChanged);
    for(var i=0; i<this.array_flowerDetails.length; i++){
      if(prod.flowerId == this.array_flowerDetails[i].flowerId &&
       prod.size ==this.array_flowerDetails[i].size ){
       this.array_flowerDetails[i].isSpecial = prod.isSpecial;
       break;
      }
    }
  }
  /**
   * This method is used to get a flower from the
   * order list
   * @param flowerId
   */
  public getFlowerFromId(flowerId :String): FlowerDetails{
    this.isDetailsChanged = true;
    this.bs_isDetailsChanged.next(this.isDetailsChanged);
     return null;
  }

  /**
   * This method is used to edit the flower in the order list
   * @param flowerDetails  flower details obj
   */
  public editFlowerInOrderList(flowerDetails : FlowerDetails){
    for(var i=0; i<this.array_flowerDetails.length; i++){
      if(this.array_flowerDetails[i].flowerId == flowerDetails.flowerId &&
         this.array_flowerDetails[i].size == flowerDetails.size){
        this.array_flowerDetails[i].qty = flowerDetails.qty;
        break;
      }
    }

    this.isDetailsChanged = true;
    this.bs_isDetailsChanged.next(this.isDetailsChanged);
    this.calc_totalFlowerCostInCart();
  }
  /**
   *
   * @param event this method push the event object to the firebase table
   */
  private pushEventDataToFirebase(event :any){
    this.isDetailsChanged = false;
    this.bs_isDetailsChanged.next(this.isDetailsChanged);
    firebase.database().ref('event/').push(event);
    console.log("Adding event");
  }

  /**
   *
   * @param event this method edit the event object in the firebase table
   */
  private editEventDataToFirebase(event :EventDetails){
    this.isDetailsChanged = false;
    this.bs_isDetailsChanged.next(this.isDetailsChanged);
    try{
      firebase.database().ref('event/'+event.eventId+"/").set(event);
      console.log("Editng event");
    }catch (EX ){
      console.log(EX);
    }

  }

  /**
   * this method loads all the events of current user on
   * eventListFromFireBase variable .
   */
  public getAllEventsDetails(){
  var context = this;
  var userId = firebase.auth().currentUser.uid;
  var playersRef = firebase.database().ref("event/");
  //load all events for current user
  var userIDsorted = playersRef.orderByChild('userID').equalTo(userId);

  //map event json to event details objects
  userIDsorted.on("value", function(snapshot) {
    //old array size
     var eventsSizeBeforeUpdate = context.eventListFromFireBase.length;
    if(snapshot != null){
      snapshot.forEach(function(childSnapshot):boolean {

        //to hold event temporarily
        var tempEventObj : EventDetails={};
        tempEventObj.username = childSnapshot.child("username").val();
        tempEventObj.userphone = childSnapshot.child("userphone").val();
        tempEventObj.eventId = childSnapshot.key;
        tempEventObj.address= childSnapshot.child("address").val();
        tempEventObj.date = childSnapshot.child("date").val();
        tempEventObj.datetimestamp = childSnapshot.child("datetimestamp").val();
        if(tempEventObj.datetimestamp== undefined || tempEventObj.datetimestamp==""){
          tempEventObj.datetimestamp = new Date(tempEventObj.date).getTime().toString();
        }
        tempEventObj.name = childSnapshot.child("name").val();
        tempEventObj.phone = childSnapshot.child("phone").val();
        tempEventObj.ordernumber = childSnapshot.child("ordernumber").val();


        if(childSnapshot.child("isorderplaced").val() ){
          tempEventObj.isorderplaced = true;
        }else{
          tempEventObj.isorderplaced = false;
        }
        tempEventObj.ordertype = childSnapshot.child("ordertype").val();
        tempEventObj.deliveryaddress = childSnapshot.child('deliveryaddress').val();
        tempEventObj.eventtype = childSnapshot.child('eventtype').val();
        if(tempEventObj.eventtype==undefined || tempEventObj.eventtype==null || tempEventObj.eventtype==""){
          tempEventObj.eventtype = "new";
        }
        tempEventObj.status = childSnapshot.child('status').val();
        if(tempEventObj.status==undefined || tempEventObj.status==null || tempEventObj.status==""){
          tempEventObj.status = "pending";
        }
        tempEventObj.orders = []; //initialise
        tempEventObj.reminders=[];
        tempEventObj.notes=[];
        //get flower details if it has
        if(childSnapshot.child("orders").numChildren()>0){
          childSnapshot.child("orders").forEach(function(childSnapshot1):boolean{
             var tempOrderObj1 : FlowerDetails ={};
             tempOrderObj1.flowerId = childSnapshot1.child("flowerId").val();
             tempOrderObj1.groupid =  childSnapshot1.child("groupid").val();
             tempOrderObj1.qty = childSnapshot1.child("qty").val();
             tempOrderObj1.size = childSnapshot1.child("size").val();
             tempOrderObj1.optionvar2 = childSnapshot1.child("optionvar2").val();
             tempOrderObj1.optionvar3 = childSnapshot1.child("optionvar3").val();
             tempOrderObj1.url =   childSnapshot1.child("url").val();
             tempOrderObj1.cost = childSnapshot1.child("cost").val();
             tempOrderObj1.name = childSnapshot1.child("name").val();
             tempOrderObj1.offSeason = childSnapshot1.child("offSeason").val();
             tempOrderObj1.comment = childSnapshot1.child("comment").val();
             tempOrderObj1.color = childSnapshot1.child("color").val();
             tempOrderObj1.type = childSnapshot1.child("type").val();
             tempOrderObj1.isSpecial = childSnapshot1.child("isSpecial").val();

             //push to temp obj 1
             tempEventObj.orders.push(tempOrderObj1);
             return false;
          });
        }
         //get reminder details if it has
        if(childSnapshot.child("reminders").numChildren()>0){
          childSnapshot.child("reminders").forEach(function(childSnapshot2):boolean{
             var tempOrderObj2 : ReminderDetails ={};
             tempOrderObj2.reminderId = childSnapshot2.child("reminderId").val();
             tempOrderObj2.time = childSnapshot2.child("time").val();
             tempOrderObj2.reminderText = childSnapshot2.child("reminderText").val();
             //push to temp obj 1
             tempEventObj.reminders.push(tempOrderObj2);
             return false;
          });
        }
        //get note details if it has
        if(childSnapshot.child("notes").numChildren()>0){
          childSnapshot.child("notes").forEach(function(childSnapshot3):boolean{
             var tempOrderObj3 : NoteDetails ={};
             tempOrderObj3.noteId = childSnapshot3.child("noteId").val();
             tempOrderObj3.time = childSnapshot3.child("time").val();
             tempOrderObj3.noteText = childSnapshot3.child("noteText").val();
             //push to temp obj 1
             tempEventObj.notes.push(tempOrderObj3);
             return false;
          });
        }
         //  shift and remove old element then push new element to main array
       if(eventsSizeBeforeUpdate>0){
       context.eventListFromFireBase.shift();
       eventsSizeBeforeUpdate--;
       }
        context.eventListFromFireBase.push(tempEventObj );
  // console.log("Event list size "+context.eventListFromFireBase.length);
        return false;
    });
     //flush the previoulsy stored events in the array
   while(eventsSizeBeforeUpdate>0){
    context.eventListFromFireBase.shift();
    eventsSizeBeforeUpdate--;
    console.log("Event list size "+context.eventListFromFireBase.length + " "+eventsSizeBeforeUpdate);
   }
    //sorting the array
    context.eventListFromFireBase.sort(function(a:EventDetails, b:EventDetails){
      try{
      var ss_ayear : Number = new Date().getTime();
      var ss_byear : Number = new Date().getTime();
      try{
         ss_ayear = new Date(a.date).getTime();
         ss_byear =  new Date(b.date).getTime();
      }catch(e1){
        console.log(e1);
      }

      if(ss_ayear>ss_byear){
          return 1;
       }else
       {
          return -1;
        }
    }catch(e){
      console.log(e);
    }
    });
    context.bs_eventlistFromFiirebase.next(context.eventListFromFireBase);
  }

});
}

 /**
  *
  * this method is used to set the date which was passed from view events
  */
 public setDate(mdatePassed: Date) {
    this.datePassed=mdatePassed;
 }
/**
   * this method is used to get the date from view events
   */
 public getDate() : Date {
   return this.datePassed;
 }
 /**
  * this metthof is used to clear the
  * passed date.
  */
 public clearDate(){
   this.datePassed= null;
 }

 public getEventObjectFromMasterArray(indexOfEvent : number) : EventDetails{
   //load flower details
   try{

     return  this.eventListFromFireBase[indexOfEvent];
   }catch(E){
     return null;
   }
 }

 public getEventObjectFromMasterArrayByKey(Eventkey : String) : EventDetails{
  try{
    for(var i=0; i<this.eventListFromFireBase.length; i++){
      if(this.eventListFromFireBase[i].eventId==Eventkey){
        return this.eventListFromFireBase[i];
      }
    }
    return null;
  }catch(E){
    return null;
  }
}

 public deleteEventFromDatabase(eventId : string ){
  var ref =firebase.database().ref("event/"+eventId+"/"); //root reference to your data
  ref.remove( function(snapshot) {
  try{
 //clear last working event
 this.lastWorkingEventId = "";
  }catch(e){}

})}
/**
 * this will calculate the tota flower cost in cart
 */
public calc_totalFlowerCostInCart(){
  console.log("Total cost ");
  this.totalFlowerCostInCart=0;
  for(var i =0; i<this.array_flowerDetails.length;i++){
    try{
      this.totalFlowerCostInCart = this.totalFlowerCostInCart +
      (parseInt(this.array_flowerDetails[i].qty) * parseFloat(this.array_flowerDetails[i].cost));
    }catch(ex){}
  }
}

public setSearchPageModal(modal : Modal){
  this.searchPageModal = modal;
}

public getSearchPageModal() : Modal{
  var temp : Modal = this.searchPageModal;
  this.searchPageModal = null;
  return temp;
}
public setFlowerSizeModal(modal : Modal){
  this.flowerSizeModal = modal;
}

public getFlowerSizeModal() : Modal{
 var temp : Modal = this.flowerSizeModal;
 this.flowerSizeModal = null;
 return temp;
}


public retriveEntireEvent(evId:string){
   var eventClicked : EventDetails = this.getEventObjectFromMasterArrayByKey(evId);
   this.lastWorkingEventId = evId;

    //set event basic details
    console.log("Event passed is "+eventClicked.name);
    var eventName = eventClicked.name;
    var phone = eventClicked.phone;
    var date =eventClicked.date;
    var address = eventClicked.address;
     //update values in singleton class
    this.ES_EventDetails.eventName = eventName;
    this.ES_EventDetails.phone = phone;
    this.ES_EventDetails.date = date;
    this.ES_EventDetails.datetimestamp = eventClicked.datetimestamp+"";
    this.ES_EventDetails.address = address;
    this.ES_EventDetails.deliveryAddress = eventClicked.deliveryaddress;
    this.ES_EventDetails.eventid = eventClicked.eventId;
    this.ES_EventDetails.ordernumber = eventClicked.ordernumber;
    if(eventClicked.isorderplaced){
      this.ES_EventDetails.isOrderPlaced = "true";
    }else{
      this.ES_EventDetails.isOrderPlaced = "false";
    }
    this.ES_EventDetails.orderType =  eventClicked.ordertype;
    this.ES_EventDetails.eventtype =  eventClicked.eventtype;
    this.ES_EventDetails.status =  eventClicked.status;

    this.isDetailsChanged=false;
    this.bs_isDetailsChanged.next(this.isDetailsChanged);
   //load flower details
   this.array_flowerDetails  = eventClicked.orders;
   //load reminders
   this.array_reminderDetails = eventClicked.reminders;
   //load notes
   this.array_noteDetails = eventClicked.notes;
}
/**
 * this method is used to edit and save the event explicitly
 */
public editEventExplicitly(){
  let eventTemp : EventDetails = this.getEventObjectFromMasterArrayByKey(this.ES_EventDetails.eventid);
  if((
    eventTemp.status=="complete"||
    eventTemp.status=="cancelled")){
      this.mtst.presentToast("The order status have been changed to completed while you were working! Cannot edit!");
      this.isDetailsChanged=false;
      return;
    }else{
      this.calc_totalFlowerCostInCart();
    //SInce because order is updated for sure
    this.ES_EventDetails.eventtype = "update";
    this.ES_EventDetails.status = "pending";

      this.editEvent();
    }
}

/**
 * this method is used to retrieve all the customers of user
 */
public retrieveCustomers(){
  /*
  var context = this;
  var userId = firebase.auth().currentUser.uid;
  var playersRef = firebase.database().ref("customer/");
  //load all events for current user
  var userIDsorted = playersRef.orderByChild('userid').equalTo(userId);
  //map event json to event details objects
  userIDsorted.on("value", function(snapshot) {
    context.arrayCustomerDetails=[];
    if(snapshot != null){
      snapshot.forEach(function(childSnapshot):boolean {
      //to hold event temporarily
      var tempEventObj : Customer={};
      tempEventObj.id = childSnapshot.key;
      tempEventObj.address= childSnapshot.child("address").val();
      tempEventObj.name = childSnapshot.child("name").val();
      tempEventObj.phone = childSnapshot.child("phone").val();
      tempEventObj.userid = childSnapshot.child("userid").val();
      context.arrayCustomerDetails.push(tempEventObj);
      console.log("Customers "+tempEventObj.name)
      return false;
      });
    }
  })  */
  //get the events from the event list form firebase
  //filter by phone number
  return new Promise((resolve,reject)=>{
    let userId =firebase.auth().currentUser.uid;
    for(let i=0; i<this.eventListFromFireBase.length; i++){
      let event : EventDetails = this.eventListFromFireBase[i];
      //check from corrupt events
      if(event.name.toString().toLocaleLowerCase() == "unnamed"){
        continue;
      }
      //check if phone already exists in the customer slist
     let isALreadyPresent = this.arrayCustomerDetails.find(cust=>{
        if(event.phone.toString()==cust.phone.toString()){
          return true;
        }
     })
     console.log(isALreadyPresent)
     if(!isALreadyPresent){
       let temp : Customer={};
       temp.id = i.toString();
       temp.address = event.address;
       temp.name = event.name;
       temp.phone = event.phone;
       temp.userid = userId;
        this.arrayCustomerDetails.push(temp);
     }
    }
  });

}


public addCustomerToFirebase(name : string , phone : string , address : string){
    var tempObj : Customer={};
    tempObj.name = name;
    tempObj.phone = phone;
    tempObj.address = address;
    tempObj.userid = firebase.auth().currentUser.uid;
    firebase.database().ref('customer/').push(tempObj);
}


/**
 * Local Notifications
/**
   * This method will schedule the reminder
   * @param schReminder
   */
  scheduleNewReminder(schReminder :ReminderDetails){
    let id = this.localNotifications.getAll();
    console.log('yekk',id);
    var reminderj = schReminder;
    var eventName = this.ES_EventDetails.eventName.concat(" | ",moment(this.ES_EventDetails.date).format("MMMM Do YYYY"));
    let stime = new Date(reminderj.time);
      this.localNotifications.schedule({
        id: parseInt(reminderj.reminderId),
        title:eventName,
        text: reminderj.reminderText,
        trigger: {at: stime},
        data: this.ES_EventDetails.eventid,
        color: '7986CC',
        led: '7986CC',
        sticky: false,
      });

    }
    /**
   * This method will update the reminder
   * @param updReminder
   */
    updateExistingReminder(updReminder :ReminderDetails){
      let id = this.localNotifications.getAll();
      console.log('yekk',id);
      var reminderj = updReminder;
      var eventName = this.ES_EventDetails.eventName.concat(" | ",moment(this.ES_EventDetails.date).format("MMMM Do YYYY"));
      let stime = new Date(reminderj.time);
        this.localNotifications.schedule({
          id: parseInt(reminderj.reminderId),
          title:eventName,
          text: reminderj.reminderText,
          trigger: {at: stime},
          data: this.ES_EventDetails.eventid,
          color: '7986CC',
        led: '7986CC',
        sticky: false,
        });

    }
     /**
   * This method will cancel the reminder
   * @param cclReminder
   */
  cancelExistingReminder(cclReminder :ReminderDetails){
      var reminderj = cclReminder;
      this.localNotifications.cancel(parseInt(reminderj.reminderId));
    }
    /**
     * this method is used to delete the custoemr
     */
    deleteCustomer(cust : Customer){
        var ref =firebase.database().ref("customer/"+cust.id+"/"); //root reference to your data
        ref.remove( function(snapshot) {
       })
  }


  public getOrderNumber() : Promise<number> {
    return new Promise((resolve,reject)=>{
    firebase.database().ref('/serialnos/').once('value').then((snapshot)=>{
       snapshot.forEach(element => {
           if(element.val().id.toString()=="orderno"){
             let temp = element.val().sno;
            this.incrementOrderNo(parseInt(temp));
             resolve(element.val().sno);
           }
       });
       resolve(0);
    });
   });
  }

  private incrementOrderNo(currSno : number){
    currSno++;
    //reset after 9999999999
    if(currSno>9999999999){
      currSno=1;
    }
    firebase.database().ref('/serialnos/orderno/sno').set(currSno);
  }

  public sendQoute(qouteDet : IQoute){
    //check if qoute field is already present
    firebase.database().ref('/qoute/'+qouteDet.user).set(qouteDet).then(data=>{
      console.log(data);
    });
  }

  private getUserProfile(){
    return new Promise((resolve,reject)=>{
      this.userservice.getProfile().then((profile:Iprofile)=>{
        this.userProfile=profile;
        if(this.userProfile.name == undefined){
          this.userProfile.name = this.authObj.getUser().email;
        }

        if(this.userProfile.mobile== undefined){
          this.userProfile.mobile = " ";
        }
        resolve(true)
      }).catch((err)=>{
        resolve(false)
      })
    })

  }

  public setEvantModalNavController(navCtrl){
    this.evantModalNavController = navCtrl;
  }

  public getEvantModalNavController(){
    return this.evantModalNavController;
  }

}

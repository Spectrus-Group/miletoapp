import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { EventServiceProvider } from '../event-service/event-service';
import {  ReminderDetails, EventDetails } from '../../interface/event_interfaces';
import firebase from 'firebase';
import { LocalNotifications } from '@ionic-native/local-notifications';
import * as moment from 'moment';
/*
  Generated class for the ReminderServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ReminderServiceProvider {

  public eventListFromFireBase : EventDetails[]= [];

  constructor(public http: HttpClient, public espr: EventServiceProvider, private localNotifications :LocalNotifications) {
    console.log('Hello ReminderServiceProvider Provider');
    this.eventListFromFireBase=[];
    
  }

 /**
   * this method loads all the events of current user on 
   * eventListFromFireBase variable .
   */
  public getAllReminderDetails(){  
    var context = this;  
    var userId = firebase.auth().currentUser.uid;
    var playersRef = firebase.database().ref("event/");
    //load all events for current user
    var userIDsorted = playersRef.orderByChild('userID').equalTo(userId);
  
    //map event json to event details objects
    userIDsorted.on("value", function(snapshot) {
    //old array size
    var eventsSizeBeforeUpdate = context.eventListFromFireBase.length;
      if(snapshot != null){
        snapshot.forEach(function(childSnapshot):boolean {
          
          //to hold event temporarily
          var tempEventObj : EventDetails={};
          tempEventObj.eventId = childSnapshot.key;
          tempEventObj.date = childSnapshot.child("date").val();
          tempEventObj.name = childSnapshot.child("name").val();
          tempEventObj.reminders=[];
           //get reminder details if it has
          if(childSnapshot.child("reminders").numChildren()>0){
            childSnapshot.child("reminders").forEach(function(childSnapshot2):boolean{
               var tempOrderObj2 : ReminderDetails ={};
               tempOrderObj2.reminderId = childSnapshot2.child("reminderId").val();
               tempOrderObj2.time = childSnapshot2.child("time").val();
               tempOrderObj2.reminderText = childSnapshot2.child("reminderText").val();
               
               //push to temp obj 1
               tempEventObj.reminders.push(tempOrderObj2);
               return false;
            });
          }
          //  shift and remove old element then push new element to main array
         if(eventsSizeBeforeUpdate>0){
         context.eventListFromFireBase.shift();
         eventsSizeBeforeUpdate--;
         }
          context.eventListFromFireBase.push(tempEventObj);
          console.log("Event list size "+context.eventListFromFireBase.length + " "+eventsSizeBeforeUpdate);
          return false;
      });
      //flush the previoulsy stored events in the array
     while(eventsSizeBeforeUpdate>0){
      context.eventListFromFireBase.shift();
      eventsSizeBeforeUpdate--;
      console.log("Event list size "+context.eventListFromFireBase.length + " "+eventsSizeBeforeUpdate);
     }
      //soring the array
      context.scheduleAllReminders();
      
    }
  });
  }
  
  scheduleAllReminders(){
    console.log('len:',this.eventListFromFireBase.length,this.eventListFromFireBase)
 for(let i =0;i<this.eventListFromFireBase.length;i++){


    for(let j =0;j<this.eventListFromFireBase[i].reminders.length;j++){
      if(new Date(this.eventListFromFireBase[i].reminders[j].time).getTime()>new Date().getTime()){
        var eventName = this.eventListFromFireBase[i].name.concat(" | ",moment(this.eventListFromFireBase[i].date).format("MMMM Do YYYY"));
        this.localNotifications.schedule({
          id: parseInt(this.eventListFromFireBase[i].reminders[j].reminderId),
          title:eventName,
          text: this.eventListFromFireBase[i].reminders[j].reminderText,
          trigger:{at: new Date(this.eventListFromFireBase[i].reminders[j].time)},
          data: this.eventListFromFireBase[i].eventId,
          color: '7986CC',
        led: '7986CC',
        sticky: true,
        icon: 'http://icons.iconarchive.com/icons/icons8/windows-8/16/Industry-Gas-Mask-icon.png'
        });
      }
      

    
    
  }


   

  }
   


     
 

    
}
}

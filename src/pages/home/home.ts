import {Component} from '@angular/core';
import {IonicPage, NavController, Platform, AlertController} from 'ionic-angular';
import {EventServiceProvider} from "../../providers/event-service/event-service";
import {HttpClient} from "@angular/common/http";
import {EventDetails, HolidayDetails} from "../../interface/event_interfaces"
import {FirebaseDataProvider} from "../../providers/firebase-data/firebase-data";
import {Icategory, ICategorySchema} from "../../interface/category.interface";
import {HolidayProvider} from "../../providers/holiday/holiday-service";
import {ProductsServiceProvider} from "../../providers/products-service/products-service"
import {Iproduct} from "../../interface/product.interface";
import {LocalNotifications} from '@ionic-native/local-notifications';
import {ReminderServiceProvider} from '../../providers/reminder-service/reminder-service';
import * as moment from 'moment';
import { IBlog } from '../../interface/blogs.interface';
import {BlogserviceProvider} from '../../providers/blogservice/blogservice'

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {

  eventList: Array<EventDetails> = [];
  category: Icategory;
  holidays: Array<HolidayDetails> = [];
  public NumberOfNewNot : number = 0;

  favourite: string[] = [];

  resultData: Iproduct[] = [];

  categoryProcess:string = "pending";
  favouriteProcess:string = "pending";
  featuredProcess:string = "pending";
  timestampNow : number =0;
  public monthTable : string[] = ["jan","feb","mar","apr","may","jun","jul","aug","sep","oct","nov","dec"];


  constructor(public navCtrl: NavController,
              public http: HttpClient,
              public eventService: EventServiceProvider,
              private firebaseData: FirebaseDataProvider,
              public holi: HolidayProvider,
              public psp: ProductsServiceProvider,
              public remService: ReminderServiceProvider,
              public BlogService : BlogserviceProvider) {

    this.timestampNow = moment().subtract(1, 'days').unix()*1000;
    this.NumberOfNewNot = 0;

  }


  ionViewDidLoad() {
    new Promise((resolve,reject)=>{
    //get all events basic details
    this.eventService.getAllEventsDetails();
    this.remService.getAllReminderDetails();

    //map events details variable to this class's variable
    this.getEvents();
    this.getHolidays();
    this.getProducts();
    this.eventService.retrieveCustomers();
    this.firebaseData.getStoreDetails();

    this.firebaseData.category.subscribe((category: Icategory) => {
      this.category = category;
      this.categoryProcess = 'resolved';
    //  console.log(this.category);
    });

    this.firebaseData.product.subscribe((data: Iproduct[]) => {
      this.resultData = data;

      /**Filtering Featured*/
      this.category.featured = [];
      /**Filtering seasonal*/
      this.category.seasonal = [];
      /**Filtering for all round the year*/
      this.category.roundAYear = [];
    //  console.log("Start product filter home . ts ......................"+new Date().getTime())
      this.resultData.forEach((data) => {
        if (data.product.featured) {
          this.category.featured.push({
            id: data.id,
            name: data.product.name,
            hex: '#C0C0C0',
            url: data.product.url_medium,
            isOffSeason : data.product.isOffSeason
            
          });
        }

        let index: number;

        if (data.product.season!= undefined && data.product.season.length>0) {
          if((data.product.season.length==1 && data.product.season[0]=="")){
            index=-1;
          }else{
            index=1;
          }
        } else {
          index = -1;
        }
        //console.log(data.product.season)
        //console.log(index)
        if (index == 1) {
          this.category.seasonal.push({
            id: data.id,
            name: data.product.name,
            hex: '#C0C0C0',
            url: data.product.url_medium,
            isOffSeason : data.product.isOffSeason
          });
        }
        /* Thre is naming discrebency here actualy we show all the flowers here */
        this.category.roundAYear.push({
          id: data.id,
          name: data.product.name,
          hex: '#C0C0C0',
          url: data.product.url_medium,
          isOffSeason : data.product.isOffSeason
        });
      })
     // console.log("End product filter home .ts ......................"+new Date().getTime())
      this.featuredProcess = 'resolved'
    })

    this.firebaseData.favourite.subscribe((data: string[]) => {
      this.favourite = data;

      this.category.favourite = [];

      this.resultData.forEach((data) => {
        let isFavorite = this.favourite.indexOf(data.id);
        if (isFavorite > -1) {
          data.isFavorite = true;
          this.category.favourite.push({
            id: data.id,
            name: data.product.name,
            hex: '#C0C0C0',
            url: data.product.url_medium,
            isOffSeason : data.product.isOffSeason
          });
        }
      })
      this.favouriteProcess = 'resolved';
    });
  })
  this.calculateNumberOfNewNots();
  }


  ionViewDidAppear() {
    this.getEvents();
    this.timestampNow = new Date().getTime();
  }

  getProducts() {
    this.psp.getAllProductsFromDb();
  }

  getEvents() {
    this.eventList = this.eventService.eventListFromFireBase;
  }

  getHolidays() {
    this.holi.fetchHolidays();
    this.holidays = this.holi.holiday;
 //   console.log('holiday', this.holi.holiday);
  }

  viewEventsPage() {
    this.navCtrl.push('ViewEventsPage');
  }

  viewNewEventPage() {
    this.navCtrl.push('EventModalPage');
  }

  viewDetailsPage(itemIndex: number) {
  //  console.log("item clicked" + itemIndex);
    //edit mode
    this.eventService.eventEditModeFlag = true;
    //set clicked items index
    this.eventService.eventItemIndexClicked = itemIndex;
    //go to evant  modal page but with edit flag on
    this.navCtrl.push('EventModalPage');
  }

  clickedSearchIcon() {
    this.navCtrl.push('SearchPage', {category: 'any', keyForSearch: 'any'});
  }

  viewAllPage(color, category, title, categoryData) {
    this.navCtrl.push('ViewAllPage', {color, category, title, categoryData,shallDisplayBackButton:false}); 
  }

  openSearch(cat: ICategorySchema, category: string) {
  //  console.log(cat);
    this.navCtrl.push('SearchPage', {data: cat, category})
  }

  goToNotification(){
    this.navCtrl.push('NotificationsPage');
  }

  private calculateNumberOfNewNots(){
    let blogData = [];
    this.BlogService.o_blogs.subscribe((data)=>{
      blogData = data;

      //check  for unread messages
      this.BlogService.getLastSeenNNotification().then((data)=>{

      let currentNumberOfNots = blogData.length;

      this.BlogService.o_lastSeeNNots.subscribe((data)=>{
        this.NumberOfNewNot =currentNumberOfNots -data;
       // console.log("Changed "+ this.NumberOfNewNot);
      }) ;
      })
    })
  }
}

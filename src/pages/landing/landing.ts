import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {UserService} from "../../providers/user.service";
import {LogService} from "../../providers/logger/log.service";

@IonicPage()
@Component({
  selector: 'page-landing',
  templateUrl: 'landing.html',
})
export class LandingPage {

  constructor(public navCtrl: NavController,
              private logService:LogService,public navParams: NavParams,private userService:UserService) {

  }

  ionViewDidLoad() {
    console.log(this.navCtrl.length())
  }




  goToSignupPage(){

    this.navCtrl.push('SignupPage',{},{animate:true,duration:600});


  }

  goToSignInPage(){


    this.navCtrl.push('SigninPage',{},{animate:true,duration:600})

  }

}

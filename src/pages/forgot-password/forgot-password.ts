import {Component, OnInit} from '@angular/core';
import {Alert, AlertController, IonicPage, Loading, LoadingController, NavController, NavParams} from 'ionic-angular';
import {ToastProvider} from "../../providers/toast";
import {UserService} from "../../providers/user.service";
import { FormBuilder, FormGroup,Validators} from '@angular/forms';
import {LoaderProvider} from "../../providers/loader/loader";
import {AuthProvider} from "../../providers/auth/auth";

@IonicPage()
@Component({
  selector: 'page-forgot-password',
  templateUrl: 'forgot-password.html',
})
export class ForgotPasswordPage implements OnInit{

  form: FormGroup;
  email:string=null;
  PURE_EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;


  constructor(public navCtrl:NavController,
              public formBuilder: FormBuilder,
              private loadingCtrl:LoadingController,
              public userService: UserService,
              private loaderService:LoaderProvider,
              public navParams:NavParams, public toastService:ToastProvider,
              private authProvider: AuthProvider,
              private alertCtrl: AlertController) {
  }

  ngOnInit(){
    this.form = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required,Validators.pattern(this.PURE_EMAIL_REGEXP)])],
    });
  }

  ionViewDidLoad() {
  }


  async forgotPassword(): Promise<void> {
    if(!this.form.valid){
      return
    }

    this.loaderService.present('Please wait. we are sending you a password reset link on your mail');


    const email = this.form.value.email;

    try {
      const loginUser: void = await this.authProvider.resetPassword(email);
      this.loaderService.dismiss();
      const alert: Alert = this.alertCtrl.create({
        message: 'Check your inbox for a password reset link',
        buttons: [
          { text: 'Cancel', role: 'cancel' },
          {
            text: 'Ok',
            handler: data => {
              this.navCtrl.pop();
            }
          }
        ]
      });
      alert.present();
    } catch (err) {
      this.loaderService.dismiss();
      this.toastService.presentToast(err);
    }
  }

}

import {Component, ViewChild} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {AbstractControl, FormBuilder, FormControl, FormGroup, NgForm, ValidatorFn, Validators} from "@angular/forms";
import {AuthProvider} from "../../providers/auth/auth";
import {ToastProvider} from "../../providers/toast";
import {LoaderProvider} from "../../providers/loader/loader";

/**
 * Generated class for the ChangePasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-change-password',
  templateUrl: 'change-password.html',
})
export class ChangePasswordPage {

  form:FormGroup;
  constructor(public navCtrl: NavController,
              private authService:AuthProvider,
              private toastService:ToastProvider,
              private loaderService:LoaderProvider,
              public navParams: NavParams,
              private formBuilder:FormBuilder) {

    this.form = this.formBuilder.group({
      currentPassword: ['',Validators.required],
      newPassword: new FormControl('', Validators.compose([Validators.minLength(6), Validators.maxLength(20), Validators.required])),
      confirmPassword: new FormControl('', [Validators.required, this.equalto('newPassword')])
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChangePasswordPage');
  }

  equalto(field_name): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {

      let input = control.value;

      let isValid = control.root.value[field_name] == input
      if (!isValid)
        return {'equalTo': {isValid}}
      else
        return null;
    };
  }

  passwordChange(){
    console.log(this.form.value);
    if(!this.form.valid){
      return false;
    }

    this.loaderService.present('Changing Password...');

    this.authService.changePassword(this.form.value.currentPassword,this.form.value.newPassword).then((result:any)=>{
      this.loaderService.dismiss();

      this.toastService.presentToast('Password changed');
      this.form.reset();
      this.navCtrl.pop();

    }).catch((err)=>{
      this.loaderService.dismiss();

      this.toastService.presentToast(err);
      this.navCtrl.pop();

    })
  }
}

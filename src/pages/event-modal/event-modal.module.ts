import { NgModule ,ViewChild} from '@angular/core';
import { IonicPageModule , Tabs } from 'ionic-angular';
import { EventModalPage } from './event-modal';




@NgModule({
  declarations: [
    EventModalPage,
  ],
  imports: [
    IonicPageModule.forChild(EventModalPage),
  ],
  entryComponents: [
  ],
})
export class EventModalPageModule { 


  
}

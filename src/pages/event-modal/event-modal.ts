import { Component ,ViewChild} from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController ,Platform,Tabs} from 'ionic-angular';
import { EventDetailsPage } from '../eventdetails/eventdetails';
import { OrderDetailsPage } from '../orderdetails/orderdetails';
import { MoreDetailsPage } from '../moredetails/moredetails';
import { EventServiceProvider } from '../../providers/event-service/event-service';
import { ToastProvider } from '../../providers/toast'
import { AlertController } from 'ionic-angular';
import { EventDetails, FlowerDetails } from '../../interface/event_interfaces';





@IonicPage()
@Component({
  selector: 'page-event-modal',
  templateUrl: 'event-modal.html' 
})
export class EventModalPage {
   isDeleted :boolean = false;
   tab1Root = EventDetailsPage;
   tab2Root = OrderDetailsPage;
   tab3Root = MoreDetailsPage; 

   espr : EventServiceProvider;
   ntoast : ToastProvider ; 
   mplatform : Platform;
   doSaveOrEdit : boolean = true;
   toUnregisterBackButtonActn : Function;
   eventTittle : string ="";
   platformId : boolean = false; // false - android , true - ipad / ios
   
  constructor(public navCtrl: NavController, public navParams: NavParams, 
    public eventService: EventServiceProvider, public viewCtrl: ViewController
  ,tst : ToastProvider ,private alertCtrl: AlertController, platform : Platform) {
      this.ntoast = tst;
      this.espr = eventService;
      this.mplatform = platform;
      this.isDeleted = false;
      this.doSaveOrEdit = true;
      this.espr.setEvantModalNavController(navCtrl);

      if(platform.is('android')){
        this.platformId = false;
      }else if(platform.is('ios')){
        this.platformId = true;
      }

}
    @ViewChild('eventTabs') tabRef: Tabs;
    ionViewDidLoad() {
 
    //Clear the singleton object in event-service.ts
    this.espr.clearEventDetailsObject();
    this.setTrashButtonVisibility();
    
     //retrive all data if edit mode
     if(this.espr.eventEditModeFlag){
      var eventClicked : EventDetails = this.eventService.getEventObjectFromMasterArray(
        this.eventService.eventItemIndexClicked);
      this.eventService.lastWorkingEventId = eventClicked.eventId;
      this.espr.retriveEntireEvent(this.espr.lastWorkingEventId);
      this.eventTittle=eventClicked.name;  
    }

    //jump to 2nd screen if wtching cart
    if(this.espr.jumpToCartScreen){
      this.tabRef.select(1); //select cart screen
      this.espr.jumpToCartScreen=false;
    }

    //check if event edit mode is on
      //then sshow the tittle on the header
      if(!this.espr.eventEditModeFlag){
        var eventTitleName =document.getElementById("nameOnHeader");
        eventTitleName.style.display="none";
      }
}

ionViewDidAppear(){
  console.log("Event modal view dissappeaed");
}

ionViewWillUnload() { 
  if(this.doSaveOrEdit){
  if(!this.isDeleted){
    if(this.espr.isDetailsChanged){
    //check whether the order has marked as complete by the time user edits it
    //also first check for new event or not
    let eventTemp : EventDetails = this.espr.getEventObjectFromMasterArrayByKey(this.espr.ES_EventDetails.eventid);
    if(((this.espr.ES_EventDetails.eventid).length>10)&&(
      eventTemp.status=="complete"||
      eventTemp.status=="cancelled")){
      this.ntoast.presentToast("The order status have been changed to completed while you were working! Cannot edit!");
      this.espr.isDetailsChanged=false;
      this.espr.eventEditModeFlag=false;
      return;
      }else{
        this.save();
      }
    }
    this.espr.isDetailsChanged=false;
    this.espr.lastWorkingEventId="";
    this.espr.eventItemIndexClicked=-1;
    this.espr.eventEditModeFlag=false;
  }else{
    this.isDeleted = false;
    this.espr.lastWorkingEventId="";
    this.espr.eventItemIndexClicked=-1;
    this.espr.eventEditModeFlag=false;
  }
}else{
  this.doSaveOrEdit = true;
  this.espr.lastWorkingEventId="";
  this.espr.eventItemIndexClicked=-1;
  this.espr.eventEditModeFlag=false;
}
}

save() {
  {
  //Edit / save
  
  if(!this.espr.eventEditModeFlag){
    this.espr.addEvent();
  }else{
    this.espr.editEvent();
  }
  this.espr.lastWorkingEventId = ""; 
  this.espr.eventEditModeFlag=false;
  this.ntoast.presentToast("Saving.. ","center");
  this.viewCtrl.dismiss();
  }
}

  popPage() {
    //check if any flower quantity is zero
    let isFound = false;
    this.espr.array_flowerDetails.forEach((flower : FlowerDetails)=>{
        if(parseInt(flower.qty)==0){
          let size = "";
          switch (flower.size.toLowerCase()){
            case "small" : size = "Small"; break;
            case "medium" : size = "Regular"; break;
            case "large" : size = "Large"; break;
          }
          let msg = "Please remove "+size+" "+flower.color+" "+ flower.name+" from the cart or increase the quantity!";
          console.log(msg)
          this.ntoast.presentToast(msg);
          isFound = true;
        }
    })
    //check if content is updated and update button has not pressed
    console.log(!this.espr.isPlacedOrUpdated)
    console.log( this.espr.isDetailsChanged )
    console.log(this.espr.ES_EventDetails.isOrderPlaced)
    if(!this.espr.isPlacedOrUpdated && this.espr.isDetailsChanged &&
       this.espr.ES_EventDetails.isOrderPlaced== "true"){
      this.ntoast.presentToast("Please update the order!");
      return;
    }

    if(!isFound){
      this.navCtrl.pop();
    }
  }

  private setTrashButtonVisibility(){
    var trashButton = document.getElementById("trash").style;
    if(!this.espr.eventEditModeFlag){
      console.log("hude trash button")
      //hide trash button
      trashButton.display = "none";
    }else{
      //show trash button
      console.log("show trash button")
      trashButton.zIndex = "block";
    }
  }   
  /**
   * this method is used to delete the event
   */
  deleteEvent(){
      let alert = this.alertCtrl.create({
        message: "Are you sure to delete the order?",
        buttons: [
          {
            text: 'CANCEL',
            role: 'cancel',
            handler: () => {
              // continue do nothing
              alert.dismiss();
            }
          },
          {
            text: 'DELETE',
            role: 'DELETE',
            handler: () => {
            // place the order
            this.espr.isPopUpAciveODPage = false;
            alert.dismiss();
           //check whether the order has marked as complete by the time user edits it
            if(this.espr.getEventObjectFromMasterArrayByKey(this.espr.ES_EventDetails.eventid).status=="complete"){
              this.ntoast.presentToast("The order status have been changed to completed while you were working! Cannot delete!");
              this.espr.isDetailsChanged=false;
              this.navCtrl.pop();
              return;
            }
              this.espr.getEventDetailsObject().isOrderPlaced = "true";
              this.espr.getEventDetailsObject().eventtype = "delete";
              this.espr.getEventDetailsObject().status = "pending";
              this.espr.getEventDetailsObject().datetimestamp = 0;
              this.ntoast.presentToast("Done! Waiting for approval!");
              this.navCtrl.pop();
            }
          }
        ]
      });
      alert.present();
  }
  /**
   * mandatory field empty
   */
  checkFields_aBox(errorText : string) {
    let alert = this.alertCtrl.create({
      title: errorText,
      message: 'Do you want to make changes?',
      buttons: [
        {
          text: 'Change',
          role: 'Change',
          handler: () => {
          
          }
        },
        {
          text: 'Exit',
          role: 'Exit',
          handler: () => {
            //disable save or edit
            //since no valid name &| date  field
            this.doSaveOrEdit = false;
            this.navCtrl.pop();  
          }
        }
      ]
    });
    alert.present();
  }

  public checkfields(){
    //check if the details were modified 
    var temp = false;
    if(this.espr.isDetailsChanged){
       temp = true;
    }
    //check name 
    //this falsely change the detail state to edited so counterct we save the state forehand
    var eventName : string = this.espr.getEventDetailsObject().eventName;
    var eventDate : string= this.espr.getEventDetailsObject().date;
    //since we just retrieved the item , details not changed indeed
    this.espr.isDetailsChanged=temp;
    var errMsgSelectFlag = 0;
    var isNameInvalid = false;

    if(eventName=="" || eventName == undefined || eventName == null){
      errMsgSelectFlag = -1;
      isNameInvalid = true;
    }

    if(eventDate=="" || eventDate == undefined || eventDate == null){
      errMsgSelectFlag = -2;
      if(isNameInvalid){
        isNameInvalid=false;
        errMsgSelectFlag = -3;
      }
    }
   

    switch(errMsgSelectFlag){
      case -1 : this.checkFields_aBox("Invalid Event name!");
                this.doSaveOrEdit = false;
                break;
      case -2 : this.checkFields_aBox("Invalid Event date!");
                this.doSaveOrEdit = false;
                break;   
      case -3 : this.checkFields_aBox("Invalid Event name & Date!");
                this.doSaveOrEdit = false;
                break;             
      default : this.doSaveOrEdit = true;      
                this.navCtrl.pop();                
    }


  }
  /**
   * on tittle edited
   */
  public onNameChange(){
    this.espr.getEventDetailsObject().eventName = this.eventTittle;
  }

   /**
   * Confirm delete
   */
  confirmDelete_aBox() {
    this.deleteEvent();
  }

  public ev_registerBackButton(){
    //set back button option
    this.toUnregisterBackButtonActn=this.espr.disableEvantModalBackButtonOverrride =
     this.mplatform.registerBackButtonAction(() => {
      //if pop ups are active disable the back button
      if( this.espr.isPopUpAciveODPage ){
        return;
      }
      this.checkfields();
    },101);
  }

  public ev_unRegisterBackButton(){
    try{
      this.toUnregisterBackButtonActn();
    }catch(e){}
  }

}

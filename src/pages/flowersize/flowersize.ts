import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,ModalController, Toast,ViewController,
   Modal,App} from 'ionic-angular';
import {FlowerDetails} from '../../interface/event_interfaces'
import {ProductsServiceProvider} from '../../providers/products-service/products-service'
import {EventServiceProvider} from '../../providers/event-service/event-service'
import {ToastProvider} from '../../providers/toast'
import {FirebaseDataProvider} from '../../providers/firebase-data/firebase-data'
import { ProductDetails } from '../../interface/product_interface';
import { AlertController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-flowersize',
  templateUrl: 'flowersize.html',
})
export class FlowersizePage {

  public smallSizeCount : number ;
  public mediumSizeCount : number ;
  public largeSizeCount : number ;

  public smallSizeCostTotal : number;
  public mediumSizeCostTotal : number;
  public largeSizeCostTotal : number;

  public smallSizeCost : number;
  public mediumSizeCost : number;
  public largeSizeCost : number ;

  public mPsp : ProductsServiceProvider;
  public mEsp : EventServiceProvider;
  public mTst : ToastProvider;
  public mFbd : FirebaseDataProvider;

  private alertCtrl : AlertController ; 
  private vwCtrl : ViewController;
  private mdlCtrl : ModalController;
  private appCtrl : App;
  private tempEventId : string;
  private colorSelected : string ="NA";
  public isFlowerSpecial : boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams,
  public mdlCtr :  ModalController ,psp :ProductsServiceProvider, esp : EventServiceProvider ,
    tst: ToastProvider,fbd : FirebaseDataProvider,
     alt : AlertController, vc:ViewController,appCtrl : App) {
    this.colorSelected = navParams.get('color');
    this.smallSizeCount=0;
    this.mediumSizeCount=0;
    this.largeSizeCount=0;
    this.smallSizeCostTotal=0;
    this.mediumSizeCostTotal=0;
    this.largeSizeCostTotal=0;
    this.smallSizeCost=1;
    this.mediumSizeCost=2 ;
    this.largeSizeCost = 3;
    this.mPsp = psp;
    this.mEsp = esp;
    this.mTst = tst;
    this.alertCtrl = alt;
    this.vwCtrl = vc;
    this.mdlCtrl = mdlCtr;
    this.appCtrl=appCtrl;
    this.isFlowerSpecial = false;
  }



  ionViewDidLoad() {
  //get flower prices
  var flower : ProductDetails = this.mPsp.getProductByKeyInMaster(this.mPsp.lastClickedProductId);
  this.smallSizeCost=this.mParseFloat(flower.price_small);
  this.mediumSizeCost=this.mParseFloat(flower.price_medium);
  this.largeSizeCost =this.mParseFloat(flower.price_large);
  var size : string[] = flower.size;
  /* To Remove
  */
 /*
  this.smallSizeCost = 1;
  this.mediumSizeCost = 1;
  this.largeSizeCost = 1;
  size.push('sm'); size.push('md'); size.push('lg')
  */
  var isSmall , isMedium , isLarge : boolean =false;
  for(var i=0; i<size.length; i++){
    if(size[i].toLocaleLowerCase() == "sm"){
      isSmall = true;
    }
    if(size[i].toLocaleLowerCase() == "md"){
      isMedium = true;
    }
    if(size[i].toLocaleLowerCase() == "lg"){
      isLarge = true;
    }
  }
  //hide flower size selection accordingly to flower size available
  if(!isSmall){
    document.getElementById("fs_div_small").style.display="none";
  }
  if(!isMedium){
    document.getElementById("fs_div_medium").style.display="none";
  }
  if(!isLarge){
    document.getElementById("fs_div_large").style.display="none";
  }
  }

  ionViewDidEnter(){
    console.log('ionViewdidEnter FlowersizePage');
    this.mcheckOffSeason();
    
  } 
  
  private mParseFloat(cost : string) : number{
    try{
     return parseFloat(cost);
    }catch(ex){
     return 0;
    }
  }

  private smallSizeIncrement(){
    this.smallSizeCount++;
    this.smallSizeCostTotal = this.smallSizeCost*this.smallSizeCount;
    console.log(this.smallSizeCount);
  }

  private mediumSizeIncrement(){
    this.mediumSizeCount++;
    this.mediumSizeCostTotal = this.mediumSizeCost*this.mediumSizeCount;
  }

  private largeSizeIncrement(){
    this.largeSizeCount++;
    this.largeSizeCostTotal = this.largeSizeCost*this.largeSizeCount;
  }

  private smallSizeDecrement(){
    if(this.smallSizeCount>0){
      this.smallSizeCount--;
      this.smallSizeCostTotal = this.smallSizeCost*this.smallSizeCount;
    }
  }

  private mediumSizeDecrement(){
    if(this.mediumSizeCount>0){
      this.mediumSizeCount--;
      this.mediumSizeCostTotal = this.mediumSizeCost*this.mediumSizeCount;
    }
  }

  private largeSizeDecrement(){
    if(this.largeSizeCount>0){
      this.largeSizeCount--;
      this.largeSizeCostTotal = this.largeSizeCost*this.largeSizeCount;
    }
  }

  private onAddToEvent(){
    var atleastOneFlowerAdded : boolean= false;
    var offSeasonFlag :boolean= this.mPsp.checkOffSeason(this.mPsp.lastClickedProductId,this.mEsp.lastWorkingEventId);
    for(var temp=0; temp<this.mEsp.array_flowerDetails.length; temp++){
      if(this.mPsp.lastClickedProductId == (this.mEsp.array_flowerDetails[temp].flowerId)){
        //check small size
        if(this.mEsp.array_flowerDetails[temp].size==("small") && this.mEsp.array_flowerDetails[temp].color==this.colorSelected ){
          this.mEsp.array_flowerDetails[temp].qty =(parseInt(this.mEsp.array_flowerDetails[temp].qty) +this.smallSizeCount).toString();
          this.smallSizeCount = 0;
          atleastOneFlowerAdded = true;
          this.mEsp.array_flowerDetails[temp].offSeason=offSeasonFlag;
        }
        //check medium size
        if(this.mEsp.array_flowerDetails[temp].size==("medium")&& this.mEsp.array_flowerDetails[temp].color==this.colorSelected ){
          this.mEsp.array_flowerDetails[temp].qty =(parseInt(this.mEsp.array_flowerDetails[temp].qty) +this.mediumSizeCount).toString();
          this.mediumSizeCount = 0;
          atleastOneFlowerAdded = true;
          this.mEsp.array_flowerDetails[temp].offSeason=offSeasonFlag;
        }
        //check large size
        if(this.mEsp.array_flowerDetails[temp].size==("large")&& this.mEsp.array_flowerDetails[temp].color==this.colorSelected ){
          this.mEsp.array_flowerDetails[temp].qty =(parseInt(this.mEsp.array_flowerDetails[temp].qty) +this.largeSizeCount).toString();
          this.largeSizeCount = 0;
          atleastOneFlowerAdded = true;
          this.mEsp.array_flowerDetails[temp].offSeason=offSeasonFlag;
        }
      }
    }
    //not found so add fresh
    //create flower details for small size flower
    if(this.smallSizeCount>0){
      var smallSizedFlower : FlowerDetails={};
      var flowerFromArr : ProductDetails={};
      smallSizedFlower.flowerId = this.mPsp.lastClickedProductId;
      smallSizedFlower.qty = this.smallSizeCount.toString();
      smallSizedFlower.size = "small";
      smallSizedFlower.isSpecial = this.isFlowerSpecial;
     
      //get the product details from firebase
    //  console.log("Product retrieved before    "+  smallSizedFlower.flowerId);
      flowerFromArr = this.mPsp.getProductByKeyInMaster(this.mPsp.lastClickedProductId);
    //  console.log("Product retrieved    "+ flowerFromArr);
      smallSizedFlower.groupid = flowerFromArr.groupid;
      smallSizedFlower.url = flowerFromArr.url_small;
      smallSizedFlower.name = flowerFromArr.name;
      smallSizedFlower.cost = flowerFromArr.price_small;
      smallSizedFlower.offSeason = offSeasonFlag;
      smallSizedFlower.color = this.colorSelected;
      smallSizedFlower.type = flowerFromArr.type;
    
      this.mEsp.addFlowerToOrderList(smallSizedFlower);
      atleastOneFlowerAdded = true;
    }

    //create flower details for medium size flower 
    if(this.mediumSizeCount>0){
      var mediumSizedFlower : FlowerDetails={};
      mediumSizedFlower.flowerId = this.mPsp.lastClickedProductId;
      mediumSizedFlower.qty = this.mediumSizeCount.toString();
      mediumSizedFlower.size = "medium";
      mediumSizedFlower.isSpecial = this.isFlowerSpecial;

      //get the product details from firebase
      flowerFromArr = this.mPsp.getProductByKeyInMaster(this.mPsp.lastClickedProductId);
      mediumSizedFlower.groupid = flowerFromArr.groupid;
      mediumSizedFlower.url = flowerFromArr.url_medium;
      mediumSizedFlower.name = flowerFromArr.name;
      mediumSizedFlower.cost = flowerFromArr.price_medium;
      mediumSizedFlower.offSeason = offSeasonFlag;
      mediumSizedFlower.type = flowerFromArr.type;
      mediumSizedFlower.color =  this.colorSelected;

      this.mEsp.addFlowerToOrderList(mediumSizedFlower);
      atleastOneFlowerAdded = true;
    }

    //create flower details for large size flower
    if(this.largeSizeCount>0){
      var largeSizedFlower : FlowerDetails={};
      largeSizedFlower.flowerId = this.mPsp.lastClickedProductId;
      largeSizedFlower.qty = this.largeSizeCount.toString();
      largeSizedFlower.size = "large";
      largeSizedFlower.isSpecial = this.isFlowerSpecial;

       //get the product details from firebase
       flowerFromArr = this.mPsp.getProductByKeyInMaster(this.mPsp.lastClickedProductId);
       largeSizedFlower.groupid = flowerFromArr.groupid;
       largeSizedFlower.url = flowerFromArr.url_large;
       largeSizedFlower.name = flowerFromArr.name;
       largeSizedFlower.cost = flowerFromArr.price_large;
       largeSizedFlower.offSeason = offSeasonFlag;
       largeSizedFlower.type = flowerFromArr.type;
       largeSizedFlower.color =  this.colorSelected;
      this.mEsp.addFlowerToOrderList(largeSizedFlower);
      atleastOneFlowerAdded = true;
    }
    if( atleastOneFlowerAdded == true){
      this.mEsp.isDetailsChanged=true;
      //check if controll passed here without going thru the events page
      //if so save the flower addtion explicitly
      if(this.mEsp.viaSearchPage){
      //save the event id temporarily
      this.mEsp.eventEditModeFlag = true;
      let temp = this.mEsp.lastWorkingEventId;
      this.tempEventId = temp;
      this.mEsp.retriveEntireEvent(this.mEsp.lastWorkingEventId);
      this.mEsp.editEventExplicitly();
      this.mEsp.viaSearchPage=false;
      this.mEsp.lastWorkingEventId="";
      }
      //close modal
      var fss : Modal = this.mEsp.getFlowerSizeModal();
      fss.dismiss();
      this.productAddedAlert();
      this.mEsp.isPopUpAciveODPage = true;

      //save the cart to event in case of adding prodct without going thru the
      // event , ie going directly thru the flower search option.
      this.mPsp.lastClickedProductId = "";
    }else{
      this.mTst.presentToast("No flower size selected!");
    }
  }

  closeModal(){
    if(this.mEsp.jumpToCartScreen){
      this.mEsp.lastWorkingEventId = "";
    }
    this.mEsp.isPopUpAciveODPage = false;
    var fss : Modal = this.mEsp.getFlowerSizeModal();
    fss.dismiss();
  }

  /**
   * this method will pop up a product added alert 
   * continue / go to cart
   */
  private productAddedAlert(){
    let alert = this.alertCtrl.create({
      title : 'The product has been added to the Cart',
      message: 'Would you like to continue browsing or view your Event Cart?',
      buttons: [
        {
          text: 'BROWSE MORE',
          role: 'more',
          handler: () => {
            this.mEsp.isPopUpAciveODPage = false;
            alert.dismiss();
            if(this.mEsp.jumpToCartScreen){
            this.mEsp.jumpToCartScreen=false;
            this.tempEventId = "";
            this.mEsp.lastWorkingEventId="";
            }
          }
        }, 
        {
          text: 'VIEW CART',
          role: 'cart',
          handler: () => {
            this.mEsp.isPopUpAciveODPage = false;
            alert.dismiss();
            if(!this.mEsp.jumpToCartScreen){
              this.navCtrl.popToRoot();
              console.log("View ctrl lenght :::::: "+this.navCtrl.getViews().length)
              if(this.navCtrl.getViews().length<3){
                this.navCtrl.pop();
              }
          
            }else{
              //edit mode
              console.log("LAst working event ID temp "+this.tempEventId);
              this.mEsp.retriveEntireEvent(this.tempEventId);
              this.mEsp.eventEditModeFlag = true;
              this.mEsp.isDetailsChanged = true;
               //go to evant  modal page but with edit flag on       
               this.navCtrl.push("EventModalPage");
            }
          }
        }
      ]
    });
    alert.present();
    alert.onDidDismiss(data=>{ this.mEsp.jumpToCartScreen=false;});
    this.mEsp.isPopUpAciveODPage = true;
  }

  private mcheckOffSeason(){
    try{
      if(this.mPsp.checkOffSeason(this.mPsp.lastClickedProductId,this.mEsp.lastWorkingEventId)){
        //show offseason alert
        console.log("Flower off season")
        document.getElementById("offSeasonWarn").style.display = "block";
      }else{
        console.log("Flower available");
        document.getElementById("offSeasonWarn").style.display = "none";
      }
    }catch(ex){
    }
  
  }
 

}

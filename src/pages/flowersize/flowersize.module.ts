import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FlowersizePage } from './flowersize';

@NgModule({
  declarations: [
    FlowersizePage,
  ],
  imports: [
    IonicPageModule.forChild(FlowersizePage),
  ],
})
export class FlowersizePageModule {}

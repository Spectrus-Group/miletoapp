import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReminderNoteModalPage } from './reminder-note-modal';
import { DatePicker } from '@ionic-native/date-picker';

@NgModule({
  declarations: [
    ReminderNoteModalPage,
  ],
  imports: [
    IonicPageModule.forChild(ReminderNoteModalPage),
  ],
})
export class ReminderNoteModalPageModule {}

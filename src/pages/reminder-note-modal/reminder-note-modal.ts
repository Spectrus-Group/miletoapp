import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController } from 'ionic-angular';
import { ReminderDetails, NoteDetails, EventDetails } from '../../interface/event_interfaces';
import { EventServiceProvider } from '../../providers/event-service/event-service';
import { DatePicker } from '@ionic-native/date-picker';
import * as moment from 'moment';

/**
 * Generated class for the ReminderNoteModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-reminder-note-modal',
  templateUrl: 'reminder-note-modal.html',
})
export class ReminderNoteModalPage {
  datePicker: DatePicker;
  dateToDisplay: string;
  espr: EventServiceProvider;
  page: string;

  reminder: ReminderDetails;
  reminderId: string;
  rtime: string;
  rtext: string;

  note: NoteDetails;
  noteId: string;
  ntime: string;
  ntext: string;
  constructor(public navCtrl: NavController,
    public navParams: NavParams, public viewCtrl: ViewController,
    eventServiceProvier: EventServiceProvider, datePicker: DatePicker, public alertCtrl: AlertController) {
    this.datePicker = datePicker;
    this.espr = eventServiceProvier;
    console.log('page', navParams.get('page'));
    this.page = navParams.get('page');

    this.reminder = this.navParams.get('reminder')
    this.reminderId = this.navParams.get('reminderId');
    this.rtime = this.navParams.get('rtime');
    this.rtext = this.navParams.get('rtext');

    this.note = this.navParams.get('note')
    this.noteId = this.navParams.get('noteId');
    this.ntime = this.navParams.get('ntime');
    this.ntext = this.navParams.get('ntext');


  }

  // this.espr.array_reminderDetails = [];
  // this.espr.array_noteDetails = [];

  ionViewDidLoad() {
    console.log('note reminder', this.note, this.reminder);
    this.checkPage();
    if (this.rtime !== undefined) {
      this.dateToDisplay = moment(this.rtime).format("DD/MMMM/YYYY | hh:mm A");
    }
    else {
      this.dateToDisplay = "Reminder Time";
    }

  }

  pickDate() {
    this.datePicker.show({
      date: new Date(),
      mode: 'datetime',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
    }).then(
      date => {
        this.rtime = date.toString();
        this.dateToDisplay = moment(this.rtime).format("DD/MMMM/YYYY | hh:mm A");
      }
    );
  }
  checkPage() {
    console.log(this.page)
    if (this.page == "reminder") {
      document.getElementById("note").style.display = "none"
    }
    else {
      document.getElementById("reminder").style.display = "none"
    }

    if (this.reminderId == undefined && this.noteId == undefined) {
      document.getElementById("edit").style.display = "none"
      document.getElementById("delete").style.display = "none"
    }
    else {
      document.getElementById("save").style.display = "none"

    }
  }

  saveToEvent() {

    if (this.page == "reminder") {
      var reminderEvent: ReminderDetails = {};
      reminderEvent.reminderId = new Date().getTime().toString();
      reminderEvent.time = new Date(this.rtime).toString();
      reminderEvent.reminderText = this.rtext;
      console.log(new Date().getTime().toString(), this.rtime.toString(), this.rtext, reminderEvent);
      this.espr.addReminderToEvent(reminderEvent);
      this.espr.scheduleNewReminder(reminderEvent);
    }
    else {
      var noteEvent: NoteDetails = {};
      noteEvent.noteId = new Date().getTime().toString();
      noteEvent.time = new Date().toString();
      noteEvent.noteText = this.ntext;
      console.log(new Date().getTime().toString(), new Date().toString(), this.ntext, noteEvent);
      this.espr.addNoteToEvent(noteEvent);
    }

    this.navCtrl.pop();

  }

  edittoEvent() {
    if (this.page == "reminder") {
      var reminderEvent: ReminderDetails = {};
      reminderEvent.reminderId = this.reminderId;
      reminderEvent.time = new Date(this.rtime).toString();
      reminderEvent.reminderText = this.rtext;
      this.espr.editReminderEvent(reminderEvent);
      this.espr.updateExistingReminder(reminderEvent);
    }
    else {
      var noteEvent: NoteDetails = {};
      noteEvent.noteId = this.noteId;
      noteEvent.time = new Date(this.ntime).toString();
      noteEvent.noteText = this.ntext;
      this.espr.editNoteEvent(noteEvent);
    }
    this.navCtrl.pop();
  }

  deletetoEvent(remnot) {
    if (this.page == "reminder") {
      let alert = this.alertCtrl.create({
      title: 'Are you want to delete this Reminder',
      buttons: [
        {
          text: 'CANCEL',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'DELETE',
          role: 'delete',
          handler: () => {
            
            remnot = this.reminder
            console.log(remnot);
            this.espr.deleteReminderEvent(remnot);
            this.espr.cancelExistingReminder(remnot);
            this.navCtrl.pop();
          }
        }
      ]
    });
    alert.present();

    }
    else{
      let alert = this.alertCtrl.create({
        title: 'Are you want to delete this Note',
        buttons: [
          {
            text: 'CANCEL',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          },
          {
            text: 'DELETE',
            role: 'delete',
            handler: () => {
              console.log('Delete clicked');
              remnot = this.note;
              console.log(remnot);
              this.espr.deleteNoteEvent(remnot);
              this.navCtrl.pop();
            }
          }
        ]
      });
      alert.present();
  
    }
      
    
  }

  closeModal() {
    this.navCtrl.pop();
  }





}

import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, Option,Platform} from 'ionic-angular';
import {ICategorySchema} from "../../interface/category.interface";
import {FirebaseDataProvider} from "../../providers/firebase-data/firebase-data";
import {Iproduct} from "../../interface/product.interface";

/**
 * Generated class for the ViewAllPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-view-all',
  templateUrl: 'view-all.html',
})
export class ViewAllPage {
  categoryData: ICategorySchema[] = [];
  filteredProductsData: ICategorySchema[] = [];
  title: string = '';
  category = '';
  titleColor: string = "#c0c0c0";

  favourite: string[] = [];
  featured: string[] = [];
  process: string = 'pending';

  resultData: Iproduct[] = [];
  currentPage : number = 1 ;
  currentLastElementIndex : number = 32;
  maxProductsPerPage : number = 32; 
  productsLength : number = 0; 
  shallDisplayBackButton : boolean = false;
  platformId : boolean = false; // false - android , true - ipad / ios
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public firebaseData: FirebaseDataProvider,
              public platform : Platform) {
    this.categoryData = this.navParams.get('categoryData');
    this.title = this.navParams.get('title');
    this.category = this.navParams.get('category');
    this.titleColor = this.navParams.get('color');
    this.shallDisplayBackButton = this.navParams.get('shallDisplayBackButton');
    this.process = 'resolved';
    console.log("Category "+this.category)

    if(platform.is('android')){
      this.platformId = false;
    }else if(platform.is('ios')){
      this.platformId = true;
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewAllPage');
    //check if search page is opened as modal
  /*  if (this.shallDisplayBackButton) {
      document.getElementById("SP_theExtraBackButton").style.display = "block";
    } else {
      document.getElementById("SP_theExtraBackButton").style.display = "none";
    } */

    this.filteredProductsData=[];
    this.currentPage = 1;
    this.productsLength= this.categoryData.length;
    this.firebaseData.favourite.subscribe((data: string[]) => {
      this.favourite = data;
      if (this.category === 'favourite') {
        this.process = 'pending';
        for (let index = 0; index <this.productsLength ; index++) {
          let isFavourite = this.favourite.indexOf(this.categoryData[index].id);
          if (isFavourite === -1) {
            this.categoryData.splice(index, 1);
          }
        }
        this.process = 'resolved';
      }
      this.doInfinite(undefined,undefined);
    });
  }

  openSerachPage(data: any) {
    /*category either color or type
    *
    */
    this.navCtrl.push('SearchPage', {data: data, category: this.category,shallDisplayExtraBackButton:this.shallDisplayBackButton})
  }

  doInfinite(infiniteScroll,direction) {
    this.filteredProductsData=[];
    let length = this.currentLastElementIndex = this.currentPage * this.maxProductsPerPage; //32 max items per page in ipad pro
    if(length>this.productsLength){
      length = this.productsLength;
    }
    for(let i=0; i<length;i++){
      this.filteredProductsData.push(this.categoryData[i]);
    }
    this.currentPage++;
    if(infiniteScroll!=undefined){
      infiniteScroll.complete();
    }
  }

  public closePage(){
    this.navCtrl.pop();
  }

}

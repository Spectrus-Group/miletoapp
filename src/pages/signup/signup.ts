import {Component, OnInit} from '@angular/core';
import {
  Alert, AlertController, IonicPage, Loading, LoadingController, NavController, Platform,
  Spinner
} from 'ionic-angular';
import {AbstractControl, FormBuilder, FormControl, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import {UserService} from "../../providers/user.service";
import {ToastProvider} from "../../providers/toast";
import {ISignupResponse} from "../../interface/signup_response.interface";
import {LoaderProvider} from "../../providers/loader/loader";
import {AuthProvider} from "../../providers/auth/auth";
import {HomePage} from "../home/home";


@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage implements OnInit {
  signupForm: FormGroup;
  email: string = null;
  password: string = null;
  termAndCondition = '';
  PURE_EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  constructor(public formBuilder: FormBuilder, private navCtrl: NavController,
              private userService: UserService,
              private platform: Platform,
              private loaderService: LoaderProvider,
              private alertCtrl: AlertController,
              private toastService: ToastProvider,
              private  authProvider: AuthProvider,
              private loadingCtrl: LoadingController) {

  }

  ionViewDidLoad() {
  }


  ngOnInit() {
    this.signupForm = new FormGroup({
      name: new FormControl('', Validators.compose([Validators.maxLength(30),
        Validators.minLength(3),
        Validators.pattern('^[A-Za-z]+(?:[ ][A-Za-z]+)*$'),
        Validators.required])),
      email: new FormControl('', Validators.compose([Validators.required, Validators.pattern(this.PURE_EMAIL_REGEXP)])),
      password: new FormControl('', Validators.compose([Validators.minLength(6), Validators.maxLength(20), Validators.required])),
      re_password: new FormControl('', [Validators.required, this.equalto('password')])
    });
  }

  equalto(field_name): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {

      let input = control.value;

      let isValid = control.root.value[field_name] == input
      if (!isValid)
        return {'equalTo': {isValid}}
      else
        return null;
    };
  }

  async onSignUp() {

    console.log(this.signupForm);

    if (!this.signupForm.valid) {
      return false;
    }

    this.loaderService.present('Please wait. we are signing you up..');



    try {
      await this.authProvider.signupUser(this.signupForm.value);
      this.loaderService.dismiss();
      this.navCtrl.setRoot('HomePage');
    } catch (err) {
      this.loaderService.dismiss();
      this.toastService.presentToast(err.message);
    }


  }

  onForgotPassword() {
  }

  goToSignInPage() {
    this.navCtrl.setRoot('SigninPage');

  }


  onTnc() {
    let alert = this.alertCtrl.create({
      title: 'Terms and Conditions',
      message: this.termAndCondition,
      buttons: ['OK']
    });
    alert.present();
  }



}

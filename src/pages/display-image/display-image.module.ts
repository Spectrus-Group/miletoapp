import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DisplayImagePage } from './display-image';

@NgModule({
  declarations: [
    DisplayImagePage,
  ],
  imports: [
    IonicPageModule.forChild(DisplayImagePage),
  ],
})
export class DisplayImagePageModule {}

import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {Iproduct} from "../../interface/product.interface";

/**
 * Generated class for the DisplayImagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-display-image',
  templateUrl: 'display-image.html',
})
export class DisplayImagePage {

  data: Iproduct;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.data = this.navParams.get('data');
    console.log(this.data);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DisplayImagePage');
  }

  onClose(){
    this.navCtrl.pop();
  }

}

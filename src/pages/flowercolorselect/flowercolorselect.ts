import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , ViewController } from 'ionic-angular';
import {ICategorySchema} from '../../interface/category.interface'
import {FirebaseDataProvider} from '../../providers/firebase-data/firebase-data'

/**
 * Generated class for the FlowercolorselectPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-flowercolorselect',
  templateUrl: 'flowercolorselect.html',
})
export class FlowercolorselectPage {
  colors : string[] = [];
  colorCats : ICategorySchema[] =[];
  constructor(public navCtrl: NavController, public navParams: NavParams ,
     public viewCtrl : ViewController ,public fbData : FirebaseDataProvider) {
     this.colors = navParams.get('colors');
  }

  ionViewDidLoad() { 
    console.log('ionViewDidLoad FlowercolorselectPage');
    //craete i category schema array
    let fbColors : ICategorySchema[]=[];
    this.fbData.category.subscribe((category)=>{
      fbColors=category.color
    })
    //check if colors presen in the db
    this.colors.forEach((color)=>{
      let isMatchFound : boolean = false;
      color = color.replace(" ","");
      //colors from db
      fbColors.forEach((fbColor)=>{
        if(!isMatchFound){
          if(color.toLowerCase()==fbColor.name.toLowerCase() ||
        color.toLowerCase().includes(fbColor.name.toLowerCase())||
        fbColor.name.toLowerCase().includes(color.toLowerCase())){
          let temp : ICategorySchema={};
          temp.name = color;
          if(temp.name.toLowerCase().includes('white')){
            temp.hex = '#efefef';
          }else{
            temp.hex = fbColor.hex;
          }
          temp.url = fbColor.url;
          temp.id = fbColor.id;
          this.colorCats.push(temp);
          isMatchFound = true;
        }  
        }
      })
      if(!isMatchFound){
        let temp : ICategorySchema={};
        temp.name = color;
        temp.hex = "#A8A8A8"
        this.colorCats.push(temp);
      }
    })    
  }

  public colorSelected(color : string){
    this.viewCtrl.dismiss({'color':color});
  }


  closeModal(){
    this.viewCtrl.dismiss(null,'cancel');
  }

}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FlowercolorselectPage } from './flowercolorselect';

@NgModule({
  declarations: [
    FlowercolorselectPage,
  ],
  imports: [
    IonicPageModule.forChild(FlowercolorselectPage),
  ],
})
export class FlowercolorselectPageModule {}

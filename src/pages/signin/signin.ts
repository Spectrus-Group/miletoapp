
import {Component, OnInit} from '@angular/core';
import {Alert, AlertController, IonicPage, Loading, LoadingController, NavController, Platform} from 'ionic-angular';
import { FormBuilder, FormGroup,Validators} from '@angular/forms';
import {UserService} from "../../providers/user.service";
import {ToastProvider} from "../../providers/toast";
import {LoaderProvider} from "../../providers/loader/loader";
import {Isettings} from "../../interface/settings.interface";
import {SettingsProvider} from "../../providers/settings/settings";
import {AuthProvider} from "../../providers/auth/auth";
import firebase from 'firebase';
import { InAppBrowser } from '@ionic-native/in-app-browser';


@IonicPage()
@Component({
  selector: 'page-signin',
  templateUrl: 'signin.html',
})
export class SigninPage implements OnInit{
  settings:Isettings={};
  signinForm: FormGroup;
  email:string=null;
  password:string=null;
  termAndCondition='';
  privacyurl='';
  PURE_EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  constructor(public formBuilder: FormBuilder,private navCtrl:NavController,
              private userService:UserService,
              private settingService:SettingsProvider,
              private platform:Platform,
              private alertCtrl:AlertController,
              private loaderService:LoaderProvider,
              private toastService:ToastProvider,
              private authProvider: AuthProvider,
              public iab: InAppBrowser) {
    this.settings=this.settingService.getSettings();

  }

  ionViewDidLoad() {
  }

  ngOnInit(){
    this.signinForm = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required,Validators.pattern(this.PURE_EMAIL_REGEXP)])],
      password: ['', Validators.compose([Validators.minLength(6),Validators.maxLength(20),Validators.required])]
    });

    this.getTermsAndCondUrl();
  }

  async onSignIn () {
    console.log(this.signinForm);

    if(!this.signinForm.valid){
      return false;
    }



    const email = this.signinForm.value.email;
    const password = this.signinForm.value.password;

    this.loaderService.present('Please wait. we are signing you in!');
    try {
      const loginUser: firebase.User = await this.authProvider.loginUser(
        email,
        password
      );
      this.loaderService.dismiss();
      this.authProvider.timestampProfileLastActiveOn();
    } catch (err) {
      this.loaderService.dismiss();
      this.toastService.presentToast(err.message);
    }

  }

  onForgotPassword() {
    this.navCtrl.push('ForgotPasswordPage');

  }


  goToSignupPage(){
    this.navCtrl.setRoot('SignupPage');
  }


  onTnc(){
    const browser = this.iab.create(this.termAndCondition);
    browser.on('loadstop').subscribe(event => {
     
    });
    return;
  }

  public onPrivacy(){
    const browser = this.iab.create(this.privacyurl);
    browser.on('loadstop').subscribe(event => {
     
    });
    return;
  }

  getTermsAndCondUrl(){
    return new Promise((resolve,reject)=>{
      firebase.database().ref('/doc_url/').once('value').then((snapshot)=>{
         snapshot.forEach(element => {
             if(element.val().id.toString()=="1"){
              this.termAndCondition = element.val().url;
             }else if(element.val().id.toString()==2){
               this.privacyurl = element.val().url;
             }
         });
         resolve(0);
      });
     });
  }




}

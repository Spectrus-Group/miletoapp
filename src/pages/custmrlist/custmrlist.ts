import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController,AlertController} from 'ionic-angular';
import {EventServiceProvider} from '../../providers/event-service/event-service'
import {Customer} from '../../interface/event_interfaces'
import {ToastProvider} from '../../providers/toast'

/**
 * Generated class for the CustmrlistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-custmrlist',
  templateUrl: 'custmrlist.html',
})


export class CustmrlistPage {
   public espr : EventServiceProvider ;
   private vctrl : ViewController;
   private actrl : AlertController;
   private tst : ToastProvider;
   private searchVal : string ;
   private customerList : Customer[]=[] ;
  @ViewChild("scust") searchCust ;
  constructor(public navCtrl: NavController,
     public navParams: NavParams, esp : EventServiceProvider, vc : ViewController,
    ac : AlertController,tst1:ToastProvider) {
       this.espr = esp;
       this.vctrl = vc;
       this.actrl = ac;
       this.tst = tst1;
       this.customerList = esp.arrayCustomerDetails;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CustmrlistPage');
    setTimeout(() => {
    //  this.searchCust.setFocus();
    },500);
  }

  customerSearch(){
    //console.log(this.searchVal);
    setTimeout(this.searchAndFillArray(),200); /// 200 ms delay for debouncing
    }
  

  searchAndFillArray(){
    this.customerList=[];
    var temp : string = this.searchVal;
    for(var i=0; i<this.espr.arrayCustomerDetails.length; i++){
      if(this.espr.arrayCustomerDetails[i].name.toLocaleLowerCase().includes(temp.toLocaleLowerCase())){
        this.customerList.push(this.espr.arrayCustomerDetails[i]);
      }
    }
  }

  onClickContact(cust : Customer){
    this.vctrl.dismiss(cust);
  }

  newContact(){
    // open a pop up to enter comment
    let alert = this.actrl.create({
      title: 'Customer',
      inputs: [
        {
          name: 'name',
          placeholder: "Name" ,
          type : 'text'
        },
        {
          name: 'phone',
          placeholder: "Phone" ,
          type : 'number'
        },
        {
          name: 'address',
          placeholder: "Address" ,
          type : 'text'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            //do nothing
            alert.dismiss();
          }
        },
        {
          text: 'Create',
          handler: data => {
            if(data.name == "" || data.name ==null || data.name == undefined){
              this.tst.presentToast("Invalid name",'center');
              return;
            }
            if(data.phone == "" || data.phone ==null || data.phone == undefined){
              this.tst.presentToast("Invalid phone",'center');
              return;
            }
            if(data.address == "" || data.address ==null || data.address == undefined){
              this.tst.presentToast("Invalid address",'center');
              return;
            }
            //add customer
            this.espr.addCustomerToFirebase(data.name,data.phone,data.address);
            this.tst.presentToast("Customer added",'center');
            //refresh the list  after 500 ms
            setInterval( this.refreshContactList(),500);
            alert.dismiss();
          }
        }
      ]
    });
    alert.present();
  }

  dismiss(){
    this.vctrl.dismiss(undefined);
  }

  refreshContactList(){
    if(this.searchVal=="" || this.searchVal=== undefined || this.searchVal==null){
      this.customerList = [];
      this.customerList = this.espr.arrayCustomerDetails;
    }else{
      this.searchAndFillArray();
    }
  }

  deleteCustomer(cust : Customer){
    let alert = this.actrl.create({
     message:"Delete customer?",
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            //do nothing
            alert.dismiss();
          }
        },
        {
          text: 'Delete',
          role: 'delete',
          handler: data => {
            this.espr.deleteCustomer(cust);
            this.tst.presentToast("Removed.","center");
            //refresh the list  after 500 ms
            setInterval( this.refreshContactList(),500);
            alert.dismiss();
          }
        }
      ]
    });
    alert.present();
  }
}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CustmrlistPage } from './custmrlist';

@NgModule({
  declarations: [
    CustmrlistPage,
  ],
  imports: [
    IonicPageModule.forChild(CustmrlistPage),
  ],
})
export class CustmrlistPageModule {}

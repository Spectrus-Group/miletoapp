import {Component, ViewChild} from '@angular/core';
import {IonicPage, Nav, NavController, NavParams} from 'ionic-angular';
import {UserService} from "../../providers/user.service";
import {ToastProvider} from "../../providers/toast";
import {AuthProvider} from "../../providers/auth/auth";
import {AppVersion} from '@ionic-native/app-version';

// export interface PageInterface {
//   title: string;
//   pageName: string;
//   tabComponent?: any;
//   index?: number;
//   icon: string;
// }

@IonicPage()
@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {
  rootPage = 'HomePage';

  @ViewChild(Nav) nav: Nav;
  appVersion : String ="";


  constructor(public navCtrl: NavController,
              private authService:AuthProvider,
              private userService: UserService,
              private toastService: ToastProvider,
             public appVer : AppVersion) {

              appVer.getVersionNumber().then((data)=>{
                this.appVersion = data;
              });
  }



  ionViewDidLoad() {
  }




  public async onLogout() {
    let isSuccess = await  this.authService.logoutUser();
    if (!isSuccess) {
      this.toastService.presentToast('We could not log you out.try again!');
    }
  }

  goToProfilePage(){
    this.nav.push('ProfilePage');

  }

  goToSettingPage(){
    this.nav.push('SettingsPage');
  }

  goToStoreDetails(){
    this.nav.push('StoredetailsPage');
  }

  goToNotification(){
    this.nav.push('NotificationsPage');
  }
}

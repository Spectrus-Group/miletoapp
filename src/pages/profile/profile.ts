import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , Platform } from 'ionic-angular';
import {UserService} from "../../providers/user.service";
import {ToastProvider} from "../../providers/toast";
import {Iuser} from "../../interface/user.interface";
import {SettingsProvider} from "../../providers/settings/settings";
import {Isettings} from "../../interface/settings.interface";
import {AuthProvider} from "../../providers/auth/auth";
import {Iprofile} from "../../interface/profile.interface";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoaderProvider } from '../../providers/loader/loader';

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  profileForm: FormGroup;
  profile:Iprofile={};
  profileSave:Iprofile={};
  settings:Isettings;
  change:boolean = false;
  platformId : boolean = false; // false - android , true - ipad / ios
  constructor(public navCtrl: NavController,
              private userService:UserService,
              private authService:AuthProvider,
              private settingsService:SettingsProvider,
              private toastService:ToastProvider,
              public navParams: NavParams,
              public formBuilder: FormBuilder,
              private loaderService: LoaderProvider,
              private platform : Platform) {

                this.profileForm = this.formBuilder.group({
                  name: [this.profile.name, Validators.compose([Validators.maxLength(30),
                    Validators.minLength(3),
                    Validators.pattern('^[A-Za-z]+(?:[ ][A-Za-z]+)*$'),
                    Validators.required])],
                  mobile: [this.profile.mobile],});

    this.settings=this.settingsService.getSettings();

    if(platform.is('android')){
      this.platformId = false;
    }else if(platform.is('ios')){
      this.platformId = true;
    }
  }

  ionViewDidLoad() {
    
    this.getProfile();
    this.profileSave=this.profile;
    
  }

  ionViewWillEnter(){
    
    
   

  }
  ionViewWillUnload(){

    this.onProfileUpdate()
    
  }
  getProfile(){
    this.userService.getProfile().then((profile:Iprofile)=>{
      this.profile=profile;
      
      console.log(this.profile,this.change);
    this.change = false;
    console.log(this.profile,this.change);
    }).catch((err)=>{
      this.toastService.presentToast(err);
    })
  }

  linkAccount(){
    this.navCtrl.push('LinkAccountPage');
  }


  gotoProfileEditPage(){
    this.navCtrl.push('ProfileEditPage',{profile:this.profile});
  }

  private async onUpdateFingerprint(e:any){
    if(this.settings.fingerprintEnabled){
       await this.settingsService.setFingerPrintEnabled();
    }
    else {
       await this.settingsService.setFingerPrintDisabled();
    }
  }

  private async logout(){
   this.authService.logoutUser().then(()=>{

   }).catch((err)=>{
     this.toastService.presentToast('We could not logged you out.try again!');

   })

  }

  goToChangePasswordPage(){
    this.navCtrl.push('ChangePasswordPage');
  }

  async onProfileUpdate(){
    
    console.log("FOrm valid"+"updating"+"Address")
    console.log(this.profile)
    this.loaderService.present('Hold on! We are updating your profile.');


    try{
      await this.userService.updateProfile(this.profile)
      this.loaderService.dismiss();
      this.toastService.presentToast('Updated!');

    }
    catch (err){
      this.loaderService.dismiss();
      this.toastService.presentToast(err);
    }

  }

  log(profile){
   // console.log(profile,this.profileSave.name,this.profileSave.mobile)
  }
  
}

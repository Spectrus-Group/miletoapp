import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,ViewController } from 'ionic-angular';
import {EventServiceProvider} from '../../providers/event-service/event-service'
import {UserService} from '../../providers/user.service'

/**
 * Generated class for the FdeliverymodePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-fdeliverymode',
  templateUrl: 'fdeliverymode.html',
})
export class FdeliverymodePage {
  private esp : EventServiceProvider;
  public deliveryAddress : string ="";
  public modeOfDelivery: string="";
  private viewctrl : ViewController;
  constructor(public navCtrl: NavController, public navParams: NavParams,
  espr : EventServiceProvider, vewctrl : ViewController,
 public userService : UserService ) {
   this.userService.getProfile().then((profile)=>{
    this.deliveryAddress =userService.profile.address;
   })
    this.modeOfDelivery = "";
    this.esp = espr;
    this.viewctrl = vewctrl;
  }

  ionViewDidLoad() {
    this.hideAddressWarning();
    this.hideModeWarning();
    document.getElementById("daddrs").style.display="none";
  }

  onAdressChange(){
     if(this.deliveryAddress.toString()==""){
       this.showAddressWarning();
     }else{
       this.hideAddressWarning();
     }
  }

  onModeChange(mode : string){
    if(mode=="delivery"){
     document.getElementById("daddrs").style.display="block";
    }else{
      document.getElementById("daddrs").style.display="none";
    }
  }
  continue(){
    console.log(this.modeOfDelivery);
    if(this.modeOfDelivery.toString()==""){
      this.showModeWarning();
      return;
    }else{
      this.hideModeWarning();
    }
    
    //check fields
    if(this.deliveryAddress.toString()=="" && this.modeOfDelivery.toString()=="delivery"){
      this.showAddressWarning();
      return;
    }else{
      this.hideAddressWarning();
      this.esp.getEventDetailsObject().orderType = this.modeOfDelivery;
      if(this.modeOfDelivery.toLocaleLowerCase()=="delivery"){
        this.esp.getEventDetailsObject().deliveryAddress = this.deliveryAddress;
      }
      this.viewctrl.dismiss("");
    }
  }

  hideAddressWarning(){
    document.getElementById("warnInvalidAddr").style.display="none";
  }

  showAddressWarning(){
    document.getElementById("warnInvalidAddr").style.display="block";
  }

  showModeWarning(){
    document.getElementById("warnInvalidMode").style.display="block";
  }

  hideModeWarning(){
    document.getElementById("warnInvalidMode").style.display="none";
  }

  overlayDismiss(){

    this.viewctrl.dismiss();
    
  }
}

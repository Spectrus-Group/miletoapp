import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FdeliverymodePage } from './fdeliverymode';

@NgModule({
  declarations: [
    FdeliverymodePage,
  ],
  imports: [
    IonicPageModule.forChild(FdeliverymodePage),
  ],
})
export class FdeliverymodePageModule {}

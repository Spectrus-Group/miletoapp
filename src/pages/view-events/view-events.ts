import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Content, ModalController, 
  VirtualScroll ,Platform} from 'ionic-angular';

import * as moment from 'moment';
import { EventServiceProvider } from '../../providers/event-service/event-service';
import { EventDetails, HolidayDetails } from "../../interface/event_interfaces";
import { HolidayProvider } from '../../providers/holiday/holiday-service';
import { CONSTANTS } from '@firebase/util';

@IonicPage()
@Component({
  selector: 'page-view-events',
  templateUrl: 'view-events.html',
})

export class ViewEventsPage {
  @ViewChild(Content) content: Content;

  currentDate = moment().startOf('month');//.format('dddd MM/DD/YYYY');
  todayDate = Date();
  displayDate = moment().startOf('month'); //.format('MMMM YYYY'); //here i am making changes...
  displayPastDate = moment().startOf('month');//.format('MMMM YYYY'); here i am making changes...
  startWeekPast = moment(this.displayPastDate).subtract(7, 'days').startOf('week');//.format('dddd MM/DD/YYYY');
  endWeekPast = moment(this.displayPastDate).subtract(7, 'days').endOf('week');//.format('dddd MM/DD/YYYY')
  weekPast = [];

  startWeekThis = moment(this.displayDate).startOf('week');//.format('dddd MM/DD/YYYY');
  endWeekThis = moment(this.displayDate).endOf('week');//.format('dddd MM/DD/YYYY');
  thisWeek = [];

  a = moment(this.todayDate).startOf('month').valueOf();
  month = moment().startOf('month')
  prevMonth = moment().subtract(1, 'months').endOf('month')
  dates = [];
  ve_eventList: Array<EventDetails> = [];

  eventSource = [];
  viewTitle: string = moment().format("MMMM YYYY");
  selectedDay: Date = new Date();
  calendar = {
    mode: 'month',
    calCurrentDate: new Date()
  };
  myArrow: string = 'md-arrow-dropdown';

  eventService: EventServiceProvider = null;
  holidays: Array<HolidayDetails> = [];
  exists = true;
  tempSelectedDate : Date = new Date();
  i = moment(this.todayDate).startOf('month');
  scrollFlag: boolean = true;
  isDateChangedProgrammatically : boolean = false;
  platformId : boolean = false; // false - android , true - ipad / ios
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public modalCtrl: ModalController,
     public es: EventServiceProvider, public holi: HolidayProvider,public platform : Platform) {
    this.eventService = es;
    this.tempSelectedDate = new Date();

    if(platform.is('android')){
      this.platformId = false;
    }else if(platform.is('ios')){
      this.platformId = true;
    }
  }

  ionViewDidLoad() {
    this.getHolidays();
    this.getEvents();

    for (let i = 0; i < 3; i++) {
      this.pushCurrentDate();
    }
    this.pushPastDate();
  }

  ionViewDidEnter() {
    this.scrollTo()
  }

  getHolidays() {
    this.holi.fetchHolidays();
    this.holidays = this.holi.holiday;
  }

  getEvents() {
    for (var i = 0; i < this.eventService.eventListFromFireBase.length; i++) {
      this.ve_eventList[i] = Object.create(this.eventService.eventListFromFireBase[i]);
      //   this.ve_eventList[i].name = this.eventService.eventListFromFireBase[i].name;
      // this.ve_eventList[i].date = this.eventService.eventListFromFireBase[i].date;
      // this.ve_eventList[i].phone = this.eventService.eventListFromFireBase[i].phone;
      // this.ve_eventList[i].address = this.eventService.eventListFromFireBase[i].address;
      // this.ve_eventList[i].eventId = this.eventService.eventListFromFireBase[i].eventId;

    }
  }
  // method to push dates to current month //
  pushCurrentDate() {
    this.thisWeek = [];
    var monthEvent = this.ve_eventList;
    var monthHoliday = this.holidays;
    var startDate = moment().startOf('month');
    var endDate = moment().endOf('month');
    let x = Math.floor((moment(endDate).diff(moment(startDate), 'days') + 1) / 7);
    var j = 0;
    for (let i = 0; i < 8; i++) {
      if (moment(this.startWeekThis).endOf('week').day('Sunday').format('MMM') === moment(this.month).format('MMM')) {
        
        var st = this.startWeekThis;
        var ed = this.endWeekThis;
        var nowEvent = monthEvent.filter(function (a) {
          var date = a.date;
          return (moment(date).unix() >= moment(st).unix() && moment(date).unix() <= moment(ed).unix());
        });

        var nowHoliday = monthHoliday.filter(function (a) {
          var date = a.date;
          return (moment(date).unix() >= moment(st).unix() && moment(date).unix() <= moment(ed).unix());
        });

        var nowArr = nowEvent.concat(nowHoliday);
        nowArr.sort(function (a, b) {
          var dateA: number = moment(a.date).unix(),
            dateB: number = moment(b.date).unix();
          return dateA - dateB;
        });

        for (let i = 0; i < nowArr.length; i++) {
          for (let j = i + 1; j < nowArr.length; j++) {
            if (nowArr[i].date == nowArr[j].date) {
              nowArr[j].date = "";
              if (nowArr[i].date == nowArr[j].date) {
              }
            }
          }
        }
        this.thisWeek.push({ "date": this.startWeekThis, "end": this.endWeekThis, "event": nowArr });
        this.startWeekThis = moment(this.startWeekThis).add(7, 'days');
        this.endWeekThis = moment(this.endWeekThis).add(7, 'days');
        
      }
      else{
        if(this.startWeekThis.unix() < endDate.unix()){
          this.startWeekThis = moment(this.startWeekThis).add(7, 'days');
          this.endWeekThis = moment(this.endWeekThis).add(7, 'days');
        }
        
      }
    }
    this.dates.push({ id: (this.displayDate).startOf('month').unix(), date: this.displayDate, week: this.thisWeek });
    console.log('dates', this.dates);
    this.month = moment(this.month).add(1, 'months')
    this.displayDate = moment(this.displayDate).add(1, 'months')
  }

  // method to Infinite Scroll //
  doPastInfinite(infiniteScroll) {
      infiniteScroll.enable(false);
      console.log('Begin async operation');
      for (let i = 0; i < 1; i++) {
        this.pushPastDate();
      }
      console.log('Async operation has ended');
      infiniteScroll.enable(true);
      infiniteScroll.complete();
  }

  // method to Infinite Scroll //
  doInfinite(infiniteScroll) {
    console.log('Begin async operation');
    for (let i = 0; i < 2; i++) {
      this.pushCurrentDate();
    }
    console.log('Async operation has ended');
    infiniteScroll.complete();
  }

  // method to push dates to past months //
  pushPastDate() {

    this.displayPastDate = moment(this.displayPastDate).subtract(1, 'months');//.format('MMMM YYYY'); here i am making chanes...
    this.weekPast = [];
    let monthEvent = this.ve_eventList;
    let monthHoliday = this.holidays;
    let startDate = moment(this.displayPastDate).startOf('month');
    let endDate = moment(this.displayPastDate).endOf('month');
    let x = Math.floor((moment(endDate).diff(moment(startDate), 'days') + 1) / 7);

    for (let i = 0; i < 8; i++) {
      console.log(moment(this.startWeekPast).startOf('week').day('Sunday').format('DD MMM') , moment(this.prevMonth).format('DD MMM'))

      if (moment(this.startWeekPast).startOf('week').day('Sunday').format('MMM') === moment(this.prevMonth).format('MMM')) {

        let st1 = this.startWeekPast;
        let ed1 = this.endWeekPast;

        let nowEvent1 = monthEvent.filter(function (a) {
          let date = a.date;
          return (moment(date).unix() >= moment(st1).unix() && moment(date).unix() <= moment(ed1).unix());
        });

        let nowHoliday1 = monthHoliday.filter(function (a) {
          let date = a.date;
          return (moment(date).unix() >= moment(st1).unix() && moment(date).unix() <= moment(ed1).unix());
        });

        let nowArr1 = nowEvent1.concat(nowHoliday1);
        nowArr1.sort(function (a, b) {
          var dateA: number = moment(a.date).unix();
          var dateB: number = moment(b.date).unix();
          return dateA - dateB;
        });

        this.weekPast.push({ "date": this.startWeekPast, "end": this.endWeekPast, "event": nowArr1 });
        this.startWeekPast = moment(this.startWeekPast).subtract(7, 'days');
        this.endWeekPast = moment(this.endWeekPast).subtract(7, 'days');
      }
      else{
        
    }
   
    }
    this.weekPast.reverse();
    this.dates.unshift({ id: (this.displayPastDate).startOf('month').unix(), date: this.displayPastDate, week: this.weekPast });
    // console.log('dates', this.dates);
    this.prevMonth = moment(this.prevMonth).subtract(1, 'months').endOf('month');
  }

  fabEvent() {
    setTimeout(function () {
      var myCalendar = document.getElementById('calendar');
      myCalendar.style.zIndex = '1';
      this.myArrow = 'md-arrow-dropup';
    }, 10);

  }

  closeCal() {
    var myCalendar = document.getElementById('calendar');
    myCalendar.style.zIndex = '-2';
    this.myArrow = 'md-arrow-dropdown';
  }


  addEvent() {
    this.setDateInAddEvent(this.tempSelectedDate)
    this.navCtrl.push('EventModalPage');
  }
  swipeEvent(e){
    console.log(e.direction)
    if (e.direction == 2) {
      this.pushPastDate();
  }
  else if(e.direction == 2){
    this.pushCurrentDate();
  }

    
  }
  onCurrentDateChanged = (ev: Date) => {
    this.tempSelectedDate = ev;
    if(this.isDateChangedProgrammatically){
      this.isDateChangedProgrammatically = false;
      return;
    }

    console.log(ev)


    this.i = moment(ev).startOf('month');
    var myCalendar = document.getElementById('calendar');
    // get the current value of the calendar's display property
    var displaySetting = myCalendar.style.zIndex;
    // console.log(i);
    let key = 'M' + moment(this.i).startOf('month').unix().toString();
    let yOffset = document.getElementById(key).offsetTop;
    let hElement: HTMLElement = this.content._elementRef.nativeElement;
    let element = hElement.querySelector("#" + key);
    // console.log('elem', element, key);
    this.scrollFlag = false;

    if (displaySetting == '1') {
      
      // element.scrollIntoView({ block: "start" });
      this.content.scrollTo(0,yOffset+50)
      
      this.viewTitle = moment(ev).format("MMMM YYYY");
      
    }
  }

  // method to scroll to current month //
  scrollTo() {
    let i = moment(this.todayDate).startOf('month').unix().toString();
    this.calendar.calCurrentDate = new Date();
    let key = 'M' + moment().startOf('month').unix().toString();
    let hElement: HTMLElement = this.content._elementRef.nativeElement;
    let element = hElement.querySelector("#" + key);
    // console.log('elem', element, key);
    element.scrollIntoView({ block: "start" });
    this.a = moment(this.todayDate).startOf('month').valueOf();
    this.viewTitle = moment().format("MMMM YYYY");
    this.clearAll();
  }

  currentDateSet() {
    this.calendar.calCurrentDate = new Date();
  }

  toggleCalendar() {
    var myCalendar = document.getElementById('calendar');
    // get the current value of the calendar's display property
    var displaySetting = myCalendar.style.zIndex;
    // now toggle the calendar and the button text, depending on current state
    if (displaySetting == '1') {
      // calendar is visible. hide it
      myCalendar.style.zIndex = '-1';
      this.myArrow = 'md-arrow-dropdown';
      this.scrollFlag = true;
    }
    else {
      // calendar is hidden. show it
      myCalendar.style.zIndex = '1';
      this.myArrow = 'md-arrow-dropup';
      this.scrollFlag = false;
    }
  }
  viewDetailsPage(event_Id: string) {
    //if the clicked item is event
    if (event_Id !== undefined) {
      var index = this.eventService.eventListFromFireBase.findIndex(x => x.eventId == event_Id);
      //edit mode
      this.eventService.eventEditModeFlag = true;
      //set clicked items index
      this.eventService.eventItemIndexClicked = index;
      //go to evant  modal page but with edit flag on 
      this.navCtrl.push('EventModalPage');
    }
  }
  onPageScroll(event) {
    var myCalendar = document.getElementById('calendar');
    // get the current value of the calendar's display property
    var displaySetting = myCalendar.style.zIndex;
 
      let today = document.getElementById('M' + moment(this.a).format('X')).offsetHeight + document.getElementById('M' + moment(this.a).format('X')).offsetTop;
      let yestr = document.getElementById('M' + moment(this.a).format('X')).offsetTop;
      if (this.content.directionY == 'down') {
        if (this.content.scrollTop > today) {
          this.a = moment(this.a).add('months', 1).startOf('month').valueOf();
          this.viewTitle = moment(this.a).format("MMMM YYYY");
          // console.log('topif', this.content.scrollTop, today + 30, this.viewTitle);
        }
      }
      else {
        if (this.content.scrollTop < yestr) {
          this.a = moment(this.a).subtract('months', 1).startOf('month').valueOf();
          this.viewTitle = moment(this.a).format("MMMM YYYY");
          // console.log('topelse', this.content.scrollTop, today, this.viewTitle);
        }
      }
    
    
  }

  clearAll() {
    this.displayDate = moment().startOf('month'); //.format('MMMM YYYY'); //here i am making changes...
    this.displayPastDate = moment().startOf('month');
    this.startWeekPast = moment(this.displayPastDate).subtract(7, 'days').startOf('week');//.format('dddd MM/DD/YYYY');
    this.endWeekPast = moment(this.displayPastDate).subtract(7, 'days').endOf('week');//.format('dddd MM/DD/YYYY')
    this.weekPast = [];
    this.startWeekThis = moment(this.displayDate).startOf('week');//.format('dddd MM/DD/YYYY');
    this.endWeekThis = moment(this.displayDate).endOf('week');//.format('dddd MM/DD/YYYY');
    this.thisWeek = [];
    this.a = moment(this.todayDate).startOf('month').valueOf();
    this.month = moment().startOf('month')
    this.prevMonth = moment().subtract(1, 'months').endOf('month')
    this.dates = [];

    for (let i = 0; i < 3; i++) {
      this.pushCurrentDate();
    }
    this.pushPastDate();
  }

  onPageScrollEnd(event) {
    this.isDateChangedProgrammatically = true;
      this.calendar.calCurrentDate = new Date(moment(this.a).valueOf());
      // console.log(this.calendar.calCurrentDate, 'date');
  }
  /**
   * Toset the date in add event page
   * @param date  date
   */
  setDateInAddEvent(date : Date){
    this.es.setDate(date);
  }

  public closePage(){
    this.navCtrl.pop();
  }

}














import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewEventsPage } from './view-events';

import { NgCalendarModule  } from 'ionic2-calendar';


@NgModule({
  declarations: [
    ViewEventsPage,
  ],
  imports: [
    IonicPageModule.forChild(ViewEventsPage),
    NgCalendarModule,
  ],
})
export class ViewEventsPageModule {}

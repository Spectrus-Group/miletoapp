import {Component, ViewChild} from '@angular/core';
import {IonicPage, NavController, NavParams, Platform, ModalController, Modal,
   ViewController, LoadingController} from 'ionic-angular';
import {SpeechRecognition} from '@ionic-native/speech-recognition';
import {FirebaseDataProvider} from "../../providers/firebase-data/firebase-data";
import {Observable} from "rxjs/Observable";
import "rxjs/add/operator/filter";
import "rxjs/add/operator/debounce";
import {Iproduct, data} from "../../interface/product.interface";
import {EventServiceProvider} from "../../providers/event-service/event-service"
import {ProductsServiceProvider} from "../../providers/products-service/products-service"
import {ToastProvider} from "../../providers/toast"
import {ICategorySchema} from "../../interface/category.interface";


@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage {

  @ViewChild('mysearch') mysearch;
  timeout = null;

  resultData: Iproduct[] = [];
  filteredData: Iproduct[] = [];
  displayData: Iproduct[] = [];
  val: string;
  favourite: string[] = [];
  category: string = 'any';
  data: ICategorySchema;
  process: string = 'pending';
  esp: EventServiceProvider;
  mdctrl: ModalController;
  keyword: string = '';
  mtoast: ToastProvider;
  clone_display_data : Iproduct[] = []
  display_seasons =[];
  inputdata: ICategorySchema;
  /**
  * Variable to hold spinner object
  */
 public orderDetailsToBowseSpinner;

  currentPage : number = 1 ;
  currentLastElementIndex : number = 32;
  maxProductsPerPage : number = 32;
  productsLength : number = 0;
  paginatedDisplayData : Iproduct[] = [];
  shallDisplayExtraBackButton : boolean = false;
  platformId : boolean = false; // false - android , true - ipad / ios
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private speechRecognition: SpeechRecognition,
              private firebaseData: FirebaseDataProvider,
              private pfr: Platform,
              private espr: EventServiceProvider,
              private mdlc: ModalController,
              private psp: ProductsServiceProvider,
              private toast: ToastProvider,
              private vctrl: ViewController,
              private loadingCtrl : LoadingController) {

    this.category = this.navParams.get('category');
    this.data = this.navParams.get('data');
    this.shallDisplayExtraBackButton = this.navParams.get('shallDisplayExtraBackButton');
   // console.log("Category")
   // console.log(this.category);
    console.log(this.data);
    this.inputdata = this.data;
    this.esp = espr;
    this.mtoast = toast;
    this.mdctrl = mdlc;

    if(pfr.is('android')){
      this.platformId = false;
    }else if(pfr.is('ios')){
      this.platformId = true;
    }
  }

  ionViewDidLoad() {
    //init pagination
    this.paginatedDisplayData=[];
    this.currentPage = 1;

    new Promise((resolve,reject)=>{
    this.firebaseData.favourite.subscribe((data: string[]) => {
      this.favourite = data;

     // console.log('this is favorite list...')
     // console.log(this.favourite);

      this.resultData.forEach((data) => {
        let isFavorite = this.favourite.indexOf(data.id);
        if (isFavorite > -1) {
          data.isFavorite = true;
        } else {
          data.isFavorite = false;
        }
      })
      this.segregateFlowers();
    });
   

  });
  }


  private segregateFlowers(){
    this.firebaseData.product.subscribe((org_alldata: Iproduct[]) => {
      this.filteredData = [];
      this.displayData = [];
      
      var alldata  : Iproduct[]=[];

      Object.assign(alldata,org_alldata);

      //console.log(alldata);
      switch (this.category) {

        case 'color': {

         // console.log('coming from color page');
        //  console.log(this.data);
        alldata.forEach((data) => {
          for (let index = 0; index < data.product.color.length; index++) {
            if (data.product.color[index].toLowerCase().indexOf(this.data.name.toLowerCase()) >= 0 ||
            data.product.color[index].toLowerCase().includes(this.data.name.toLowerCase()) ||
            this.data.name.toLowerCase().toLowerCase().includes(data.product.color[index]) ) {
            let isFavorite = this.favourite.indexOf(data.id);
          //  console.log('checking is favorite');
          //  console.log(isFavorite > -1);
            if (isFavorite > -1) {
              data.isFavorite = true;
            } else {
              data.isFavorite = false;
            }
            this.filteredData.push(data);
          }}
          });
          this.displayData = this.filteredData;
          this.process = 'resolved';
          break;
        }

        case 'type': {

         // console.log('coming from type page'+this.data)
         // console.log(this.data)
       alldata.forEach((data) => {

            for (let index = 0; index < data.product.type.length; index++) {
              if (data.product.type[index].toLowerCase().indexOf(this.data.name.toLowerCase()) >= 0 ||
              data.product.type[index].toLowerCase().includes(this.data.name.toLowerCase()) ||
              this.data.name.toLowerCase().includes(data.product.type[index].toLowerCase()) ||
              data.product.name.toLowerCase().includes(this.data.name.toLowerCase())) {
                let isFavorite = this.favourite.indexOf(data.id);
                if (isFavorite > -1) {
                  data.isFavorite = true;
                } else {
                  data.isFavorite = false;
                }
                this.filteredData.push(data);
                break;
              }
            }
          });
          this.displayData = this.filteredData;
          this.process = 'resolved';

          break;
        }

        case 'favourite': {

       //   console.log('comming from favourite page');
       alldata.forEach((data: Iproduct) => {

         //   console.log(this.data.id);
         //   console.log(data.id);

            if (this.data.id === data.id) {
              let isFavorite = this.favourite.indexOf(data.id);
              if (isFavorite > -1) {
                data.isFavorite = true;
                this.filteredData.push(data);
              }else{
                data.isFavorite = false;
                this.filteredData.push(data);
              }
            }
          });
          this.displayData = this.filteredData;
          this.process = 'resolved';
        //  console.log(this.displayData);

          break;
        }

        case 'featured': {

      //  console.log('comming from featured page');
      alldata.forEach((data: Iproduct) => {

        //    console.log(this.data.id);
        //    console.log(data.id);

            if (this.data.id === data.id) {
              let isFavorite = this.favourite.indexOf(data.id);
              if (isFavorite > -1) {
                data.isFavorite = true;
              }else{
                data.isFavorite = false;
              }
              this.filteredData.push(data);
            }
          });
          this.displayData = this.filteredData;
          this.process = 'resolved';
       //   console.log(this.displayData);

          break;
        }

        case 'seasonal': {

      //    console.log('comming from seasonal page');
      alldata.forEach((data: Iproduct) => {

      //      console.log(this.data.id);
      //      console.log(data.id);

            if (data.id === this.data.id) {
              let isFavorite = this.favourite.indexOf(data.id);
              if (isFavorite > -1) {
                data.isFavorite = true;
              }else{
                data.isFavorite = false;
              }
              
              this.filteredData.push(data);
            }
          });
          this.displayData = this.filteredData;
          this.process = 'resolved';
            this.process = 'resolved';
     //     console.log(this.displayData);

          break;
        }

        case 'roundayear': {

      //    console.log('comming from roundayear page');
      alldata.forEach((data: Iproduct) => {

         //   console.log(this.data.id);
         //   console.log(data.id);

            if (data.id === this.data.id) {
              let isFavorite = this.favourite.indexOf(data.id);
              if (isFavorite > -1) {
                data.isFavorite = true;
              }else{
                data.isFavorite = false;
              }
              this.filteredData.push(data);
            }
          });
          this.displayData = this.filteredData;
          this.process = 'resolved';
       //   console.log(this.displayData);

          break;
        }


        default: {

        //  console.log('comming directly on search page');
        alldata.forEach((data: Iproduct) => {
            let isFavorite = this.favourite.indexOf(data.id);
            if (isFavorite > -1) {
              data.isFavorite = true;
            } else {
              data.isFavorite = false;
            }
            this.filteredData.push(data);
          });
           
            this.process = 'resolved';
         //   console.log(this.displayData);

        }

      }
        //offtoonSeason seasons
        this.OffToOnSeasonConversion();
      //paginate
      this.currentPage=1;
      this.doInfinite(undefined,undefined);
    //  console.log(this.resultData);
    })
  
  }

  ionViewWillUnload() {
  }

  doDelayedSearch(event: any) {

    this.keyword = event.target.value.toLowerCase().trim();
    this.loadingSpinnerBrowseProd();
    if (this.timeout) {
      clearTimeout(this.timeout);
    }
    this.timeout = setTimeout(async () => {

      try {
        //this.resultData = await this.firebaseData.searchProduct()
        this.displayData = [];
        this.process = 'pending;'

        if (this.keyword !== '') {
        //  console.log(this.keyword);
          var wordsArray = this.keyword.split(" ");
            this.filteredData.forEach((data) => {
              for (var i = 0; i < wordsArray.length; i++) {
                if (data.product.name.toLowerCase().indexOf(wordsArray[i].toLowerCase()) >= 0) {
                  // let isFavorite = this.favorite.indexOf(data.id);
                  // if (isFavorite){
                  //   data.isFavorite = true;
                  // }
                  this.displayData.push(data);
                  break;
                }
              }
            });
        } else {
          if (this.category === 'color' || this.category === 'type') {
            this.displayData = this.filteredData;
          } else {
            this.displayData = [];
          }
        }
        this.process = 'resolved';
     //   console.log(this.displayData);
      } catch (err) {

      }
      //offtoonSeason seasons
      this.OffToOnSeasonConversion();
      //paginate
      this.currentPage=1;
      this.doInfinite(undefined,undefined);
      this.dismissLoadingSpinner();
      //doSearch(val); //this is your existing function
    }, 500);
  }

  async onSpeechRecognition() {

    let isRecognitionAvailable: boolean, hasPermission: boolean;

    try {
      isRecognitionAvailable = await this.speechRecognition.isRecognitionAvailable();
    }

    catch (err) {

    }

    if (isRecognitionAvailable) {

      try {
        hasPermission = await this.speechRecognition.hasPermission();
      } catch (err) {

      }

    } else {

    }

    if (hasPermission) {

      this.speakNow();

    } else {

      // Request permissions

      try {
        await this.speechRecognition.requestPermission();

        this.speakNow();

      }
      catch (err) {

      }

    }


  }

  speakNow() {

    let options = {
      matches: 1,
      prompt: "",      // Android only
      showPopup: true,  // Android only
      showPartial: true
    }

    this.speechRecognition.startListening(options)
      .subscribe(
        (matches: Array<string>) => {
        //  console.log(matches);

          if (matches.length > 0) {
            this.keyword = matches[0].trim();
            this.displayData = [];
            this.process = 'pending';
            var wordsArray = matches[0].trim().split(" ");

              this.filteredData.forEach((data) => {
                for (var i = 0; i < wordsArray.length; i++) {
                  if (data.product.name.toLowerCase().indexOf(wordsArray[i].toLowerCase()) >= 0) {
                    this.displayData.push(data);
                    break;
                  }
                }
              });
          } else {
            if (this.category === 'color' || this.category === 'type') {
              this.displayData = this.filteredData;
            } else {
              this.displayData = [];
            }
          }
           //offtoonSeason seasons
           this.OffToOnSeasonConversion();

           //paginate
            this.currentPage=1;
             this.doInfinite(undefined,undefined);
          this.process = 'resolved';
          //alert(matches);
        },
        (onerror) => console.log('error:', onerror)
      )
  }

  /**
   * this method is called on search.html on add to event
   * @param key key of the product
   */
  private onAddToEvent(key: string, product : data) {
  //  console.log("Product ket  " + key)
   // console.log("event ket  " + this.espr.lastWorkingEventId);
    //set product id in product details serice
    this.psp.lastClickedProductId = key;
    if (key == "" || key == null || key == undefined) {
      this.toast.presentToast("Something went wrong!!", 'center');
    } else {
      //check for event id
      if (this.esp.lastWorkingEventId == "" || this.esp.lastWorkingEventId == undefined || this.esp.lastWorkingEventId == null) {
        //present event modal and let the user choose the event
        var msp = this.mdctrl.create("MlisteventPage");
        msp.onDidDismiss(data => {
          if (data == true) {
            //color selection
            let colors_passed=product.color;
            if(this.category =='color'){
               colors_passed = [];
               colors_passed.push(this.data.name)
            }
            var csp = this.mdctrl.create("FlowercolorselectPage",{colors:colors_passed});
            this.espr.isPopUpAciveODPage = true;
            csp.onDidDismiss((data,role)=>{
              if(role=='cancel'){
                this.esp.jumpToCartScreen=false;
                this.esp.lastWorkingEventId="";
                return;
              }
              //color
              // Size selection
            var mss = this.mdctrl.create("FlowersizePage",{color:data.color});
            this.espr.setFlowerSizeModal(mss);
            this.espr.isPopUpAciveODPage = true;
            mss.present();
            })
            csp.present();
            return;
          } else {
            this.mtoast.presentToast("No event selected", "center");
            this.espr.isPopUpAciveODPage = false;
            return;
          }
        });
        msp.present();
        this.espr.isPopUpAciveODPage = true;
        return;
      }
      //Pop up to ask the user color and size of the flower
      let colors_passed=product.color;
      if(this.category =='color'){
         colors_passed = [];
         colors_passed.push(this.data.name)
      }
      var csp = this.mdctrl.create("FlowercolorselectPage",{colors:colors_passed});
      this.espr.isPopUpAciveODPage = true;
      csp.onDidDismiss((data,role)=>{
        if(role=='cancel'){
          this.esp.jumpToCartScreen=false;
          this.esp.lastWorkingEventId="";
          return;
        }
      //color
      // Size selection
      var mss = this.mdctrl.create("FlowersizePage",{color:data.color});
      this.espr.setFlowerSizeModal(mss);
      this.espr.isPopUpAciveODPage = true;
      mss.present();
      })
      csp.present();
    }
  }

  toggleShow(i: number) {
    let isShowing: boolean = this.displayData[i].show;
    this.displayData.forEach((data) => {
      data.show = false;
    })
    this.displayData[i].show = !isShowing;
  }

  displayImage(data: Iproduct) {
    this.navCtrl.push('DisplayImagePage', {data});
  }


  markAsFavourite(data: Iproduct) {
    if (data.isFavorite) {
      this.firebaseData.removeFromFavorite(data);
    } else {
      this.firebaseData.addAsFavorite(data);
    }
  }

  closePage() {
    this.navCtrl.pop();
  }

  doInfinite(infiniteScroll,direction) {
    this.paginatedDisplayData=[];
    this.productsLength = this.clone_display_data.length;
    let length = this.currentLastElementIndex = this.currentPage * this.maxProductsPerPage; //32 max items per page in ipad pro
    if(length>this.productsLength){
      length = this.productsLength;
    }
    for(let i=0; i<length;i++){
      //add space to color array
      let temp_color_array = [];
      let temp_clone = this.clone_display_data[i];
      temp_clone.product.color.forEach((col)=>{
        col = col.replace(" ","");
        temp_color_array.push(" "+col);
      })
      temp_clone.product.color = temp_color_array;
      this.paginatedDisplayData.push(temp_clone);
    }
    this.currentPage++;
    if(infiniteScroll!=undefined){
      infiniteScroll.complete();
    }
  }

  loadingSpinnerBrowseProd() {
    document.getElementById("div_Loading_text").style.display='block';
 }

 private dismissLoadingSpinner(){
   setTimeout(function(){
    document.getElementById("div_Loading_text").style.display='none';
   },300)
 }



 private OffToOnSeasonConversion(){
  // console.log("-----------------Off To On Season called-----------called by")
  this. clone_display_data  = this.displayData;
  this.display_seasons=[];

  let  months =["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
  for(let k=0; k<this.clone_display_data.length; k++)
  {
   
    let season =this.clone_display_data[k].product.season;
    if(season != undefined){
    let trimmedFlowerSeason = [];
    //trim flower season to 3 letters
    season.forEach((season)=>{
        season = season.replace(" ","").replace("  ","");
        if(season.length>3){
         trimmedFlowerSeason.push(season.substring(0,3));
        }else{
         trimmedFlowerSeason.push(season);
        }
    })

    //Invert seasons from available to unavailable
    //looop thru flower available month
    let indices = [];
    for(let i=0; i<trimmedFlowerSeason.length; i++){
        let onMonth = trimmedFlowerSeason[i];
        //loop thru the months
        for(let j=0; j<months.length;j++){
            let month = months[j]
         if((onMonth.toLowerCase()==month.toLowerCase()||
         onMonth.toLowerCase().includes(month.toLowerCase())||
         month.toLowerCase().includes(onMonth.toLowerCase()))){
             indices.push(j);
             break;
         }
        }
    }
    //insert on off season months
    trimmedFlowerSeason=[];
    if(indices.length>0){
     for(let i=0; i<months.length;i++){

         if((indices.find((index)=>{if(index==i){
             return true;
         }}))==undefined){
             trimmedFlowerSeason.push(" "+(months[i]));
           }
     }}
    //this.clone_display_data[k].product.season = trimmedFlowerSeason;
    this.display_seasons.push(trimmedFlowerSeason);
  }else{
    let temp = []
    temp.push("Throughout the year");
    this.display_seasons.push(temp);

   // this.clone_display_data[k].product.season=[];
    //this.clone_display_data[k].product.season.push("Throughout the year");
  }
  }
 } 
}

import { Component, ViewChild } from '@angular/core';
import {NavController, Slides, NavParams, IonicPage} from 'ionic-angular';
import {SettingsProvider} from "../../providers/settings/settings";
import {ToastProvider} from "../../providers/toast";
import {Isettings} from "../../interface/settings.interface";
import {ISignupResponse} from "../../interface/signup_response.interface";

@IonicPage()

@Component({
  selector: 'page-intro',
  templateUrl: 'intro.html'
})
export class IntroPage {
  @ViewChild(Slides) slides: Slides;
  skipMsg: string = "Skip";
  state: string = 'x';
  signupResult:ISignupResponse={};
  constructor(public navCtrl: NavController,
              private settingsService:SettingsProvider,
              private navParms:NavParams) {
    this.signupResult=this.navParms.get('data') || {};
    console.log(this.signupResult)
  }



  async skip() {

    let settings:Isettings= this.settingsService.getSettings();


    if(!settings.pinEnabled){
      this.navCtrl.setRoot('PasscodeLockPage',{},{animate:true,duration:600})
    }
    else {
      this.navCtrl.setRoot('HomePage',{},{animate:true,duration:600})

    }

  }


  slideChanged() {
    if (this.slides.isEnd())
      this.skipMsg = "Alright, I got it";
  }

  slideMoved() {
    if (this.slides.getActiveIndex() >= this.slides.getPreviousIndex())
      this.state = 'rightSwipe';
    else
      this.state = 'leftSwipe';
  }

  animationDone() {
    this.state = 'x';
  }

}

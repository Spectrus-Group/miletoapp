import { Component } from '@angular/core';
import { NavController,ModalController,Platform, ToastController,
  LoadingController,
  LoadingOptions} from 'ionic-angular';
import { AlertController,App } from 'ionic-angular';
import {HttpClient} from "@angular/common/http";
import {EventServiceProvider} from "../../providers/event-service/event-service";
import 'rxjs/add/operator/map';
import {FlowerDetails} from "../../interface/event_interfaces";
import {ProductDetails} from "../../interface/product_interface"
import {ProductsServiceProvider} from "../../providers/products-service/products-service";
import {EventDetails} from "../../interface/event_interfaces"
import { ToastProvider } from '../../providers/toast';
import {FirebaseDataProvider} from '../../providers/firebase-data/firebase-data'
import {Iproduct} from '../../interface/product.interface'
import {Icategory} from '../../interface/category.interface'
import {IQoute} from '../../interface/qoute.interface'
import firebase from 'firebase';




@Component({
  selector: 'page-orderdetails',
  templateUrl: 'orderdetails.html'
})

export class OrderDetailsPage {
  espr :EventServiceProvider ;
  pspr : ProductsServiceProvider;
  mdlCtrlr : ModalController;
 isDisabled : boolean;
 labelStatusFinal : string ="Order Completed"; // Cancelled

  mToast : ToastProvider ;
  loadingCtrl : LoadingController;
/**
  * Variable to hold spinner object
  */
 public orderDetailsToBowseSpinner;
 favourite: string[] = [];

  resultData: Iproduct[] = [];
  category: Icategory={};
  platformId : boolean = false; // false - android , true - ipad / ios

  constructor(public navCtrl: NavController, public http: HttpClient,
    public alertCtrl: AlertController,eventServiceProvier :EventServiceProvider,
    psp :ProductsServiceProvider , mc: ModalController, platForm : Platform,private app : App,
    mtoast : ToastProvider , ldctrl : LoadingController , public FirebaseData : FirebaseDataProvider
    ) {
   this.espr = eventServiceProvier;
   this.pspr = psp;
   this.mdlCtrlr = mc;
   this.mToast = mtoast;
   this.labelStatusFinal="Order Completed";
   this.loadingCtrl = ldctrl;
   if(platForm.is('android')){
     this.platformId = false;
   }else if(platForm.is('ios')){
     this.platformId = true;
   }
  }

  /**
   * this method is used to plave the
   * order , which will be embeeddded to the
   * event obj
   */
  placeOrder(){
    if(this.espr.array_flowerDetails.length<=0){
       this.mToast.presentToast("Empty cart!!","center");
       return;
    }
    if(!this.flowerValidityTest()){
      return;
    }
    let alert = this.alertCtrl.create({
      title: "Are you ready to place the order?",
      message: 'Once the order is placed,your order details will be sent to the store. You will be able to edit the order details until the order is processed',
      buttons: [
        {
          text: 'CANCEL',
          role: 'cancel',
          handler: () => {
            // continue do nothing
            alert.dismiss();
            this.espr.isPopUpAciveODPage = false;
          }
        },
        {
          text: 'CONTINUE',
          role: 'CONTINUE',
          handler: () => {
          // place the order
          this.espr.isPopUpAciveODPage = false;
          alert.dismiss();
         this. orderTypePopUp();
          }
        }
      ]
    });
    alert.present();
    this.espr.isPopUpAciveODPage = true;
  }

  private orderPlacedPopUp(){
    let alert = this.alertCtrl.create({
      message: 'Order placed succesfully!',
      buttons: [
        {
          text: 'OK',
          role: 'OK',
          handler: () => {
            this.espr.isPopUpAciveODPage = false;
            this.espr.isPlacedOrUpdated = true;
            this.espr.getEventDetailsObject().isOrderPlaced = "true";
            this.espr.getEventDetailsObject().eventtype = "new";
            this.espr.getEventDetailsObject().status = "pending";
            this.espr.getEventDetailsObject().datetimestamp = new Date(this.espr.getEventDetailsObject().date).getTime().toString();
           this. checkOrderPlacing();
           //close tab view and save
           this.espr.getEvantModalNavController().pop();

           
          }
        }
      ]
    });
    alert.present();
    this.espr.isPopUpAciveODPage = true;
  }

  updateOrder(){
    if(!this.espr.isDetailsChanged){
       return;
    }
    if(this.espr.array_flowerDetails.length<=0){
      this.mToast.presentToast("Empty cart!!","center");
      return;
     }
     if(!this.flowerValidityTest()){
       return;
     }
     //disable update button
     let text_update_order = document.getElementById('orderDelete');
     text_update_order.style.color = "#a8a8a8";

    //check whether the order has marked as complete by the time user edits it
    if(this.espr.getEventObjectFromMasterArrayByKey(this.espr.ES_EventDetails.eventid).status=="complete"){
        this.mToast.presentToast("The order status have been changed to completed while you were working! Cannot edit!");
        this.espr.isDetailsChanged=false;
        this.espr.bs_isDetailsChanged.next(false);
        this.espr.getEvantModalNavController().pop();
        return;
    }
    this.espr.isPlacedOrUpdated = true;
    this.espr.getEventDetailsObject().isOrderPlaced = "true";
    this.espr.getEventDetailsObject().eventtype = "update";
    this.espr.getEventDetailsObject().status = "pending";
    this.espr.getEventDetailsObject().datetimestamp = new Date(this.espr.getEventDetailsObject().date).getTime().toString();
    this.espr.getEvantModalNavController().pop();
    this.mToast.presentToast("Updated!");
  }


  /**
   * this method will redirectt the screen to search product screen
   * and will return from there.
   */
  goToSearchPage(){
    if(this.isDisabled){
      return;
    }
   {
      //go to search page
      this.loadingSpinnerBrowseProd();
      let searchMdl = this.mdlCtrlr.create('BrowseproductsPage');// SearchPage
      this.espr.setSearchPageModal(searchMdl);
      this.espr.isSearchPageOpenModal = true;
      searchMdl.present();
      searchMdl.onDidDismiss(data=>{
        console.log("Search dismissed")
        this.dismissLoadingSpinner();
        this.enableUpdateButton();
        this.espr.calc_totalFlowerCostInCart();}
    );
    }

  }

  ionViewDidLoad(){
 //calculate total flower cost in cart initially
 //later at add ,edit and delete flower api call this is
 //clculated automatically
   this.espr.calc_totalFlowerCostInCart();
   this.checkOrderPlacing();
   this.enableUpdateButton();
  }

  ionViewDidAppear() {
    console.log("ion view did aappear");
  }
  /**
   * this method is used to increse the flower count
   */
  public incrementFlowerCount(flower : FlowerDetails){
    if(this.isDisabled){
      return;
    }
    flower.qty =(parseInt(flower.qty)+ 1).toString();
   this.espr.editFlowerInOrderList(flower);
  }

  /**
   * this method is used to decrease the flower count
   */
  public decrementFlowerCount(flower : FlowerDetails){
    if(this.isDisabled){
      return;
    }
    if((parseInt(flower.qty)- 1)>0){
      flower.qty =(parseInt(flower.qty)- 1).toString();
      this.espr.editFlowerInOrderList(flower);
    }
  }

  /**
   *
   * @param product this method is used to remove the flower
   */
  public removeFlower(product : FlowerDetails){
    if(this.isDisabled){
      return;
    }
    let alert = this.alertCtrl.create({
      message: 'Would you like remove this flower from Cart?',
      buttons: [
        {
          text: 'CANCEL',
          role: 'cancel',
          handler: () => {
            // continue
            this.espr.isPopUpAciveODPage = false;
          }
        },
        {
          text: 'REMOVE',
          role: 'remove',
          handler: () => {
          //delete
          this.espr.deleteFlowerInOrderList(product);
          this.espr.isPopUpAciveODPage = false;
          }
        }
      ]
    });
    alert.present();
    this.espr.isPopUpAciveODPage = true;
  }

  private orderTypePopUp(){
    var fdm = this.mdlCtrlr.create("FdeliverymodePage");

    // on modal dismissed
    fdm.onDidDismiss(data => {
      this.espr.isPopUpAciveODPage = false;
      this.orderPlacedPopUp();
    });

    fdm.present();
    this.espr.isPopUpAciveODPage = true;
  }

  private  checkOrderPlacing(){
    if(this.espr.ES_EventDetails.status.toString()=="complete"){
      document.getElementById("B_placeOrder").style.display="none";
      document.getElementById("orderPlaced").style.display="block";
      document.getElementById("fabAddButton").style.display="none";
      document.getElementById("orderDelete").style.display="none";
      this.labelStatusFinal="Order completed";
      this.isDisabled = true;
    }else if (this.espr.ES_EventDetails.status.toString()=="cancelled"){
      document.getElementById("B_placeOrder").style.display="none";
      document.getElementById("orderPlaced").style.display="block";
      document.getElementById("fabAddButton").style.display="none";
      document.getElementById("orderDelete").style.display="none";
      this.labelStatusFinal="Cancelled by store";
      this.isDisabled = true;
    //  console.log("Order delte not  displayingggggggggg")
    }else if (this.espr.ES_EventDetails.eventtype.toString()=="delete"){
      document.getElementById("B_placeOrder").style.display="none";
      document.getElementById("orderPlaced").style.display="block";
      document.getElementById("fabAddButton").style.display="none";
      document.getElementById("orderDelete").style.display="none";
      this.labelStatusFinal="Order Deleted";
      this.isDisabled = true;
    //  console.log("Order delte not  displayingggggggggg")
    }
     else if((this.espr.ES_EventDetails.status.toString()=="processing" || this.espr.ES_EventDetails.status.toString()=="pending")&&
      this.espr.ES_EventDetails.isOrderPlaced=="true" ){
      document.getElementById("B_placeOrder").style.display="none";
      document.getElementById("orderPlaced").style.display="none";
      document.getElementById("fabAddButton").style.display="block";
      document.getElementById("orderDelete").style.display="block";
      console.log("Order delte displayingggggggggg")
    }
    else{
      document.getElementById("B_placeOrder").style.display="block";
      document.getElementById("orderPlaced").style.display="none";
      document.getElementById("fabAddButton").style.display="block";
      document.getElementById("orderDelete").style.display="none";
      this.isDisabled = false;
    }
  }

  public comment(produc : FlowerDetails){
    var ph_comment : string = produc.comment;
    let commModal = this.mdlCtrlr.create("OrderCommentModalPage",{"comment":ph_comment});
    commModal.present();
    commModal.onDidDismiss((data)=>{
      // on modal dismiss
      console.log(data)
      if(data.status){
        produc.comment = data.comment;
        this.espr.addCommentToFlower(produc);
      }
    });
    /*
    // open a pop up to enter comment
      let alert = this.alertCtrl.create({
        title: 'Comment',
        inputs: [
          {
            name: 'Comment',
            placeholder: "comment" ,
            value : ph_comment ,
            type : 'text'
          }
        ],
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            handler: data => {
              //do nothing
            }
          },
          {
            text: 'Done',
            handler: data => {
              console.log("Data : "+data.Comment);
              produc.comment = data.Comment;
              this.espr.addCommentToFlower(produc);
            }
          }
        ]
      });
      alert.present();
      */
  }

  public specialOrderStatusChange(product : FlowerDetails){
    console.log("STatus change")
    this.espr.isDetailsChanged = true;
    this.espr.changeSpecialOrderStatus(product);
  }

  private loadPreviousEventOrders(){
    let length = this.espr.eventListFromFireBase.length;
    let orders : FlowerDetails[]=[]
    for(let i=length-1; i>=0; i--){
      let event = this.espr.eventListFromFireBase[i];
      if(event.orders.length >0){
        orders= event.orders;
        break;
      }
    }
    //reduce qty to 0
    for(let i=0; i<orders.length; i++){
      orders[i].qty="0";
      console.log(orders[i].name)
      this.espr.addFlowerToOrderList(orders[i]);
    }
  }

  public addProductRootSelector(){
    if(this.espr.array_flowerDetails.length==0 && this.espr.eventListFromFireBase.length!=0){
      this.onAddNewProduct();
    }else{
      this.onAddNewProduct_noPreviousOrderOption();
    }
  }

  public onAddNewProduct(){
    if(this.isDisabled){
      return;
    }
    //check if name field is empty
    if(this.espr.getEventDetailsObject().eventName==undefined ||
    this.espr.getEventDetailsObject().eventName==""||
    this.espr.getEventDetailsObject().eventName==" "){
      this.mToast.presentToast("Please add the Order name in event details page!");
      return;
    }

    let opt=3;
    let alert = this.alertCtrl.create();
    alert.setTitle('Add From');

    alert.addInput({
      type: 'radio',
      label: 'Previous Order',
      value: 'PR',
      checked: true,
      handler:data=>{
        opt = 3;
      }
    });
     alert.addInput({
      type: 'radio',
      label: 'Browse Products',
      value: 'BP',
      checked: false,
      handler:data=>{
        opt = 1;
      }
    });
    alert.addInput({
      type: 'radio',
      label: 'Favorites',
      value: 'FV',
      checked: false,
      handler:data=>{
       opt = 2;
      }
    });

    alert.addButton({
      text: 'Cancel',
      handler: data => {
       alert.dismiss();
      }
    });

    alert.addButton({
      text : 'Ok',
      handler:data=>{
        switch(opt){
          case 1 :  alert.dismiss();
                  this.goToSearchPage();
                  break;
          case 2 :  alert.dismiss();
          this.getFavFlowers().then(data=>{
            if(data==true){
              let alert = this.mdlCtrlr.create("ViewAllPage",{color:'#000000', category:'favourite', title:'Favourite Flowers',categoryData:this.category.favourite,shallDisplayBackButton:true})
              alert.present();
            }else{
              this.mToast.presentToast("Could not load favorite flowers at the moment. Try again!");
            }
          })
          break;
          case 3 : alert.dismiss();
          if(this.espr.array_flowerDetails.length==0 && this.espr.eventListFromFireBase.length!=0){
            this.loadPreviousEventOrders();
            if(this.espr.array_flowerDetails.length==0){
              this.mToast.presentToast("No flowers found in the previous event!");
              return;
            }
          }else{
            this.mToast.presentToast("Cannot be loaded from previous order, when cart is not empty!")
            this.goToSearchPage();
          }
          break;
          default : this.mToast.presentToast("Please choose one!");
        }
      }
    })
    alert.present();
    alert.onDidDismiss((data)=>{
      alert.dismiss();
    })
  }

  public onAddNewProduct_noPreviousOrderOption(){
    if(this.isDisabled){
      return;
    }
    let opt = 1;
    let alert = this.alertCtrl.create();
    alert.setTitle('Add From');
     alert.addInput({
      type: 'radio',
      label: 'Browse Products',
      value: 'BP',
      checked: true,
      handler:data=>{
        opt =1;
      }
    });
    alert.addInput({
      type: 'radio',
      label: 'Favorites',
      value: 'FV',
      checked: false,
      handler:data=>{
        opt = 2;
      }
    });

    alert.addButton({
      text: 'Cancel',
      handler: data => {
       alert.dismiss();
      }
    });

    alert.addButton({
      text: 'Ok',
      handler: data => {
       alert.dismiss();
       switch(opt){
         case 1 :  alert.dismiss();
                 this.goToSearchPage();
                 break;
         case 2 :  alert.dismiss();
         this.getFavFlowers().then(data=>{
           if(data==true){
             let alert = this.mdlCtrlr.create("ViewAllPage",{color:'#000000', category:'favourite', title:'Favourite Flowers',categoryData:this.category.favourite,shallDisplayBackButton:true})
             alert.present();
           }else{
             this.mToast.presentToast("Could not load favorite flowers at the moment. Try again!");
           }
         })
         break;
         default : this.mToast.presentToast("Please choose one!");
       }
      }
    });


    alert.present();
    alert.onDidDismiss((data)=>{
      alert.dismiss();
    })
  }


  public onAddNewProduct_loadFromPrevious(){
    let alert = this.alertCtrl.create({
      title: 'Orders',
      message:'Do you want to load flowers from previous event?',
      buttons: [
        {
          text: 'No',
          role: 'No',
          handler: data => {
            //go to search page
            this.loadingSpinnerBrowseProd();
            let searchMdl = this.mdlCtrlr.create('BrowseproductsPage');// SearchPage
            this.espr.setSearchPageModal(searchMdl);
            this.espr.isSearchPageOpenModal = true;
            searchMdl.present();
            searchMdl.onDidDismiss(data=>{
              console.log("Search dismissed");
              this.dismissLoadingSpinner();
              this.espr.calc_totalFlowerCostInCart();}
          );
          }
        },
        {
          text: 'Load',
          role: 'Load',
          handler: data => {
            this.loadPreviousEventOrders();
            if(this.espr.array_flowerDetails.length==0){
              this.mToast.presentToast("No flowers found in the previous event!");
              return;
            }
          }
        }
      ]
    });
    alert.present();
  }

  loadingSpinnerBrowseProd() {
   /* console.log("Present spinner")
   this.orderDetailsToBowseSpinner= undefined;
      this.orderDetailsToBowseSpinner = this.loadingCtrl.create({
        content: 'Please wait...',
        dismissOnPageChange : true,
      }); */
  // If you enable possible ios bug
  // this.orderDetailsToBowseSpinner.present();
 }

 private dismissLoadingSpinner(){
  //this.orderDetailsToBowseSpinner.dismissAll()
 }

 private getFavFlowers(){
   return new Promise((resolve,reject)=>{
    this.FirebaseData.product.subscribe((data: Iproduct[]) => {
      this.resultData = data;
      this.FirebaseData.favourite.subscribe((data: string[]) => {
        this.favourite = data;
        this.category.favourite = [];

        this.resultData.forEach((data) => {
          let isFavorite = this.favourite.indexOf(data.id);
          if (isFavorite > -1) {
            data.isFavorite = true;
            this.category.favourite.push({
              id: data.id,
              name: data.product.name,
              hex: '#C0C0C0',
              url: data.product.url_medium
            });
          }
        })
        resolve(true);
      })
    })
   })
 }

 public qoute(){
  if(this.espr.array_flowerDetails.length<=0){
    this.mToast.presentToast("Empty cart!!","center");
    return;
  }
  if(!this.flowerValidityTest()){
    return;
  }
  let alert = this.alertCtrl.create({
    title: 'Please enter customer email',
    inputs: [
      {
        name: 'email',
        placeholder: 'Email' 
      },
    ],
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        handler: data => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Email Qoute',
        handler: data => {
          if(!data.email.toString().includes('@')){
           this.mToast.presentToast("Please enter valid email!");
           return;
          }
          if(this.espr.array_flowerDetails.length==0){
            this.mToast.presentToast("Your cart is empty!");
           return;
          }
          let qoute : IQoute={};
          qoute.user = firebase.auth().currentUser.uid;
          qoute.email = data.email;
          qoute.orderdet = this.espr.array_flowerDetails;
          qoute.orderdate = this.espr.ES_EventDetails.date;
          qoute.timestamp = new Date().getTime().toString();
          this.espr.sendQoute(qoute);
          this.mToast.presentToast("Qoute sent!");
          alert.dismiss();
        }
      }
    ]
  });
  alert.present();
 }

 private flowerValidityTest() : boolean{
   //check if any flower quantity is zero
   let isFound = true;
   this.espr.array_flowerDetails.forEach((flower : FlowerDetails)=>{
       if(parseInt(flower.qty)==0){
         let size = "";
         switch (flower.size.toLowerCase()){
           case "small" : size = "Small"; break;
           case "medium" : size = "Regular"; break;
           case "large" : size = "Large"; break;
         }
         let msg = "Please remove "+size+" "+flower.color+" "+ flower.name+" from the cart or increase the quantity!";
         console.log(msg)
         this.mToast.presentToast(msg);
         isFound = false;
       }
   })
   return isFound;
 }

 private enableUpdateButton(){
   try{
     let text_update_order = document.getElementById('orderDelete');
    if(!this.espr.isDetailsChanged){
      text_update_order.style.color = "#a8a8a8";
     }else{
      text_update_order.style.color = "#FFFFFF";
     }
     this.espr.o_isDetailsChanged.subscribe((data)=>{
       if(data){
         //Details changed
         console.log("Deatils changed")
            text_update_order.style.color = "#FFFFFF";
       }
     })
   }catch(e){}
  
 }

}

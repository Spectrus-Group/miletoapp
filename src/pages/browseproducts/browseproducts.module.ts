import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BrowseproductsPage } from './browseproducts';

@NgModule({
  declarations: [
    BrowseproductsPage,
  ],
  imports: [
    IonicPageModule.forChild(BrowseproductsPage),
  ],
})
export class BrowseproductsPageModule {}

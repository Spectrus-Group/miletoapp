import {Component} from '@angular/core';
import {IonicPage, NavController, Platform,ModalController} from 'ionic-angular';
import {EventServiceProvider} from "../../providers/event-service/event-service";
import {EventDetails, HolidayDetails} from "../../interface/event_interfaces"
import {FirebaseDataProvider} from "../../providers/firebase-data/firebase-data";
import {Icategory, ICategorySchema} from "../../interface/category.interface";
import {HolidayProvider} from "../../providers/holiday/holiday-service";
import {Iproduct} from "../../interface/product.interface";


import * as moment from 'moment';

/**
 * Generated class for the BrowseproductsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-browseproducts',
  templateUrl: 'browseproducts.html',
})
export class BrowseproductsPage {
  category: Icategory;
  favourite: string[] = [];
  resultData: Iproduct[] = [];
  categoryProcess:string = "pending";
  favouriteProcess:string = "pending";
  featuredProcess:string = "pending";
  timestampNow : number =0;
  mdclr : ModalController;
  loading;
  public monthTable : string[] = ["jan","feb","mar","apr","may","jun","jul","aug","sep","oct","nov","dec"];
  platformId : boolean = false; // false - android , true - ipad / ios
  constructor(public navCtrl: NavController,
    public eventService: EventServiceProvider,
    private firebaseData: FirebaseDataProvider,
    public holi: HolidayProvider,
    public mdlr : ModalController,
  public platform : Platform) {
      this.mdclr=mdlr;

      if(platform.is('android')){
        this.platformId = false;
      }else if(platform.is('ios')){
        this.platformId = true;
      }
  }

  

  ionViewDidLoad() {
    new Promise((reject,resolve)=>{
    this.firebaseData.category.subscribe((category: Icategory) => {
      this.category = category;
      this.categoryProcess = 'resolved';
     // console.log(this.category);
    });

    this.firebaseData.product.subscribe((data: Iproduct[]) => {
      this.resultData = data;

      /**Filtering Featured*/
      this.category.featured = [];
      /**Filtering seasonal*/
      this.category.seasonal = [];
      /**Filtering for all round the year*/
      this.category.roundAYear = [];

      this.resultData.forEach((data) => {
        if (data.product.featured) {
          this.category.featured.push({
            id: data.id,
            name: data.product.name,
            hex: '#C0C0C0',
            url: data.product.url_medium,
            isOffSeason : data.product.isOffSeason
          });
        }

        let index: number;

        if (data.product.season!= undefined && data.product.season.length>0) {
          if((data.product.season.length==1 && data.product.season[0]=="")){
            index=-1;
          }else{
            index=1;
          }
        } else {
          index = -1;
        }
        //console.log(data.product.season)
        //console.log(index)
        if (index == 1) {
          this.category.seasonal.push({
            id: data.id,
            name: data.product.name,
            hex: '#C0C0C0',
            url: data.product.url_medium,
            isOffSeason : data.product.isOffSeason
          });
        }

         /* Thre is naming discrebency here actualy we show all the flowers here */
         this.category.roundAYear.push({
          id: data.id,
          name: data.product.name,
          hex: '#C0C0C0',
          url: data.product.url_medium,
          isOffSeason : data.product.isOffSeason
        });
      })
      this.featuredProcess = 'resolved'
    })


    

    this.firebaseData.favourite.subscribe((data: string[]) => {
      this.favourite = data;

      this.category.favourite = [];

      this.resultData.forEach((data) => {
        let isFavorite = this.favourite.indexOf(data.id);
        if (isFavorite > -1) {
          data.isFavorite = true;
          this.category.favourite.push({
            id: data.id,
            name: data.product.name,
            hex: '#C0C0C0',
            url: data.product.url_medium,
            isOffSeason : data.product.isOffSeason
          });
        }
      })
      this.favouriteProcess = 'resolved';
    });
  })
  }

  clickedSearchIcon() {
    let shallDisplayExtraBackButton = true;
    let searchMdl = this.mdclr.create('SearchPage',{shallDisplayExtraBackButton});// SearchPage
    this.eventService.setSearchPageModal(searchMdl);
    this.eventService.isSearchPageOpenModal = true;
    searchMdl.present();
    searchMdl.onDidDismiss(data=>{
    // console.log("Search dismissed")
      this.eventService.calc_totalFlowerCostInCart();}
  ); 
  }

  viewAllPage(color, category, title, categoryData) {
    let shallDisplayBackButton = true;
    let searchMdl = this.mdclr.create('ViewAllPage', {color, category, title, categoryData,shallDisplayBackButton});// SearchPage
    this.eventService.setSearchPageModal(searchMdl);
    this.eventService.isSearchPageOpenModal = true;
    searchMdl.present();
    searchMdl.onDidDismiss(data=>{
     // console.log("Search dismissed")
      this.eventService.calc_totalFlowerCostInCart();}
  ); 
  }

  openSearch(cat: ICategorySchema, category: string) {
//    this.navCtrl.push('', {data: cat, category})
    let shallDisplayExtraBackButton = true;
    let searchMdl = this.mdclr.create('SearchPage', {data: cat, category,shallDisplayExtraBackButton});// SearchPage
    this.eventService.setSearchPageModal(searchMdl);
    this.eventService.isSearchPageOpenModal = true;
    searchMdl.present();
    searchMdl.onDidDismiss(data=>{
    //  console.log("Search dismissed")
      this.eventService.calc_totalFlowerCostInCart();}
  ); 
  }

  closePage(){
    this.navCtrl.pop();
  }

}

import { Component } from '@angular/core';
import { NavController, AlertController,Platform, ModalController, ToastController,App } from 'ionic-angular';
import { LocalNotifications } from '@ionic-native/local-notifications';
import {HttpClient} from "@angular/common/http";
import {EventServiceProvider} from "../../providers/event-service/event-service";
import 'rxjs/add/operator/map';
import { EventDetails } from '../../interface/event_interfaces';

@Component({
  selector: 'page-moredetails',
  templateUrl: 'moredetails.html'
})

export class MoreDetailsPage {

  espr :EventServiceProvider ;
  title: string;
  at: Date;
  text: string;
  eventDate= new Date().getTime().toString();
  
  constructor(public navCtrl: NavController, public alertCtrl: AlertController, private localNotifications: LocalNotifications,
     public plt: Platform, public http: HttpClient,
     eventService: EventServiceProvider,public modalCtrl: ModalController, platForm: Platform, app: App) {
      this.espr = eventService
      
      
  }

  reminderModal(reminderId, rtime, rtext, reminder) {
  
    if(reminderId == undefined){
      let modal = this.modalCtrl.create('ReminderNoteModalPage', { page: "reminder"});
      modal.present();
    }

    else{
      let modal = this.modalCtrl.create('ReminderNoteModalPage', { page: "reminder" ,reminderId, rtime, rtext, reminder, });
      modal.present();
    }
  

  
}


noteModal(noteId, ntime, ntext, note) {
  if(noteId == undefined){

    let modal = this.modalCtrl.create('ReminderNoteModalPage', { page: "note"});
  modal.present();

  }
  
  else{
    let modal = this.modalCtrl.create('ReminderNoteModalPage', { page: "note",noteId, ntime, ntext, note});
  modal.present();
  }
  
}
// onNameChange(){
//   //  console.log("Name changed "+ this.ED_Event.eventName);
//       this.espr.getEventDetailsObject().eventName = this.ED_Event.eventName;
//     }
}
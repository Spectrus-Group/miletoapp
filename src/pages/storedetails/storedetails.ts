import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , Platform } from 'ionic-angular';
import { EmailComposer } from '@ionic-native/email-composer';
import {ToastProvider} from '../../providers/toast'
import { CallNumber } from '@ionic-native/call-number';
import {StoreDetails} from '../../interface/storedetails_interface';
import {FirebaseDataProvider} from '../../providers/firebase-data/firebase-data'
/**
 * Generated class for the StoredetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-storedetails',
  templateUrl: 'storedetails.html',
})
export class StoredetailsPage {
  public storeAddress : string = "";
  public phone : string ="";
  public email : string ="";
  public storeHeader : string ="";
  platformId : boolean = false; // false - android , true - ipad / ios
  
  constructor(public navCtrl: NavController, public navParams: NavParams,
    private emailComposer: EmailComposer,private toast : ToastProvider,
    private callNumber: CallNumber,
    private firebasedata : FirebaseDataProvider,
    private plaatform : Platform) {

    this.storeAddress="";
    this.phone="";
    this.email="";
    this.storeHeader="";

    if(plaatform.is('android')){
      this.platformId = false;
    }else if(plaatform.is('ios')){
      this.platformId = true;
    }
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad StoredetailsPage');
    // get and form store address4
    var sa : StoreDetails;
    sa = this.firebasedata.storeDetails[0];
    if(sa != undefined){
      this.storeAddress = sa.storeAddrline1 +'\n'+
      sa.storeAddrline2+'\n'+
      sa.storeAddrline3;
      this.phone = sa.storephone;
      this.email = sa.storeemail;
      this.storeHeader = sa.storeHeader;
    }else{
      this.storeAddress = "Stands 69 to 72 , Sydney flower market" +'\n'+
      "Homebush"+'\n'+
      "New southwales"+'\n'+
      "Australia"+'\n';
      this.phone = "+91-9632276375";
      this.email = "sales@miletoflowers.com"
    }
  }



  callMileto(){
    console.log("Cal mileto");
    this.callNumber.callNumber(this.phone, true)
  .then(res => console.log('Launched dialer!', res))
  .catch(err => this.toast.presentToast("Error launching dialer"));
  }

  emailMileto(){
    console.log("email mileto");

      // avl was false even if mail composer is present in the device
      //so let it be true.
      if(true){

        var perMail : Promise<boolean> = this.emailComposer.hasPermission();
        perMail.then((perm1:boolean)=>{

          if(perm1){
            //has permission
            this.createAndOpenMail();

         }else{
            // no permisson so request persmisson
           var permag : Promise<any> = this.emailComposer.requestPermission();
           permag.then(data=>{
             //check permission again
             if(this.emailComposer.hasPermission()){
               //has permisison
               this.createAndOpenMail();

             }else{
               this.toast.presentToast("Email permission not granted!");
             }

           })
         }

        })


        }else{
        // this.toast.presentToast("Email composer not available in your device!");
        }


  }

  private createAndOpenMail(){
    this.emailComposer.addAlias('gmail', 'com.google.android.gm');
    let emailc = {
      to: this.email,
      cc: '',
      bcc: '',
      subject: '',
      body: '',
      isHtml: true
    };

    // Send a text message using default options
    this.emailComposer.open(
      emailc);
  }

}

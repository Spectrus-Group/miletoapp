import { Component } from '@angular/core';
import { NavController ,ModalController ,Modal } from 'ionic-angular';
import { EventServiceProvider } from '../../providers/event-service/event-service';
import { DatePicker } from '@ionic-native/date-picker';
import {FlowerDetails,EventDetails} from "../../interface/event_interfaces";
import {ToastProvider} from "../../providers/toast"
import { CalendarModal, CalendarModalOptions, DayConfig, CalendarResult } from "ion2-calendar";




@Component({
  selector: 'page-eventdetails',
  templateUrl: 'eventdetails.html'
})

export class EventDetailsPage { 
  datePicker : DatePicker;
  eventService : EventServiceProvider;
  mToast : ToastProvider ;
  dateToDisplay : string ;
  mdlCtrl : ModalController;
  isEditDisabled : boolean ;

  public ED_Event =  {
    date: "",
    phone: "",
    address:"",
    eventId : "", 
    eventName:""      
    };


  constructor(public navCtrl: NavController,
    public es: EventServiceProvider,datePicker:DatePicker ,
     toast : ToastProvider , mdctrl : ModalController) {
      this.eventService =es;
     this.datePicker = datePicker;
     this.mToast = toast;
    this.mdlCtrl = mdctrl;
     this.ED_Event.phone= "";
     this.ED_Event.address="";
     this.isEditDisabled = false;
     this.es.retrieveCustomers();
  }
  pickDate(){
    if(this.isEditDisabled){
      return;
    }
  let context = this;
  let css = 'calenderHeight';
  let weekdays = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
  const options: CalendarModalOptions = {
    title: '',
    defaultDate : new Date(this.ED_Event.date),
    defaultScrollTo : new Date(this.ED_Event.date),
    cssClass : css,
    weekdays : weekdays,
    color : 'dark',
  };
  let myCalendar =  this.mdlCtrl.create(CalendarModal, {
    options: options
  });
  let scr = document.getElementById('event_det_content').style.opacity="0.5";
  myCalendar.present();

  myCalendar.onDidDismiss((date: CalendarResult, type: string) => {
    let scr = document.getElementById('event_det_content').style.opacity="1";
    console.log(date);
    context.ED_Event.date =context.getCurrentDate(new Date(date.time));
    this.onDateChange();
    if(this.es.getEventDetailsObject().isOrderPlaced.toString()=="true"){
      this.es.getEventDetailsObject().eventtype = "update";
      this.es.getEventDetailsObject().status = "pending";
    }else{
      this.es.getEventDetailsObject().eventtype = "new";
      this.es.getEventDetailsObject().status = "pending";
    }
  })
  /*
    this.datePicker.show({
      date: new Date(),
      mode: 'date',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
    }).then(
      date =>{
       
      }
    ); */
  }
  
 
  /**
   * when view appear check from edit flag
   * if edit flag is on load up the evnet details and
   * show on the screen
   */
  ionViewDidLoad(){
    console.log("View appeared");
     //check if the date is passed while calling this page
     if(!this.eventService.eventEditModeFlag){
     if(this.eventService.getDate()==null){
      this.dateToDisplay = "Event Date";
      this.ED_Event.date =  this.getCurrentDate(new Date());
      this.onDateChange();
      
      }else{
       this.ED_Event.date = this.getCurrentDate(this.eventService.getDate());
       this.onDateChange();
      this. changeDateLabelColorToBlack();
       this.eventService.clearDate();
      }
      
    }
      
    if(this.eventService.eventEditModeFlag ){

      //load event details from singleeton
      this.ED_Event.eventName = this.eventService.ES_EventDetails.eventName;
      this.ED_Event.phone = this.eventService.ES_EventDetails.phone;
      this.ED_Event.eventId = this.eventService.eventIdClicked;
      this.ED_Event.date = this.getCurrentDate(new Date(this.eventService.ES_EventDetails.date));

      this.ED_Event.address = this.eventService.ES_EventDetails.address;


        this. changeDateLabelColorToBlack();
        //hide the evet name of content space as it will be shown on header
        this.hideEventTittleOnContent();
      }else{
         //new event
         this.es.lastWorkingEventId="newEvent";
      }
  
    if(this.es.ES_EventDetails.status.toString()=="complete" ||this.es.ES_EventDetails.status.toString()=="cancelled" ){
     this.isEditDisabled = true; //the customer wants it in that way!!
    }
  }



  /// To get the formatted date
  getCurrentDate(date : Date) : string {
    var dc_dateObj = date;
    var dc_day = dc_dateObj.getDay().toString();
    var dc_date = dc_dateObj.getDate();
    var dc_month = dc_dateObj.getMonth()+1;
    var dc_year = dc_dateObj.getFullYear();
    var final_Date_String = "";
    this.dateToDisplay = "";
    //format day
    switch(dc_day){
      case "0" : dc_day="Sunday";
                 break;
      case "1" : dc_day = "Monday"
                 break;
      case "2" : dc_day = "Tuesday"
                 break;
      case "3" : dc_day = "Wednesday"
                 break;     
      case "4" : dc_day = "Thursday"
                 break;                              
      case "5" : dc_day = "Friday"
                 break;  
      case "6" : dc_day = "Saturday"
                 break;  
    }           
    final_Date_String = final_Date_String + dc_day;  
    this.dateToDisplay = this.dateToDisplay + dc_day; 
   
   final_Date_String = final_Date_String + this.formatMonth(dc_month,false);
   final_Date_String = final_Date_String + this.formatDate(dc_date,true);

   this.dateToDisplay = this.dateToDisplay + this.formatDate(dc_date,false);
   this.dateToDisplay = this.dateToDisplay + this.formatMonth(dc_month,true);
  
    //format year
    final_Date_String = final_Date_String + "/"+dc_year;
    this.dateToDisplay = this.dateToDisplay +"/"+dc_year;
    return final_Date_String;
  }

  onNameChange(){
//  console.log("Name changed "+ this.ED_Event.eventName);
    this.eventService.getEventDetailsObject().eventName = this.ED_Event.eventName;
  }

  onDateChange(){
 //   console.log("Date changed "+ this.ED_Event.date);
 if(this.ED_Event.date.toLocaleLowerCase()=='select date'){
  this.eventService.getEventDetailsObject().date = "";
 }else{
  this.eventService.getEventDetailsObject().date = this.ED_Event.date;
  this.eventService.getEventDetailsObject().datetimestamp = new Date(this.ED_Event.date).getTime();
 }

    
  }

  onPhoneChange(){
    //console.log("Phone changed "+ this.ED_Event.phone);
    this.eventService.getEventDetailsObject().phone = this.ED_Event.phone;
  }

  onAdressChange(){
  //  console.log("Address changed "+ this.ED_Event.address);
    this.eventService.getEventDetailsObject().address = this.ED_Event.address;
  }

changeDateLabelColorToBlack(){
  document.getElementById("lbl_date").setAttribute("class","datePicLabelBlack");
}


private hideEventTittleOnContent(){
  var nameLine = document.getElementById("lineNameOnContent");
   nameLine.style.display="none  "; 
   var nameItem = document.getElementById("nameOnContent");
   nameItem.style.display="none  ";
}

private formatDate(datet : number , flag : boolean ) : string{
  var final_Date_String : string="";
  var mSLash : string = "";
  if(flag){
    mSLash = "/";
  }else{
    mSLash = " ";
  }
//Format date
if(datet<10){
  final_Date_String = final_Date_String + mSLash+"0"+datet;
}else{
  final_Date_String = final_Date_String + mSLash+""+datet;
}
return final_Date_String;
}

private formatMonth(montht : Number , flag : boolean) : string{
  var final_Date_String : string="";
  var mSLash : string = "";
  if(flag){
    mSLash = "/";
  }else{
    mSLash = " ";
  }
  if(montht<10){
    final_Date_String = final_Date_String + mSLash+"0"+montht;
  }else{
    final_Date_String = final_Date_String + mSLash+""+montht;
  }
  return final_Date_String;
}
/**
 * pop up customer search page
 */
customerSearch(){
   console.log("On tap event eedit");
   var clp = this.mdlCtrl.create("CustmrlistPage");
   clp.onDidDismiss(data =>{
     if(data != undefined || data != null){
      console.log("Customer "+data.name);
      this.ED_Event.eventName = data.name;
      this.ED_Event.address = data.address;
      this.ED_Event.phone = data.phone;
      this.onAdressChange();
      this.onNameChange();
      this.onPhoneChange();
     }
   });
   clp.present();
}
}


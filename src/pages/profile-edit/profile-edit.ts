
import {Component} from '@angular/core';
import {IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserService} from "../../providers/user.service";
import {ToastProvider} from "../../providers/toast";
import {LoaderProvider} from "../../providers/loader/loader";


@IonicPage()
@Component({
  selector: 'page-profile-edit',
  templateUrl: 'profile-edit.html',
})
export class ProfileEditPage {
  profileForm: FormGroup;

  email: string = null;
  password: string = null;

  constructor(public formBuilder: FormBuilder, private navCtrl: NavController,
              private userService: UserService,
              private navParams:NavParams,
              private toastService: ToastProvider,
              private loaderService: LoaderProvider) {

    this.profileForm = this.formBuilder.group({
      name: [this.navParams.get('profile').name, Validators.compose([Validators.maxLength(30),
        Validators.minLength(3),
        Validators.pattern('^[A-Za-z]+(?:[ ][A-Za-z]+)*$'),
        Validators.required])],
      mobile: [this.navParams.get('profile').mobile],});

  }

  ionViewDidLoad() {
    console.log(this.navCtrl.length())
    console.log(this.navParams.get('profile'))
    console.log(this.profileForm)

  }



  async onProfileUpdate(){
    if(!this.profileForm.valid){
      return false;
    }

    this.loaderService.present('Hold on! We are updating your profile');


    try{
      await this.userService.updateProfile(this.profileForm.value)
      this.loaderService.dismiss();
      this.toastService.presentToast('Updated!');

    }
    catch (err){
      this.loaderService.dismiss();
      this.toastService.presentToast(err);
    }

  }






}

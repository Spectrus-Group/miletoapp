import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {EmailSentPage} from "./email-sent";

@NgModule({
  declarations: [
    EmailSentPage,
  ],
  imports: [
    IonicPageModule.forChild(EmailSentPage),
  ],
})
export class EmailSentPageModule {}

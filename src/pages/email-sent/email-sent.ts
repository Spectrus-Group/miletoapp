import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-email-sent',
  templateUrl: 'email-sent.html',
})
export class EmailSentPage {

  email:string=null;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.email=this.navParams.get('email')||null;
  }

  ionViewDidLoad() {
  }
  goToLoginPage(){
    this.navCtrl.setRoot('SigninPage')
  }

  goToForgetPasswordPage(){
    this.navCtrl.setRoot('ForgotPasswordPage')
  }
}

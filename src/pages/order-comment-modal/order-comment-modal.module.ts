import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrderCommentModalPage } from './order-comment-modal';

@NgModule({
  declarations: [
    OrderCommentModalPage,
  ],
  imports: [
    IonicPageModule.forChild(OrderCommentModalPage),
  ],
})
export class OrderCommentModalPageModule {}

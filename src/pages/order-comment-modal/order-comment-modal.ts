import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController,ViewController } from 'ionic-angular';

/**
 * Generated class for the OrderCommentModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-order-comment-modal',
  templateUrl: 'order-comment-modal.html',
})
export class OrderCommentModalPage {
  comment :string = ""
  constructor(public navCtrl: NavController, public navParams: NavParams ,
    public mdlCtrl : ModalController , public viewCtrl : ViewController) {
  }

  ionViewDidLoad() {
    this.comment = this.navParams.get('comment')
  }

  goBack(){
    let data = {
      "status" : false ,
      "comment" : ""
    }
    data.status = false;
    this.viewCtrl.dismiss(data);
  }

  saveToEvent(){
    let data = {
      "status" : true ,
      "comment" : this.comment
    }
    this.viewCtrl.dismiss(data);
  }

}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , Platform } from 'ionic-angular';
import { IBlog } from '../../interface/blogs.interface';
import {BlogserviceProvider} from '../../providers/blogservice/blogservice'

/**
 * Generated class for the NotificationsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-notifications',
  templateUrl: 'notifications.html',
})
export class NotificationsPage {

  public blogData : IBlog[] =[];
  platformId : boolean = false; // false - android , true - ipad / ios
  
  constructor(public navCtrl: NavController, 
    public navParams: NavParams, 
    public BlogService : BlogserviceProvider,
     public platform : Platform) {
    this.subscribeBlogData();

    if(platform.is('android')){
      this.platformId = false;
    }else if(platform.is('ios')){
      this.platformId = true;
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad notification');
  }

  ionViewWillUnload(){
    console.log("View iwll disappear")
    this.BlogService.setLastSeenNotifications(this.blogData.length);
  }

  private subscribeBlogData(){
    let context = this;
    this.BlogService.o_blogs.subscribe((data)=>{
    this.blogData = data;
  })}

}

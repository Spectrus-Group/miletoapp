import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MlisteventPage } from './mlistevent';

@NgModule({
  declarations: [
    MlisteventPage,
  ],
  imports: [
    IonicPageModule.forChild(MlisteventPage),
  ],
})
export class MlisteventPageModule {}

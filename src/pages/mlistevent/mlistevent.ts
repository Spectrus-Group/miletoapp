import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , ViewController } from 'ionic-angular';
import {EventDetails} from "../../interface/event_interfaces"
import {EventServiceProvider} from "../../providers/event-service/event-service";
import {ToastProvider} from "../../providers/toast"
import * as moment from 'moment';

/**
 * Generated class for the MlisteventPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mlistevent',
  templateUrl: 'mlistevent.html',
})
export class MlisteventPage {
  eventList: EventDetails[] = [];
  espr : EventServiceProvider;
  viewCtrl : ViewController
  timestampNow : number =0;
  constructor(public navCtrl: NavController, public navParams: NavParams,
  public esp : EventServiceProvider , public viewCtr : ViewController, public mtoast : ToastProvider) {
    this.espr = esp;
    this.viewCtrl = viewCtr;
    this.eventList =[];
    this.filterOutEvents();
  }

  filterOutEvents(){
    this.espr.o_eventistFromFirebase.subscribe((data)=>{

      this.eventList =[];
      this.timestampNow =  this.timestampNow = moment().subtract(1, 'days').unix()*1000;
      //filter out the non editable events
      for(var i=0; i<data.length; i++){
        if(data[i].status.toString() !="complete" 
        && data[i].status.toString() !="cancelled"
        && this.timestampNow <= parseInt(data[i].datetimestamp))
        {
         console.log(data[i].status  + data[i].name)
         this.eventList.push(data[i]);
        }
      }
     
      if(this.eventList.length==0){
        this.mtoast.presentToast("No Orders available");
      }
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MlisteventPage');
    //if list is emprty throw toast and close
  }
  public AddToEvent(id : string,name : string){
    //load the index of event in event service
    for(var i=0; i<this.espr.eventListFromFireBase.length;i++){
      if(id==this.espr.eventListFromFireBase[i].eventId){
        //found
        this.esp.eventItemIndexClicked = i;
        break;
      }
    }
    //load the event details in the singleton class
    this.espr.retriveEntireEvent(id);
    this.espr.jumpToCartScreen = true;
    console.log("Clicked event "+name);
    //dismiss the modal
    console.log("Add to event "+ id);
    this.dismiss(id);
  }


  public dismiss(id : string){
    this.esp.isPopUpAciveODPage = false;
    this.espr.viaSearchPage = true;
    if(id=="" || id == undefined || id==null){
      this.viewCtrl.dismiss(false);
    }else{
      this.esp.lastWorkingEventId = id;
      this.viewCtrl.dismiss(true);
    }
  }
  
  overlayDismiss(){
    this.viewCtrl.dismiss();
  }

  viewNewEventPage() {
    this.navCtrl.push('EventModalPage');
  }

}

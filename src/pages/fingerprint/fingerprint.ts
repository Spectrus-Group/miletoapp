import { Component } from '@angular/core';
import {NavController, NavParams, IonicPage} from 'ionic-angular';
import {SettingsProvider} from "../../providers/settings/settings";

@IonicPage()
@Component({
  selector: 'page-fingerprint',
  templateUrl: 'fingerprint.html',
})
export class FingerprintPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,private settingsService:SettingsProvider) {
  }

  ionViewDidLoad() {
  }



  async enable(){
    let isVerified=await this.settingsService.fingerprintVerification();
    if(isVerified){
      let enable=await this.settingsService.setFingerPrintEnabled();
      this.navCtrl.setRoot('HomePage');
    }


  }

  skip(){
    this.navCtrl.setRoot('HomePage')
  }
}

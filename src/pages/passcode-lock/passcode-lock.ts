import {Component} from '@angular/core';
import {
  AlertController, IonicPage, LoadingController, NavController, NavParams, Platform,
  ViewController
} from 'ionic-angular';
import {FingerprintPage} from "../fingerprint/fingerprint";
import {Isettings} from "../../interface/settings.interface";
import { Vibration } from '@ionic-native/vibration';
import {UserService} from "../../providers/user.service";
import {SettingsProvider} from "../../providers/settings/settings";
import {AuthProvider} from "../../providers/auth/auth";


@IonicPage()
@Component({
  selector: 'page-passcode-lock',
  templateUrl: 'passcode-lock.html',
})
export class PasscodeLockPage {

  settings:Isettings;
  enteredPasscode = '';
  attemptsLeft=5;
  passcodeWrong:boolean=null;
  passcodeLabel:string;
  source:string;
  isVerifiedSuccessfully=false;
  warningMessage='If you enter wrong pin again then you will be logged out and your pin will be reset';
  constructor(public navCtrl: NavController,
              private viewController:ViewController,
              public navParams: NavParams,
              private authService:AuthProvider,
              private vibration:Vibration,
              private userService:UserService,
              private platform:Platform,
              private loadingCtrl:LoadingController,
              private alertController:AlertController,
              private settingsService:SettingsProvider) {

    this.settings=this.settingsService.getSettings();
    this.source=this.navParams.get('source')|| null;


    if(!this.settings.pin){
      this.passcodeLabel='SET 4 DIGIT PIN';
    }
    else{
      this.passcodeLabel='ENTER 4 DIGIT PIN'
      this.checkForFingerprint();

    }
  }

  ionVewDidLoad(){
   /* this.platform.backButton.subscribe((event:any)=>{
      console.log('back button is subscribed')
      if(this.source==='startup'){
        this.platform.exitApp();
      }
    });*/
  }

  ionViewWillUnload(){

    console.log('ion view unload')

    // if attempts==0,then do not exit from app ,
    //once attempts will be 0, logout event will be fired from digit function
    //and since logout event is being subscribed by app.component,it will redirect to sign in page
    if(this.source==='onResume' && this.attemptsLeft>0){
      console.log(this.passcodeWrong)
      if(!this.isVerifiedSuccessfully){
        console.log('exiting from app')
        this.platform.exitApp();

      }
      else {
      }
    }

  }

  presentConfirm(passcode:number) {
    let alert = this.alertController.create({
      title: 'Warning',
      message: 'This PIN you have created is extremely common and my be easily guessed by someone with access to your phone.would you like to use this PIN anyway?',
      buttons: [
        {
          text: 'TRY AGAIN',
          role: 'cancel',
          handler: () => {
            this.all_clear();
          }
        },
        {
          text: 'CONTINUE',
          handler: () => {
            this.createPasscode(passcode);
          }
        }
      ]
    });
    alert.present();
  }


  presentWarning(){
    let alert = this.alertController.create({
      title: 'Warning!',
      subTitle:'Last Chance',
      message:this.warningMessage,
      buttons: ['OK']
    });
    alert.present();
  }


  isPasscodeAreTooEasy(passcode){
    if(/^([0-9])\1*$/.test(passcode)) {
      return true
    }

    else{
      return false;

    }
  }


  done(isVerfied:boolean,isSetOperation:boolean=false) {
    if(this.source==='startup'){
      if(!isVerfied){
        return this.platform.exitApp();
      }
      else {
       return this.navCtrl.setRoot('HomePage')

      }
    }

    return this.viewController.dismiss({success:isVerfied});
  }


  async checkForFingerprint(){
    const  isEnabled=await this.settingsService.isFingerPrintEnabled();
    if(isEnabled){
      const isVerified=await this.settingsService.fingerprintVerification();
      if(isVerified){
        this.isVerifiedSuccessfully=true;
        this.done(true);
      }
    }
  }

  async goToNextPage(){
    let isAvailable=await this.settingsService.isFingerPrintAvailable();
    if(isAvailable){
      this.navCtrl.setRoot('FingerprintPage');
    }
    else{
      this.navCtrl.setRoot('HomePage');
    }
  }

  createPasscode(passcode:number):void {

      let loader = this.loadingCtrl.create({
        content: 'Setting 4 digit pin...'
      });
      loader.present();

      this.settingsService.setPinInStorage(passcode).then(()=>{
        this.goToNextPage();
        loader.dismiss();
      }).catch ((err)=>{
        loader.dismiss();

    })


  }
  digit(input:number){
    let digit=''+input;
    this.enteredPasscode+=digit;
    if(this.enteredPasscode.length===4){
      let passcode=parseInt(this.enteredPasscode);

      if(!this.settings.pin){
        let isTooEasy=this.isPasscodeAreTooEasy(passcode)
        if(isTooEasy){
          this.presentConfirm(passcode)
        }
        else{
          this.createPasscode(passcode)
        }
        }
        else {
        if(passcode!==this.settings.pin){
          this.vibration.vibrate(200);

          this.passcodeWrong=true;
          this.isVerifiedSuccessfully=false;
          this.attemptsLeft--;
          if(this.attemptsLeft===1){
            this.presentWarning();
          }
          if(this.attemptsLeft===0){
            this.settingsService.resetPin();
            this.authService.logoutUser();
            return;
          }
          setTimeout(()=>{
            this.passcodeWrong=false;
            this.all_clear();
          },800)


        }
        // pincode verified
        else{
          this.isVerifiedSuccessfully=true;
          this.done(true);

        }

        }

    }
  }

  all_clear(){
    this.enteredPasscode = '';

  }

  delete(){
    this.enteredPasscode = this.enteredPasscode.slice(0,-1);

  }

}

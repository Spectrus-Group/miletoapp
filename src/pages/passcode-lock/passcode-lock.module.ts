import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {PasscodeLockPage} from "./passcode-lock";

@NgModule({
  declarations: [
    PasscodeLockPage,
  ],
  imports: [
    IonicPageModule.forChild(PasscodeLockPage),
  ],
})
export class PasscodeLockPageModule {}

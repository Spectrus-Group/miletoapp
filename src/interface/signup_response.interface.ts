export interface ISignupResponse {
  id?:string,
  id_token?:string,
  email?:string,
  username?:string,
  name?:string

}

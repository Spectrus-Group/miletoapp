export interface ICategorySchema {
  id?: string;
  hex?: string;
  name?: string;
  url?: string;
  isOffSeason? : boolean;
}

export interface Isource {
  id?: string;
  source?: string;
}

export interface Icategory {
  color?: ICategorySchema[];
  source?: Isource[];
  type?: ICategorySchema[];
  favourite?: ICategorySchema[];
  featured?: ICategorySchema[];
  seasonal?: ICategorySchema[];
  roundAYear?: ICategorySchema[];
}


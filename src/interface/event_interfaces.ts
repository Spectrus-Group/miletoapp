
export interface FlowerDetails {
    flowerId? : string;
    groupid? : string;
    qty? : string;
    size? : string;
    optionvar2? : string;
    optionvar3? : string;
    url? : string;
    name ? : string ;
    cost ? : string;
    offSeason ? : boolean;
    comment? : string;
    type? : string[];
    color? : string;
    isSpecial? : boolean;
  } ; 

  export interface EventDetails {
    ordernumber? : string;
    userID? :  string;
    username? : string;
    userphone? : string;
    eventId? : string;
    name? :  string;
    address?: string;
    phone ? :string;
    date ?: string;
    datetimestamp ?: string;
    orders ? :  FlowerDetails[];
    reminders ?: ReminderDetails[];
    notes ?: NoteDetails[];
    isorderplaced ? : boolean;
    ordertype ? : string; 
    deliveryaddress? : string;
    status ? : string;
    eventtype ? : string;
  }

  export interface HolidayDetails {
    date ?: string;
    holiday ? :  string;
  }

  export interface ReminderDetails {
    reminderId ?: string;
    time ?: string;
    reminderText ? :  string;
  }

  export interface NoteDetails {
    noteId ?: string;
    time ?: string;
    noteText ? :  string;
  }

  export interface Customer {
    name? : string;
    phone? : string;
    address ? : string;
    id ? : string;
    userid ? : string ;
  }
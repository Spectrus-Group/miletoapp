export interface IBlog {
    content? : string ;
    createdAt ? : string;
    id?:string;
    pushSent?:string;
    updatedAt?:string;
}
export interface Isettings {
  pin?:number,
  pinEnabled?:boolean,
  fingerprintAvailable?:boolean,
  fingerprintEnabled?:boolean,
  isLoggedIn?:boolean,
  redirectToPin?:boolean
}

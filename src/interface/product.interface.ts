export interface category {
  color?: string;
  size?: string;
  type?: string;
}

export interface data {
  color?: string[];
  description?: string;
  featured?: boolean;
  name?: string;
  price_large?: string;
  price_medium?: string;
  price_small?: string;
  season?: string[];
  size?: string[];
  type?: string[];
  url_large?: string;
  url_medium?: string;
  url_small?: string;
  source?: string;
  est_stems? : string;
  isOffSeason? :  boolean;
}

export interface Iproduct {
  id?: string;
  isFavorite?: boolean;
  show?: boolean;
  product?: data;
}


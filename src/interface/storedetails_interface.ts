export interface StoreDetails {
    storekey ? : string;
    storeHeader ?: string;
    storeAddrline1 ? : string;
    storeAddrline2 ? : string;
    storeAddrline3 ? : string;
    storeAddrline4 ? : string;
    storephone? : string;
    storeemail? : string;

}
export interface ProductDetails {
    id? : string;
    name? : string;
    groupid? : string;
    description? : string;
    color? : string[];
    price_large? : string;
    price_medium? : string;
    price_small? : string;
    season? : string;
    url_large? : string;
    url_medium? : string;
    url_small? : string;
    category? : ProductCategory;
    size? :string[];
    type? : string[];
    est_stems? : string;
  } ; 

  export interface ProductCategory{
      color? : string[];
      type? : string[];
      size? : string[];
  }
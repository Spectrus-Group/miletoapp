import {FlowerDetails} from '../interface/event_interfaces'

export interface IQoute {
  user?: string;
  email?: string;
  orderdet? : FlowerDetails[];
  orderdate? : string;
  timestamp? : string;
}


